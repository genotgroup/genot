<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Layout {
	

	private $theme = 'public';
	 
	public function set_theme($theme) {
		$this->theme = $theme;
	}
	
	public function view($name, $data = array()) {
		$CI =& get_instance();
		//$params['content_for_layout'] = $CI->load->view($this->theme . '/content/' . $name, $data, true);
		$params['content'] = $CI->load->view($name, $data, true);
		//print_r($params['content']);
		//print_r('template/'.$this->theme . '/template');
		$CI->load->view('template/'.$this->theme . '/template.php', $params);
	}
}
