<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dossier_cotation
 *
 * @author romuald
 */
//load_class('Flexi_auth_lite', 'libraries', FALSE);
class Dossier_cotation {
    var $CI;
    //put your code here
    function __construct() {
        $this->CI = & get_instance();
        $this->CI->load->model('Model_print', 'model', TRUE);
//        $this->CI->load->library('Flexi_auth_lite', FALSE, 'flexi_auth');
    }

    public function newfolders() {
        $id_user = $this->CI->flexi_auth->get_user_by_id($this->CI->flexi_auth->get_user_id())->result();
        $id_user = $id_user[0]->uacc_id;
        $req = 'select dossiers.id as id_dossier,dossiers.date_d_entree,dossiers.code_dossier,dossiers.objet,'
                . 'cotation_dossier.id as id_cotation,cotation_dossier.date_cotation,'
                . 'user_profiles.upro_first_name,user_profiles.upro_last_name '
                . 'from dossiers,cotation_dossier,user_accounts,user_profiles where '
                . 'dossiers.id_utilisateur = user_accounts.uacc_id and '
                . 'user_accounts.uacc_id=user_profiles.upro_uacc_fk and '
                . 'dossiers.id=cotation_dossier.id_dossier and '
                . 'cotation_dossier.id_utilisateur_arrive='.$id_user.' and cotation_dossier.dossier_lu=0 '
                . 'group by cotation_dossier.id order by cotation_dossier.date_cotation '
                . 'limit 0, 20';
        
        $list_dos=$this->CI->model->getEntities($req);
        
        
        return $list_dos;
    }

}
