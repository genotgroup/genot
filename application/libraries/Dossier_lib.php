<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Tank_auth
 *
 * User library for Code Igniter.
 *
 * @package		Freelance
 * @author		Anatole ABE
 * @version		1.0.0
 */
class Dossier_lib {

    private $error = array();
    private $profile_table_name = 'user_profiles';
    private $user_table_name = 'administrateur';

    function __construct() {
        
        $this->ci = & get_instance();


        $this->ci->load->library('session');
        $this->ci->load->database();
        $this->ci->load->model('model_suivi_dossier');
        $this->ci->load->helper('url');

        // Try to autologin
        //$this->autologin();
    }

    /**
     * modifie le profil d'un utilisateur
     * user_id, username, password, email, new_email_key (if any).
     *
     * @param	string
     * @param	string
     * @param	string
     * @param	bool
     * @return	array
     */
    function update_profil($user_id, $profil) {
        $data = array(
            'description' => $profil['description'],
            'disponibility' => $profil['disponibility'],
            'work_mode' => $profil['work_mode'],
            'remuneration_average' => $profil['remuneration_average'],
            'number_employ' => $profil['number_employ'],
            'alert_email' => $profil['alert_email'],
        );
        if (!is_null($res = $this->ci->model_freelance_africa->update_Where($this->profile_table_name, 'user_id', $user_id, $data))) {
            return $res;
        }
    }

    /**
     * fonction qui renvoie les informations d'un utilisateur. utilise les tables:
     *  user, user_profils, sessions
     * @return	array
     */
    function get_user_data($user_id) {
        $res = $this->ci->model_freelance_africa->get_by_id($this->user_table_name, $user_id);

        return $res;
    }

    /**
     * fonction qui renvoie les informations de profil d'un utilisateur. utilise les tables:
     *  user_profils
     * 
     * @return	array
     */
    function get_user_profil($user_id) {
        $res = $this->ci->model_freelance_africa->get_where_attrib($this->profile_table_name, 'user_id', $user_id);

        return $res;
    }

    // Return the value of time different in "xx times ago" format
    function ago($timestamp, $delai, $deja_traite=0) {
        if ($deja_traite == 1) return "non";
        $temps = 0;
        $today = new DateTime(date('y-m-d h:m:s'));
        //$thatDay = new DateTime('Sun, 10 Nov 2013 14:26:00 GMT');
        $thatDay = new DateTime($timestamp);
        $dt = $today->diff($thatDay);

        if ($dt->y > 0) {
            $number = $dt->y;
            $unit = "year";
            $delai_depasse = 'oui';
            return $delai_depasse;
        }
        if ($dt->m > 0) {
            $number = $dt->m;
            $unit = "month";
            $delai_depasse = 'oui';
            return $delai_depasse;
        }
        if ($dt->d > 0) {
            $number = $dt->d;
            $unit = "day";
            $temps = $dt->d * 24 * 60 * 60;
        }
        if ($dt->h > 0) {
            $number = $dt->h;
            $unit = "hour";
            $temps = $temps + $dt->h * 60 * 60;
        }
        if ($dt->i > 0) {
            $number = $dt->i;
            $unit = "minute";
            $temps = $temps + $dt->i * 60;
        }
        if ($dt->s > 0) {
            $number = $dt->s;
            $unit = "second";
            $temps = $temps + $dt->s;
        }

        if ($temps > $delai * 60 * 60) {
            $delai_depasse = 'oui';
            return $delai_depasse;
        }

        return 'non';
    }
    
    /**
     * Envoie de mail 
     */
    function sendMail($msg, $email_user_to, $objet) {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.pnlt.cm',
            'smtp_port' => 465,
            'smtp_user' => 'programme.tb@pnlt.cm', // change it to yours
            'smtp_pass' => 'programme', // change it to yours
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'wordwrap' => TRUE
        );

        $message = $msg;
        $this->ci->load->library('email' );
        
        $this->ci->email->initialize($config);
        $this->ci->email->set_newline("\r\n");
        $this->ci->email->from('programme.tb@pnlt.cm'); // change it to yours
        $this->ci->email->to($email_user_to); // change it to yours
        $this->ci->email->subject($objet);
        $this->ci->email->message($message);
        if ($this->ci->email->send()) {
            return true;
        } else {
            show_error($this->ci->email->print_debugger());
            return false;
        }
    }
    
    function generateRandomString($length = 10) {
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

  
function delais_du_dossier_depassee($nbheurepasee, $duree, $statut=0) {
    if ($statut == 1)return "non";
    $dossier_est_depasse = $nbheurepasee > $duree ;
    if ($dossier_est_depasse==true) return "oui";
    return "non";
}


}

/* End of file Tank_auth.php */
/* Location: ./application/libraries/Tank_auth.php */