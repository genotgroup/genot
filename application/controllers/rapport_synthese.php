<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rapport_synthese extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
		
                $this->load->library(array('table', 'form_validation'));
                $this->load->helper('url');
                $this->load->helper('date');
                $this->load->library('security');
                $this->load->library('dossier_lib');
//                $this->load->library('utility_lib');
                $this->load->helper('download');
        //        $this->load->library('candidature_lib');
                $this->load->library('session');
                $this->load->model('model_suivi_dossier', '', TRUE); 
		// Load CI benchmark and memory usage profiler.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'lite' flexi auth library by default.
		// If preferable, functions from this library can be referenced using 'flexi_auth' as done below.
		// This prevents needing to reference 'flexi_auth_lite' in some files, and 'flexi_auth' in others, everything can be referenced by 'flexi_auth'.
		$this->load->library('flexi_auth_lite', FALSE, 'flexi_auth');	
                $this->load->library('dossier_cotation', FALSE, 'dossiers');
                
		// Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
		$this->load->vars('base_url', "http://" . $_SERVER['HTTP_HOST'] . "/dossier/");
		$this->load->vars('includes_dir', "http://" . $_SERVER['HTTP_HOST'] . "/dossier/includes/");
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		if (!$this->flexi_auth->is_logged_in_via_password() && uri_string() != 'auth/logout') 
		{
			redirect('auth/index');
		}
		$this->data = null;
	}
	
	public function index()
	{
		redirect('dossiers/liste');
	}
	

        /**
        * Fonction qui retourne la liste de tous utilisateurs ayant traiter un dossier
        */
       public function liste_utilisateur($id=0){

          
           $utilisateurs = $this->model_suivi_dossier->getEntities('select * from user_profiles up INNER JOIN user_accounts ua ON up.upro_uacc_fk=ua.uacc_id INNER JOIN cotation_dossier cd ON (ua.uacc_id=cd.id_utilisateur_arrive OR ua.uacc_id=cd.id_utilisateur_depart) Group by ua.uacc_id Order by up.upro_last_name, up.upro_first_name ');

           $this->load->library('table');
           $tmpl = array ( 'table_open'  => '<table class="table table-striped table-bordered table-hover" >' );
           $this->table->set_heading('Utilisateurs', 'Dossiers Traités', 'Dossiers En Cours','Dossiers Non Traités','Totaux');
           $this->table->set_template($tmpl); 
           foreach ($utilisateurs as $user) {
             # code...
   
              $restriction_utilisateur = "AND (id_utilisateur_depart = ".$user->uacc_id." OR id_utilisateur_arrive = ".$user->uacc_id.") ";
              
              $req_total_dossier_gerer ="SELECT  d.* FROM dossiers d ,cotation_dossier c WHERE c.id_dossier = d.id and 
                  (c.id_utilisateur_depart = ".$user->uacc_id." OR c.id_utilisateur_arrive = ".$user->uacc_id.")";
              
              $req_total_dossier_en_attente = "SELECT  d.* FROM dossiers d ,cotation_dossier c WHERE c.`id_dossier` = d.`id` and 
                  (c.id_utilisateur_arrive = ".$user->uacc_id.") and TIMESTAMPDIFF(HOUR, d.date_creation, CURDATE()) <= d.duree 
                  and d.statut = 0";
                  
              $req_total_dossier_traite = "SELECT  d.* FROM dossiers d ,cotation_dossier c WHERE c.`id_dossier` = d.`id` and 
                  (c.id_utilisateur_arrive = ".$user->uacc_id.")  and d.statut = 1";     

              $req_total_dossier_non_traite = "SELECT  d.* FROM dossiers d ,cotation_dossier c WHERE c.id_dossier = d.`id` and 
                  (c.id_utilisateur_arrive = ".$user->uacc_id.") and TIMESTAMPDIFF(HOUR, d.date_creation, CURDATE()) > d.duree 
                  and d.statut = 0";    

              //$filtre_depassedate_du_dossier = " and TIMESTAMPDIFF(HOUR, d.date_creation, CURDATE()) > d.duree"; 
              
              //$nbdossier_sans_restriction = $this->model_suivi_dossier->count_results_of_request($req . " GROUP BY c.`id_dossier`");
              $nbdossier = $this->model_suivi_dossier->count_results_of_request($req_total_dossier_gerer. " GROUP BY c.`id_dossier`");
              $nbdossier_traite = $this->model_suivi_dossier->count_results_of_request($req_total_dossier_traite. " GROUP BY c.id_dossier");
              $nbdossier_en_cours = $this->model_suivi_dossier->count_results_of_request($req_total_dossier_en_attente. " GROUP BY c.id_dossier");
              $nbdossier_depasse = $this->model_suivi_dossier->count_results_of_request($req_total_dossier_non_traite. " GROUP BY c.id_dossier");
              
              $link = $user->upro_last_name.' '.$user->upro_first_name;

              $this->table->add_row(array($link, 
                  '<center>'.anchor('rapport_synthese/liste_dossier_rapport/'.$user->uacc_id.'/3',$nbdossier_traite).'</center>', 
                  '<center>'.anchor('rapport_synthese/liste_dossier_rapport/'.$user->uacc_id.'/2',$nbdossier_en_cours).'</center>',
                  '<center>'.anchor('rapport_synthese/liste_dossier_rapport/'.$user->uacc_id.'/4',$nbdossier_depasse).'</center>',
                  '<center>'.anchor('rapport_synthese/liste_dossier_rapport/'.$user->uacc_id.'/1',$nbdossier).'</center>'));
           }

           $data['table'] = $this->table->generate();
           //$data['utilisateurs'] = $utilisateurs;
           $data['contents'] = 'rapport_synthèse/liste_users';
           $this->load->view('template/chargeur', $data);
           //$this->load->view('dossier_administratif/suivi_dossier_liste', $data);
       } 
       
       public function voir_rapport_synthese($id_user){


       }

       public function liste_dossier_rapport($id_user=0, $motif=0, $offset = 0) {
           //print_r($id_user." ".$motif." ".$offset);
          $dossier_est_depasse_css = "";
           $data = array();     
           if($motif == 1){      // req_total_dossier_gerer
                $req ="SELECT  d.*,c.date_cotation FROM dossiers d ,cotation_dossier c WHERE c.id_dossier = d.id and 
                  (c.id_utilisateur_depart = ".$id_user." OR c.id_utilisateur_arrive = ".$id_user.")";
                  $data["titre"] = "Total des dossiers gérés ";
           }   
              
           if($motif == 2){ // req_total_dossier_en_attente 
                $req = "SELECT  d.*,c.date_cotation FROM dossiers d ,cotation_dossier c WHERE c.id_dossier = d.id and 
                  (c.id_utilisateur_arrive = ".$id_user.") and TIMESTAMPDIFF(HOUR, d.date_creation, CURDATE()) <= d.duree 
                  and d.statut = 0";
                  $data["titre"] = "Les dossiers en attente ";
           } 
              
           if($motif == 3){  // req_total_dossier_traite  
                  $req = "SELECT  d.*,c.date_cotation FROM dossiers d ,cotation_dossier c WHERE c.id_dossier = d.id and 
                  (c.id_utilisateur_arrive = ".$id_user.")  and d.statut = 1";     
                  $data["titre"] = "Les dossiers traités ";
           }     

           if($motif == 4) { // req_total_dossier_non_traite   
              $req = "SELECT  d.*,c.date_cotation FROM dossiers d ,cotation_dossier c WHERE c.id_dossier = d.id and 
                  (c.id_utilisateur_arrive = ".$id_user.") and TIMESTAMPDIFF(HOUR, d.date_creation, CURDATE()) > d.duree 
                  and d.statut = 0";    
                  $data["titre"] = "Les dossiers non traités ";   
                  $dossier_est_depasse_css = "oui";   
          }        

        //Chargement de la librairie pagination
        $this->load->library('pagination');
        
        //Défini l'URL de base
        $config['base_url'] = site_url('rapport_synthese/liste_dossier_rapport/'.$id_user.'/'.$motif.'/');
        $nb_dossier = $this->model_suivi_dossier->count_results_of_request($req. " GROUP BY c.id_dossier");
        
        //Initialise le nombre total d'enregistrements
        $config['total_rows'] = $nb_dossier;

        //Initialise le nombre d'enregistrements à afficher par page
        $uri_segment = 5;
        $lim = 10;
        $config['per_page'] = $lim;
        $config['uri_segment'] = $uri_segment;
        
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';

        $offset = $this->uri->segment(5);
        if ($offset == null)
            $offset = 0;
        //print_r($offset);
        //Initialisation de notre pagination
        $this->pagination->initialize($config);

        $list_dossier_cote = $this->model_suivi_dossier->getEntities($req. " GROUP BY c.id_dossier "." LIMIT $offset,$lim");
        $count = $offset;
        $liste_tableau = "";

        if (empty($list_dossier_cote) == false)
            foreach ($list_dossier_cote as $dossier) {
                $count++;
                //print_r($dossier);
                $statut_action = $dossier->statut == 1 ? "" : "none";
                //selection des info des utilisateurs 
                
                $liste_tableau .="<tr class='$dossier_est_depasse_css'>
                        <td style=\"width: 40px;\">".$count."</td>
                        <td style=\"width: 150px;\"> $dossier->code_dossier </td> 
                        <td style=\"width: 300px;\"> $dossier->objet </td>
                        <td style=\"width: 50px;\">  $dossier->duree H </td> 
                        <td style=\"width: 50px;\">  $dossier->date_d_entree </td> 
                        <td style=\"width: 50px;\">  $dossier->date_cotation </td> 
                </tr>";
                $tableau_est_vide = true;
            }
        $user = $this->model_suivi_dossier->getEntities("select * from user_profiles where upro_uacc_fk=".$id_user);
        
        $data['titre'] = $data['titre'].' par : '.$user[0]->upro_last_name.'  '.$user[0]->upro_first_name;
        $data['tableau'] = $liste_tableau;
        $data['contents'] = 'rapport_synthèse/liste';
        $this->load->view('rapport_synthèse/liste', $data);     
    }

       public function voire_contation($id_cot=0,$id_dos=0){
           if($id_cot!=0 and $id_dos!=0){
               $cot = $this->model_suivi_dossier->get_by_id('cotation_dossier',$id_cot);
               if(isset($cot)){
                   $this->model_suivi_dossier->update_Where('cotation_dossier','id',$id_cot,array('dossier_lu' => TRUE));
               
                   redirect('dossiers/detail/'.$id_dos);
               }
           }  else {
               redirect('welcome');
            }
       }
        
}


