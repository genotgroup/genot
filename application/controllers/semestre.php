<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Semestre extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
        //$this->load->library('dossier_lib');
		 $this->load->model('model_generic', 'model', TRUE);
		// Load CI benchmark and memory usage profiler.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		 
		// Load CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		$this->data = null;
        $this->load->library('grocery_CRUD');       
		// Load 'lite' flexi auth library by default.
		// If preferable, functions from this library can be referenced using 'flexi_auth' as done below.
		// This prevents needing to reference 'flexi_auth_lite' in some files, and 'flexi_auth' in others, everything can be referenced by 'flexi_auth'.
		$this->load->library('flexi_auth');	
        //$this->load->library('dossier_cotation', FALSE, 'dossiers');
                
		// Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
		$this->load->vars('base_url', "http://" . $_SERVER['HTTP_HOST'] . "/genot/");
		$this->load->vars('includes_dir', "http://" . $_SERVER['HTTP_HOST'] . "/genot/includes/");
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		if (!$this->flexi_auth->is_logged_in_via_password() && uri_string() != 'auth/logout') 
		{
			redirect('auth/index');
		}
		$this->data = null;
        $this->layout->set_theme('admin');
	}

    public function semestre_rud(){
        $this->grocery_crud->set_table('semestre'); 
        $this->grocery_crud->set_relation('id_classe', 'classe', 'code'); 
        $this->grocery_crud->display_as('id_classe', 'Classe'); 
        $this->grocery_crud->columns("nom","code", "id_classe");
        $this->grocery_crud->set_theme('datatables');
        $this->grocery_crud->unset_add();
        $this->grocery_crud->unset_delete();
        $this->grocery_crud->unset_edit();
        //$this->grocery_crud->add_action('Delete', '', '','ui-icon-image',array($this,'delete'));
        $this->grocery_crud->add_action('Admin', '', 'semestre/adminSemestre','ui-icon-key');
        $this->grocery_crud->add_action('', '', 'semestre/initDeleteSemestre','ui-icon-trash');
        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des Semestres'; 
        $data['message'] = $this->session->userdata('message');
        $this->session->unset_userdata('message');
        $this->layout->view('admin/semestre/admin_rud_semestre.php', $data); 
    }

    public function adminSemestre($primary_key){
        $semestre = $this->model->getEntities("select * from semestre where id=".$primary_key)[0];
        $this->session->set_userdata('semestre', $semestre);
        redirect("semestre_crud_element/modules");
    }

    public function initDeleteSemestre($primary_key){
        $semestre = $this->model->getEntities("select * from semestre where id=".$primary_key)[0];
        $data['message'] = "Voulez vous vraiment supprimer le semestre [ ".$semestre->code." : ".$semestre->nom." ] ? <br/>".
                              "Pour continuer cliquez sur ".anchor("semestre/delete/$semestre->id", 'Supprimer')." ou alors ".
                              anchor('semestre/semestre_rud', 'Annuler');  
        $data['titre'] = 'Gestion des Semestres'; 
        $this->layout->view('admin/semestre/admin_rud_semestre.php', $data); 
    }

    function delete($primary_key)
    {
        //print_r($primary_key);
        $semestre = $this->model->getEntities("select * from semestre where id=".$primary_key)[0];
        $this->model->execQuery("DROP TABLE IF EXISTS modules_".$semestre->code);
        $this->model->execQuery("DROP TABLE IF EXISTS matiere_".$semestre->code);
        $this->model->execQuery("DROP TABLE IF EXISTS type_evaluation_".$semestre->code);
        $this->model->execQuery("DROP TABLE IF EXISTS evaluation_".$semestre->code);
        $this->model->execQuery("DROP TABLE IF EXISTS notes_".$semestre->code);
        $this->model->execQuery("DROP TABLE IF EXISTS moyenne_".$semestre->code);
        $this->model->execQuery("DROP TABLE IF EXISTS matiere_".$semestre->code);
        $this->model->execQuery("DELETE FROM semestre WHERE semestre.id =".$semestre->id);
        return redirect('semestre/semestre_rud');
    }

    public function initCreateSemestre() {
        $date = time("Y-m-d H:m:s");
        //$id = $this->flexi_auth->get_user_id();
        $this->form_data = new stdClass;
        $this->form_data->nom = '';
        $this->form_data->code = '';
        $this->form_data->id_classe = ''; 
        $data['classe_aca'] = $this->model->list_all('classe'); 
        $data['titre'] = "Creation d'un nouveau semestre";
        $this->layout->view('admin/semestre/admin_semestre_create.php', $data); 
    }
    
    public function createSemestre(){
        $this->validationForm();
        $this->form_validation->set_message('required', '* Champ obligatoire'); 
        if ($this->form_validation->run() == FALSE) {
            $this->initSobmitedSemestre();
            $data['classe_aca'] = $this->model->list_all('classe'); 
            $data['classe'] = !empty($this->input->post('id_classe'))?$this->model->get_by_id('classe',$this->input->post('id_classe'))[0]->id:'';
             
            $this->layout->view('admin/semestre/admin_semestre_create.php', $data);
        } else {
            $exist = $this->model->getEntities("select * from semestre where LOWER(code) like '%".strtolower($this->input->post('code'))."%'");
            if(!empty($exist)){
                $this->initSobmitedSemestre();
                $data['classe_aca'] = $this->model->list_all('classe'); 
                $data['classe'] = !empty($this->input->post('id_classe'))?$this->model->get_by_id('classe',$this->input->post('id_classe'))[0]->id:'';

                $data['message'] = 'Le code existe déjà';
                $this->layout->view('admin/semestre/admin_semestre_create.php', $data);
            }else{
                $info = array();
                $info['nom'] = $this->input->post('nom');
                $info['code'] = $this->input->post('code');
                $info['id_classe'] = $this->input->post('id_classe'); 
                $this->create_schama_semestre($info['code']);
                $id = $this->model->save('semestre', $info);
                $this->session->set_userdata('message', 'Semestre "'.$info['code'].'" créé avec succès');
                redirect('semestre/semestre_rud');
            }
        }
    }  

    // fonctions de validation du formulaire d'enregistrement
    function validationForm() { 
        $this->form_validation->set_rules('nom', 'Nom', 'trim');
        $this->form_validation->set_rules('code', 'Code', 'trim|required');
        $this->form_validation->set_rules('id_classe', 'Classe', 'trim|required'); 
    }

    /**
     * Initialise le formulaire de agrements aux informations saisies précédement 
     */
    function initSobmitedSemestre() {  
        $this->form_data = new stdClass;
        $this->form_data->nom = $this->input->post('nom');
        $this->form_data->code = $this->input->post('code');
        $this->form_data->id_classe = $this->input->post('id_classe');  
    } 

    function create_schama_semestre($codeSem){
        $reqModule = "CREATE TABLE IF NOT EXISTS modules_$codeSem (id int(11) NOT NULL AUTO_INCREMENT,code varchar(255) DEFAULT NULL,nom varchar(255) DEFAULT NULL,id_semestre int(11) DEFAULT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $reqMatiere = "CREATE TABLE IF NOT EXISTS matiere_$codeSem (id int(11) NOT NULL AUTO_INCREMENT,code varchar(255) DEFAULT NULL,nom varchar(255) DEFAULT NULL,id_module int(11) DEFAULT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $reqTypeEval = "CREATE TABLE IF NOT EXISTS type_evaluation_$codeSem (id int(11) NOT NULL AUTO_INCREMENT,nom varchar(255) DEFAULT NULL,code varchar(255) DEFAULT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $reqEvaluation = "CREATE TABLE IF NOT EXISTS evaluation_$codeSem (id int(11) NOT NULL AUTO_INCREMENT,date_eval DATE NOT NULL,pourcentage float DEFAULT NULL,id_type_evaluation int(11) DEFAULT NULL,id_matiere int(11) DEFAULT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $reqNotes = "CREATE TABLE IF NOT EXISTS notes_$codeSem (id int(11) NOT NULL AUTO_INCREMENT,note float DEFAULT NULL,id_auditeur int(11) DEFAULT NULL,id_evaluation int(11) DEFAULT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $reqMoyenne = "CREATE TABLE IF NOT EXISTS moyenne_$codeSem (id int(11) NOT NULL AUTO_INCREMENT,moyenne float DEFAULT NULL,id_auditeur int(11) DEFAULT NULL,id_module int(11) DEFAULT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $reqMoyenne_matiere = "CREATE TABLE IF NOT EXISTS moyenne_matiere_$codeSem (id int(11) NOT NULL AUTO_INCREMENT,moyenne float DEFAULT NULL,id_auditeur int(11) DEFAULT NULL,id_matiere int(11) DEFAULT NULL, PRIMARY KEY (id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";
        $this->model->execQuery($reqModule);
        $this->model->execQuery($reqMatiere);
        $this->model->execQuery($reqTypeEval);
        $this->model->execQuery($reqEvaluation);
        $this->model->execQuery($reqNotes);
        $this->model->execQuery($reqMoyenne);
        $this->model->execQuery($reqMoyenne_matiere);
    }

	public function index()
	{
            redirect('semestre/semestre_rud');
    }
             
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
