<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MainCrud extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('model_generic', 'model', TRUE);
        // To load the CI benchmark and memory usage profiler - set 1==1.
        if (1 == 2) {
            $sections = array(
                'benchmarks' => TRUE, 'memory_usage' => TRUE,
                'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE,
                'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
            );
            $this->output->set_profiler_sections($sections);
            $this->output->enable_profiler(TRUE);
        }

        // Load required CI libraries and helpers.
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('form');

        // IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
        // It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
        $this->auth = new stdClass;

        // Load 'standard' flexi auth library by default.
        $this->load->library('flexi_auth');

        // Redirect users logged in via password (However, not 'Remember me' users, as they may wish to login properly).
        if ($this->flexi_auth->is_logged_in_via_password()) {
            ;
        } else
            redirect('welcome');

        // Define a global variable to store data that is then used by the end view page.
        $this->data = null;
        $this->load->library('grocery_CRUD');
        // Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
        $this->load->vars('base_url', "http://" . $_SERVER['HTTP_HOST'] . "/genot/");
        $this->load->vars('includes_dir', "http://" . $_SERVER['HTTP_HOST'] . "/genot/includes/");
        $this->load->vars('current_url', $this->uri->uri_to_assoc(1));

        // Define a global variable to store data that is then used by the end view page.
        $this->data = null;
        $this->layout->set_theme('admin');
    }

    public function index() {
        $data['titre'] = 'Administration de la base de données';
//        $data['titre'] = 'Gestion des Années Académique'; 
        $this->layout->view('admin/admin_crud.php', $data);
    }

    public function anneAcademique() {
        $this->grocery_crud->set_table('anne_academique');
        $this->grocery_crud->columns("id","annee","id_precedente"); 
        $this->grocery_crud->set_relation('id_precedente', 'anne_academique', 'annee'); 
        $this->grocery_crud->display_as('id_precedente', 'Année Précédente');

//        $this->grocery_crud->callback_after_insert(array($this, 'redirect_semestre'));
//        $this->grocery_crud->callback_after_update(array($this, 'redirect_semestre'));

        $data = $this->grocery_crud->render();
        //$this->load->view('mainCrud/mainCrud.php', $output);
        $data['titre'] = 'Gestion des Années Académique'; 
        $this->layout->view('admin/admin_crud.php', $data); 
    }

  /*  public function redirect_semestre($post_array,$primary_key){
        redirect("mainCrud/semestre/".$primary_key);
    }*/

    public function semestre($id){
        $this->grocery_crud->set_table('semestre');
        //$this->grocery_crud->columns("id","annee","id_precedente"); 
        $this->grocery_crud->set_relation('id_annee_aca', 'anne_academique', 'annee'); 
        $this->grocery_crud->display_as('id_annee_aca', 'Année Académique');

        $this->grocery_crud->set_relation('id_niveau', 'niveau', 'code'); 
        $this->grocery_crud->display_as('id_niveau', 'niveau Académique');

        $this->grocery_crud->set_relation('id_specialite', 'specialite', 'code'); 
        $this->grocery_crud->display_as('id_specialite', 'Spécialités'); 

        $data = $this->grocery_crud->render();
        //$this->load->view('mainCrud/mainCrud.php', $output);
        $data['titre'] = 'Gestion des Semestres'; 
        $this->layout->view('admin/admin_crud.php', $data); 
    }

    public function anneSpecialite() {
        $this->grocery_crud->set_table('specialite');  
        $this->grocery_crud->columns("id","code","nom"); 
        //$this->grocery_crud->add_action('Semestre', '', 'demo/action_more','ui-icon-plus');
        //$this->grocery_crud->add_action('Photos', '', '','ui-icon-image',array($this,'just_a_test'));
        
        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des Spécialités du systeme'; 
        $this->layout->view('admin/admin_crud.php', $data); 
    }

    public function niveau() {
        $this->grocery_crud->set_table('niveau'); 
        $this->grocery_crud->columns("id","code","nom"); 
        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des Niveaux Académiques du systeme'; 
        $this->layout->view('admin/admin_crud.php', $data); 
    }

    public function promotion() {
        $this->grocery_crud->set_table('promotion'); 
        $this->grocery_crud->set_relation('id_annee_aca', 'anne_academique', 'annee'); 
        $this->grocery_crud->display_as('id_annee_aca', 'Année Académique');
        $this->grocery_crud->columns("id","id_annee_aca","nom");
        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des Promotions Académiques du systeme'; 
        $this->layout->view('admin/admin_crud.php', $data); 
    }

     

    public function classe(){
        $this->grocery_crud->set_table('classe'); 
        $this->grocery_crud->set_relation('id_annee_aca', 'anne_academique', 'annee'); 
        $this->grocery_crud->display_as('id_annee_aca', 'Année Académique');
        $this->grocery_crud->set_relation('id_specialite', 'specialite', 'nom'); 
        $this->grocery_crud->display_as('id_specialite', 'Spécialité');
        $this->grocery_crud->set_relation('id_niveau', 'niveau', 'code'); 
        $this->grocery_crud->display_as('id_niveau', 'Niveau');
        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des classes';  
        $this->layout->view('admin/admin_crud.php', $data); 
    }

    function just_a_test($primary_key , $row)
{
    return site_url('demo/action/action_photos').'?country=';
}
    
    public function auditeurs(){
        $this->grocery_crud->set_table('auditeurs'); 
        $this->grocery_crud->set_relation('id_classe', 'classe', 'code'); 
        $this->grocery_crud->display_as('id_classe', 'Classe');
        $this->grocery_crud->set_relation('id_promotion', 'promotion', 'nom'); 
        $this->grocery_crud->display_as('id_promotion', 'Promotion');
        $this->grocery_crud->columns("matricule","nom","prenom", "id_classe","id_promotion");
        $this->grocery_crud->set_theme('datatables');
        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des auditeurs'; 
        if($this->session->userdata('message')){
            $data['message'] = $this->session->userdata('message');
            $this->session->unset_userdata('message');
        }
        $this->layout->view('admin/admin_crud_auditeurs.php', $data); 
    }

    public function action_import_auditeurs(){
        $config = array();
        $config['upload_path'] = 'uploads';
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = '204800';
        $config['max_filename'] = '100'; 

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file_incident'))
        {
            $error = $this->upload->display_errors(); 
            print_r($this->upload->data());
            $this->session->set_userdata('message', $error);
            redirect('mainCrud/auditeurs');
        }
        else
        {   //print_r($this->upload->data());
            $data = $this->upload->data();
            $csv = new SplFileObject($data['full_path']);
            $csv->setFlags(SplFileObject::READ_CSV);
            $csv->setCsvControl(';');
            //print_r($csv); 
            print_r("\n"."<br/>");
            print_r("\n"."<br/>");
            $return = "";
            $i=0; $j=0; $p=0;
            foreach($csv as $ligne){
                $i++;
                if($i>1 && sizeof($ligne)>1){
                     
                    /*print_r($moniteur);
                    print_r("aaaaaaaaaa<br/>");
                    print_r($moniteur[0]->id);*/
                    
                    $auditeurs_info = 
                        array( 
                            "matricule"=>(string)$ligne[0],
                            "nom"=>(string)$ligne[1],
                            "prenom"=>(string)$ligne[2],
                            "email"=>(string)$ligne[3],
                            "lieu_naiss"=>(string)$ligne[4],
                            "date_naiss"=>(string)$ligne[5],
                            "id_classe"=>$ligne[6],
                            "id_promotion"=>$ligne[7]
                        );

                    $auditeur = $this->model->getEntities("select * from auditeurs where LOWER(matricule) like '%".strtolower($auditeurs_info['matricule'])."%'");
                        if(empty($auditeur) 
                            && !empty($auditeurs_info['matricule']) 
                            && !empty($auditeurs_info['nom']) 
                            && !empty($auditeurs_info['id_classe']))
                            {
                                $id_auditeur = $this->model->save("auditeurs",$auditeurs_info); 
                                $j++;
                            }
                        else {
                            $p++;
                            $return = $return.", ".(string)$i;       
                        } 
                }
            }
            $message = "Nombre de lignes : ".$i;
            $message = $message."<br/>Nombre d'enrégistrement(s) : ".$j; 
            $message = $message."<br/>Nombre de rejet(s) : ".$p;
            $message = $message."<br/>Les lignes de rejet : ".$return;
            $this->session->set_userdata('message', $message);
            redirect('mainCrud/auditeurs');
        } 
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */