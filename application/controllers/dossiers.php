<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dossiers extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library(array('table', 'form_validation'));
        $this->load->helper('url');
        $this->load->helper('date');
        $this->load->library('security');
        $this->load->library('dossier_lib');
//                $this->load->library('utility_lib');
        $this->load->helper('download');
        //        $this->load->library('candidature_lib');
        $this->load->library('session');
        $this->load->model('model_suivi_dossier', '', TRUE);

        // Load CI benchmark and memory usage profiler.
        if (1 == 2) {
            $sections = array(
                'benchmarks' => TRUE, 'memory_usage' => TRUE,
                'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE,
                'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
            );
            $this->output->set_profiler_sections($sections);
            $this->output->enable_profiler(TRUE);
        }

        // Load CI libraries and helpers.
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');

        // IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
        // It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
        $this->auth = new stdClass;

        // Load 'lite' flexi auth library by default.
        // If preferable, functions from this library can be referenced using 'flexi_auth' as done below.
        // This prevents needing to reference 'flexi_auth_lite' in some files, and 'flexi_auth' in others, everything can be referenced by 'flexi_auth'.
        $this->load->library('flexi_auth_lite', FALSE, 'flexi_auth');
        $this->load->library('dossier_cotation', FALSE, 'dossiers');

        // Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
        $this->load->vars('base_url', "http://" . $_SERVER['HTTP_HOST'] . "/dossier/");
        $this->load->vars('includes_dir', "http://" . $_SERVER['HTTP_HOST'] . "/dossier/includes/");
        $this->load->vars('current_url', $this->uri->uri_to_assoc(1));
        if (!$this->flexi_auth->is_logged_in_via_password() && uri_string() != 'auth/logout') {
            redirect('auth/index');
        }
        $this->data = null;
    }

    //    page d'accueil de l'application
    public function index() {
        $this->initDossier();
    }

    public function initDossier($s = 0) {
        $date = time("Y-m-d H:m:s");
        $this->session->set_userdata('code_dossier', '15P' . $this->dossier_lib->generateRandomString(2) . date('i', $date) . date('s', $date));
        $id = $this->flexi_auth->get_user_id();
        $this->form_data = new stdClass;
        $this->form_data->id_utilisateur = $id;
        $this->form_data->code_dossier = $this->session->userdata('code_dossier');
        $this->form_data->objet = '';
        $this->form_data->description = '';
        $this->form_data->type_dossier = '';
        $this->form_data->etat = '';
        $this->form_data->date_d_entree = '';
//            $this->form_data->date_sortie = '';
        $this->form_data->id_utilisateur_arrive = '';
        $this->form_data->duree = '';
        $this->form_data->message = '';
        $data['liste_collaborateurs'] = $liste_collaborateurs = $this->model_suivi_dossier->getEntities("SELECT * FROM user_accounts a, user_profiles p WHERE a.`uacc_id` = p.`upro_uacc_fk` AND a.`uacc_id` <> $id  ");  //selection des utilisateurs
        $data['action'] = site_url('dossiers/creerDossier/');
        if ($s == 1) {
            $data["confirm"] = "Enregisrement Effectueré avec succès";
        }
        $data['contents'] = 'dossier_administratif/form_creer_dossier';
        $this->load->view('template/chargeur', $data);
    }

    public function creerDossier() {

        //$this->output->enable_profiler(TRUE);
        //       validation du formulaire
        $this->validationForm();

        $this->form_validation->set_message('required', '* Champ obligatoire');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = "Erreur de valisation";
            $data['action'] = site_url('dossiers/creerDossier/');
            $data['message'] = 'Echec de l\'enregistrement';
            // chargement des valeurs initiales
            $id_user_connected = $this->flexi_auth->get_user_id(); //id utilisateur connecté
            $data['liste_collaborateurs'] = $liste_collaborateurs = $this->model_suivi_dossier->getEntities("SELECT * FROM user_accounts a, user_profiles p WHERE a.`uacc_id` = p.`upro_uacc_fk` AND a.`uacc_id` <> $id_user_connected  ");  //selection des utilisateurs
            $this->initSobmitedDossier();
            $data['contents'] = 'dossier_administratif/form_creer_dossier';
            $this->load->view('template/chargeur', $data);
        } else {
            $data['id_utilisateur'] = $this->flexi_auth->get_user_id();
            $data['code_dossier'] = $this->session->userdata('code_dossier');
            $data['objet'] = $this->input->post('objet');
            $data['description'] = $this->input->post('description');
            $data['type_dossier'] = $this->input->post('type_dossier');
            $data['etat'] = $this->input->post('etat');
            $data['date_d_entree'] = $this->input->post('date_d_entree');
//                $data['date_sortie']= $this->input->post('date_sortie');
            $data['duree'] = $this->input->post('duree');


            $date_systeme = date('Y-m-d H:m:s');
            $data['date_creation'] = $date_systeme;
            $data['date_dernier_modif'] = date('Y-m-d H:m:s');
            $id_dossier = $this->model_suivi_dossier->save('dossiers', $data);

            $cotation_dossier = array();
            $cotation_dossier['id_dossier'] = $id_dossier;
            $cotation_dossier['id_utilisateur_depart'] = $this->flexi_auth->get_user_id();
            $cotation_dossier['id_utilisateur_arrive'] = $this->input->post('id_utilisateur_arrive');
            $cotation_dossier['id_dossier'] = $id_dossier;
            $cotation_dossier['message'] = $this->input->post('message');
            $cotation_dossier['duree'] = $this->input->post('duree');
            $cotation_dossier['objet'] = $this->input->post('objet');
            $cotation_dossier['date_cotation'] = $date_systeme;
            $cotation_dossier['dossier_lu'] = false;

//                $traiter['id_personne']= $this->input->post('id_personne');
//                $traiter['id_demande'] = $id;  //id de la demande pré-enregistrée
            $id = $this->model_suivi_dossier->save('cotation_dossier', $cotation_dossier);
			
			$config = array();
            $config['upload_path'] = 'uploads';
            $config['allowed_types'] = 'gif|jpeg|jpg|png|tiff|doc|docx|txt|odt|xls|xlsx|pdf|ppt|pptx|pps|ppsx|mp3|m4a|ogg|wav|mp4|m4v|mov|wmv|flv|avi|mpg|ogv|3gp|3g2';
            $config['max_size'] = '204800';
            $config['max_filename'] = '100';

            $this->load->library('upload', $config);
            for($i=1;$i<=5;$i++){
                if($this->upload->do_upload('file_'.$i)){
                 $data_file_upload = $this->upload->data();
                    $piece_dossier = array();
                    $piece_dossier['id_dossier']= $id_dossier;
                    $piece_dossier['nom_fichier']= $data_file_upload['file_name'];
                    $piece_dossier['type']= $data_file_upload['file_type'];
                    $piece_dossier['chemin_sauvegarde']= 'uploads/'.  $data_file_upload['file_name'];
                     $this->model_suivi_dossier->save('pieces_jointes', $piece_dossier);
                }
            }
            
			
            $data["confirm"] = "Enregistrement Effectuer avec succès";

            $email_sender = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
            $email_sender = $email_sender[0]->uacc_email;

            $name_sender = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
            $name_sender = $name_sender[0]->upro_first_name . " " . $name_sender[0]->upro_last_name;


            $email_recipient = $this->flexi_auth->get_user_by_id($this->input->post('id_utilisateur_arrive'))->result();
            $email_recipient = $email_recipient[0]->uacc_email;

            $subject = "[PROCRASTINATE - PNLT Cameroun] " . $this->input->post('objet');
            $content = "Bonjour Mme./M. 
                    <br/>Vous avez un nouveau dossier qui vous a été coté par <b>" . $name_sender . "</b> dans la plateforme PROCRASTINATE. Bien vouloir vous connecter à votre compte pour y répondre à l'adresse suivante: <br/> www.pnlt.cm/dossier.
                        <br/> <b>Code du dossier</b> : " . $data['code_dossier'] . "<br/><b>Objet</b> : " . $data['objet'] . "
                            <br/> <br/><br/> Programme National de Lutte contre la Tuberculose.";

            $reponse = 3;
            if ($this->dossier_lib->sendMail($content, $email_recipient, $subject) == true)
                $reponse = 4;

            $this->liste($reponse);
//                $this->initDossier(1);
        }
    }

    // fonctions de validation du formulaire d'enregistrement
    function validationForm() {
//           $this->form_validation->set_rules('id_utilisateur', 'Createur dossier', 'trim|required');
        //$this->form_validation->set_rules('code_dossier', 'Code du dossier', 'trim|required');
        //        block 2
        $this->form_validation->set_rules('objet', 'Objet', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        $this->form_validation->set_rules('type_dosier', 'Type du dossier', 'trim');
        $this->form_validation->set_rules('etat', 'Etat', 'trim');
        $this->form_validation->set_rules('date_d_entree', 'Date entree dossier', 'trim|required');
//           $this->form_validation->set_rules('date_sortie', 'Date sortie dossier', 'trim');
        $this->form_validation->set_rules('duree', 'Durée dossier', 'trim|required');
//           $this->form_validation->set_rules('niveau_progression', 'niveau_progression', 'trim');
        $this->form_validation->set_rules('id_utilisateur_arrive', 'Ce dossier est coté à qui?', 'trim');
        $this->form_validation->set_rules('message', 'message', 'trim');
    }

    /**
     * Initialise le formulaire de agrements aux informations saisies précédement 
     */
    function initSobmitedDossier() {

//            $this->form_data->id_utilisateur = $this->input->post('id_utilisateur');
        $this->form_data->code_dossier = $this->session->userdata('code_dossier');
        $this->form_data->objet = $this->input->post('objet');
        $this->form_data->description = $this->input->post('description');
        $this->form_data->description = $this->input->post('type_dosier');
        $this->form_data->etat = $this->input->post('etat');
        $this->form_data->date_d_entree = $this->input->post('date_d_entree');
//            $this->form_data->date_sortie = $this->input->post('date_sortie');
        $this->form_data->duree = $this->input->post('duree');
        $this->form_data->duree = $this->input->post('id_utilisateur_arrive');
//            $this->form_data->niveau_progression = $this->input->post('niveau_progression');
        $this->form_data->message = $this->input->post('message');
    }

    /**
     * Fonction qui retourne la liste de tous les demandes d'agréments
     */
    public function liste($s = 0) {
        if ($s == 2) {
            $data["message"] = "Vous avez coté ce dossier à l'utilisateur xxxx";
            $data["message_type"] = "success";
        } else if ($s == 1) {
            $data["message"] = "Echec de l'opération";
            $data["message_type"] = "danger";
        } else if ($s == 3) {
            $data["message"] = "Opération effectué avec succès. Email non envoyé";
            $data["message_type"] = "success";
        } else if ($s == 4) {
            $data["message"] = "Opération effectué avec succès. Un E-mail a été envoyé à cet utilisateur.";
            $data["message_type"] = "success";
        }
        else if ($s == 4) {
            $data["message"] = "Opération effectué avec succès. Un E-mail a été envoyé à cet utilisateur.";
            $data["message_type"] = "success";
        }
        
        $filtre_url = $this->uri->segment(3);
        $filtre_traiter_encours_depasse_du_dossier = ""; //filtre du statut du dossier
        if ($filtre_url == null)
            $filtre_url = 0;
        else {
            if ($filtre_url == "processed"){//ce filtre permet de lister uniquement les dossiers en fonction de leurs statut (traité)
                $filtre_traiter_encours_depasse_du_dossier = "and statut = 1 ";
            }else if ($filtre_url == "ongoing"){//ce filtre permet de lister uniquement les dossiers en fonction de leurs statut (en cours)
                $filtre_traiter_encours_depasse_du_dossier = "and statut = 0 "; 
            }else if ($filtre_url == "outdated"){//ce filtre permet de lister uniquement les dossiers en fonction de leurs statut (dépassé)
                $filtre_traiter_encours_depasse_du_dossier = "and statut = 0 and TIMESTAMPDIFF(HOUR, date_creation, CURDATE()) > d.duree"; 
            }
        }
        //Récupération du parametre 
       
       
        $id_user_connected = $this->flexi_auth->get_user_id(); //id utilisateur connecté
        //on compte le nombre de candidats
       
        $restriction_utilisateur_req = "(c.id_utilisateur_depart = $id_user_connected or c.id_utilisateur_arrive = $id_user_connected) $filtre_traiter_encours_depasse_du_dossier ";
        $req_calcul_nb_heure_passee = "TIMESTAMPDIFF(HOUR, d.date_creation, CURDATE()) AS nbheurepasee ";
        $req = "SELECT *, $req_calcul_nb_heure_passee FROM cotation_dossier c, dossiers d "
                . "WHERE c.`id_dossier` = d.`id`  and "
                . "$restriction_utilisateur_req "
                . "GROUP BY c.`id_dossier` ORDER BY c.date_cotation  DESC";
        $nbagrements = $this->model_suivi_dossier->countEntities($req);

        //Chargement de la librairie pagination
        $this->load->library('pagination');
        
        //Défini l'URL de base
        $config['base_url'] = 'http://' . $_SERVER['HTTP_HOST'] . '/dossier/dossiers/liste/' . $filtre_url;
        $lien_controleur = base_url('dossiers');
        $lien_controleur2 = base_url('suivredossier');
        //Initialise le nombre total d'enregistrements
        $config['total_rows'] = $nbagrements;

        //Initialise le nombre d'enregistrements à afficher par page
        $uri_segment = 4;
        $lim = 10;
        $config['per_page'] = $lim;
        $config['uri_segment'] = $uri_segment;
        
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';

        $offside = $this->uri->segment(4);
        if ($offside == null)
            $offside = 0;
        //Initialisation de notre pagination
        $this->pagination->initialize($config);


        $list_dossier_cote = $this->model_suivi_dossier->getEntities($req." LIMIT $offside,$lim");
        $nbdossier_cote = $this->model_suivi_dossier->count_all('cotation_dossier');

        if ($nbdossier_cote > 0)
            $data["list_dossier_cote"] = $list_dossier_cote;
        else
            $data["list_dossier_cote"] = array();
        $data["valeur"] = "RAS";

        $liste_tableau = "";

        $count = $offside;
        $tableau_est_vide = false;
        if (empty($list_dossier_cote) == false)
            foreach ($list_dossier_cote as $dossier) {
               
                $count++;
//                -----------------------  Statut d'un dossier ------------------------------------------
                $statut = $dossier->statut == 0 ? "En cours" : ($dossier->statut == 1 ? "Traité" : "Rejetée");
                $statut_color = $dossier->statut == 0 ? "#D9534F" : ($dossier->statut == 1 ? "#5CB85C" : "");
                $statut_css = $dossier->statut == 0 ? "danger" : ($dossier->statut == 1 ? "success" : "danger");
                $statut_text = $dossier->statut == 0 ? "Marquer ce dossier comme traité" : ($dossier->statut == 1 ? "Dossier traité" : "danger");
//                ------------------------ Etat terminé d'un dossier --------------------------------------------'
                $terminer_est_possible = $dossier->statut == 0 ? "not-active" : ($dossier->statut == 1 ? "" : "");
                $terminer_text = $dossier->terminer == 0 ? "Ce dossier n'est pas encore cloturé" : ($dossier->terminer == 1 ? "Dossier cloturé" : "");
                $terminer_color = $dossier->terminer == 0 ? "#D9534F" : ($dossier->terminer == 1 ? "#5CB85C" : "");
                if ($dossier->statut == 0 && $dossier->terminer == 0) {
                    $terminer_color = "#7F7F7F";
                } 
//                --------------------------- Etat depassé ou pas d'un dossier -------------------------------------------'
//                $dossier_est_depasse = $this->dossier_lib->ago($dossier->date_creation, $dossier->duree,$dossier->statut);
                $dossier_est_depasse = $this->dossier_lib->delais_du_dossier_depassee($dossier->nbheurepasee , $dossier->duree, $dossier->statut) ;
                $dossier_est_depasse_css = $dossier_est_depasse == "oui" ? "danger" : ""; 


//                ----------------  on interdi à l'utilisateur de valider ses propres dossiers cotés ----------------------------
                $autorisation_de_validation = "";
                if ($id_user_connected == $dossier->id_utilisateur_depart) {
                    $autorisation_de_validation = "not-active";
                    $statut_text = "Vous ne pouvez pas valider ce dossier";
                }
//                ----------------- on interdit à un utilisateur de cloturer un dossier qui lui a été coté et dont il a validé -----------------------
                if ($dossier->statut == 1 && $id_user_connected != $dossier->id_utilisateur_depart) {
                    $terminer_est_possible = "not-active";
                }

                $statut_action = $dossier->statut == 1 ? "" : "none";
                //selection des info des utilisateurs".$agrement
                $user_origine = $this->model_suivi_dossier->getEntity("SELECT p.`upro_first_name` , p.`upro_last_name` FROM user_profiles p WHERE  p.`upro_uacc_fk` =   " . $dossier->id_utilisateur_depart);
                $user_cible = $this->model_suivi_dossier->getEntity("SELECT p.`upro_first_name` , p.`upro_last_name` FROM user_profiles p WHERE  p.`upro_uacc_fk` =   " . $dossier->id_utilisateur_arrive);

                $liste_tableau .="<tr class='$dossier_est_depasse_css'><td style=\"width: 40px;\" > $count</td>
                         <td style=\"width: 150px;\"> $dossier->code_dossier </td>
                         <td style=\"width: 200px;\"> $user_origine->upro_last_name $user_origine->upro_first_name </td>
                         <td style=\"width: 200px;\">$user_cible->upro_last_name  $user_cible->upro_first_name</td>
                        <td style=\"width: 300px;\">$dossier->objet </td>
                        <td style=\"width: 50px;\"> $dossier->duree H </td>
                        <td style=\"width: 90px;\">$dossier->date_cotation </td>
                         
                        <td style=\"width: 80px;\"><span class=\"label label-$statut_css\">$statut</span></td>
                        <td style=\"\">
                                <a href=\"$lien_controleur/init_BoitedialogueCotationDossier/$dossier->id_dossier/\"  title=\"Coter ce dossier à un collaborateur\"> <i class=\"fa fa-external-link fa-1x\"></i> </a>&nbsp
                                <a href=\"$lien_controleur/valider_dossier/$dossier->id_dossier\" onclick=\"if (confirm('Vous Confirmez ?')) return true; return false;\"  title=\"$statut_text\" class='$autorisation_de_validation'> <i style=\"color:$statut_color; \"class=\"fa fa-check-square fa-1x \"></i> </a>&nbsp
                                <a href=\"$lien_controleur/terminer_dossier/$dossier->id_dossier\" onclick=\"if (confirm('Vous Confirmez ?')) return true; return false;\" title=\"$terminer_text\" class='$terminer_est_possible'> <i style=\"color:$terminer_color; \"class=\"fa fa-lock fa-1x \"></i> </a>&nbsp
                                <a href=\"$lien_controleur2/suiviliste/$dossier->id_dossier/\"  title=\"Suivre le parcours de ce dossier\"> <i class=\"fa  fa-fast-forward fa-1x \"></i> </a>&nbsp
                                <a href=\"$lien_controleur/detail/$dossier->id\" title ='Voir les détails'><i class=\"fa  fa-list fa-1x\"></i></a>&nbsp
                                <a href=\"$lien_controleur#  \"  title ='Exporter ce en PDF'><i class=\"fa  fa-pdf fa-1x\"></i> </a>
                        </td>
                </tr>
                         ";
                $tableau_est_vide = true;
            }

            
        $data['tableau'] = $liste_tableau;
        if ($tableau_est_vide == false)
            $data['tableau'] = "<tr><td colspan='9 '> Pas de dossier disponible pour vous </td> </tr>";

        $this->load->view('dossier_administratif/liste', $data);
    }

    public function filter() {
        
        $filtre_url = $this->uri->segment(3);
        $filtre_url = isset($filtre_url) && $filtre_url != "f" && $filtre_url != "t" && $filtre_url != "a" ? "all" : $filtre_url;
        $id_user_connected = $this->flexi_auth->get_user_id(); //id utilisateur connecté
        $motif = $filtre_url=="all"?"":$filtre_url;
        //on compte le nombre de candidats
        $req = "SELECT * FROM cotation_dossier c, dossiers d "
                . "WHERE c.`id_dossier` = d.`id` and d.type_dossier like '$motif%' and "
                . "(c.id_utilisateur_depart = $id_user_connected or "
                . "c.id_utilisateur_arrive = $id_user_connected) "
                . "GROUP BY c.`id_dossier` ORDER BY c.date_cotation  DESC";
        $nbagrements = $this->model_suivi_dossier->countEntities($req);

        //Chargement de la librairie pagination
        $this->load->library('pagination');
        
        //Défini l'URL de base
        $config['base_url'] = 'http://' . $_SERVER['HTTP_HOST'] . '/dossier/dossiers/filter/' . $filtre_url;
        $lien_controleur = base_url('dossiers');
        $lien_controleur2 = base_url('suivredossier');
        //Initialise le nombre total d'enregistrements
        $config['total_rows'] = $nbagrements;

        //Initialise le nombre d'enregistrements à afficher par page
        $uri_segment = 4;
        $lim = 10;
        $config['per_page'] = $lim;
        $config['uri_segment'] = $uri_segment;
        
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';

        $offside = $this->uri->segment(4);
        if ($offside == null)
            $offside = 0;
        //Initialisation de notre pagination
        $this->pagination->initialize($config);


        $list_dossier_cote = $this->model_suivi_dossier->getEntities($req." LIMIT $offside,$lim");
        $nbdossier_cote = $this->model_suivi_dossier->count_all('cotation_dossier');

        if ($nbdossier_cote > 0)
            $data["list_dossier_cote"] = $list_dossier_cote;
        else
            $data["list_dossier_cote"] = array();
        $data["valeur"] = "RAS";

        $liste_tableau = "";

        $count = $offside;
        $tableau_est_vide = false;
        if (empty($list_dossier_cote) == false)
            foreach ($list_dossier_cote as $dossier) {
                $count++;
//                -----------------------  Statut d'un dossier ------------------------------------------
                $statut = $dossier->statut == 0 ? "En cours" : ($dossier->statut == 1 ? "Traité" : "Rejetée");
                $statut_color = $dossier->statut == 0 ? "#D9534F" : ($dossier->statut == 1 ? "#5CB85C" : "");
                $statut_css = $dossier->statut == 0 ? "danger" : ($dossier->statut == 1 ? "success" : "danger");
                $statut_text = $dossier->statut == 0 ? "Marquer ce dossier comme traité" : ($dossier->statut == 1 ? "Dossier traité" : "danger");
//                ------------------------ Etat terminé d'un dossier --------------------------------------------'
                $terminer_est_possible = $dossier->statut == 0 ? "not-active" : ($dossier->statut == 1 ? "" : "");
                $terminer_text = $dossier->terminer == 0 ? "Ce dossier n'est pas encore cloturé" : ($dossier->terminer == 1 ? "Dossier cloturé" : "");
                $terminer_color = $dossier->terminer == 0 ? "#D9534F" : ($dossier->terminer == 1 ? "#5CB85C" : "");
                if ($dossier->statut == 0 && $dossier->terminer == 0) {
                    $terminer_color = "#7F7F7F";
                }
//                --------------------------- Etat depassé ou pas d'un dossier -------------------------------------------'
                $dossier_est_depasse = $this->dossier_lib->ago($dossier->date_creation, $dossier->duree);
                $dossier_est_depasse_css = $dossier_est_depasse == "oui" ? "danger" : "";


//                ----------------  on interdi à l'utilisateur de valider ses propres dossiers coté ----------------------------
                $autorisation_de_validation = "";
                if ($id_user_connected == $dossier->id_utilisateur_depart) {
                    $autorisation_de_validation = "not-active";
                    $statut_text = "Vous ne pouvez pas valider ce dossier";
                }
                //                ----------------  on interdi à l'utilisateur de valider ses propres dossiers cotés ----------------------------
                $autorisation_de_validation = "";
                if ($id_user_connected == $dossier->id_utilisateur_depart) {
                    $autorisation_de_validation = "not-active";
                    $statut_text = "Vous ne pouvez pas valider ce dossier";
                }

                $statut_action = $dossier->statut == 1 ? "" : "none";
                //selection des info des utilisateurs".$agrement
                $user_origine = $this->model_suivi_dossier->getEntity("SELECT p.`upro_first_name` , p.`upro_last_name` FROM user_profiles p WHERE  p.`upro_uacc_fk` =   " . $dossier->id_utilisateur_depart);
                $user_cible = $this->model_suivi_dossier->getEntity("SELECT p.`upro_first_name` , p.`upro_last_name` FROM user_profiles p WHERE  p.`upro_uacc_fk` =   " . $dossier->id_utilisateur_arrive);

                $liste_tableau .="<tr class='$dossier_est_depasse_css'><td style=\"width: 40px;\" > $count</td>
                         <td style=\"width: 150px;\"> $dossier->code_dossier </td>
                         <td style=\"width: 200px;\"> $user_origine->upro_last_name $user_origine->upro_first_name </td>
                         <td style=\"width: 200px;\">$user_cible->upro_last_name  $user_cible->upro_first_name</td>
                        <td style=\"width: 300px;\">$dossier->objet </td>
                        <td style=\"width: 50px;\"> $dossier->duree H </td>
                        <td style=\"width: 90px;\">$dossier->date_cotation </td>
                         
                        <td style=\"width: 80px;\"><span class=\"label label-$statut_css\">$statut</span></td>
                        <td style=\"\">
                                <a href=\"$lien_controleur/init_BoitedialogueCotationDossier/$dossier->id_dossier/\"  title=\"Coter ce dossier à un collaborateur\"> <i class=\"fa fa-external-link fa-1x\"></i> </a>&nbsp
                                <a href=\"$lien_controleur/valider_dossier/$dossier->id_dossier\" onclick=\"if (confirm('Vous Confirmez ?')) return true; return false;\"  title=\"$statut_text\" class='$autorisation_de_validation'> <i style=\"color:$statut_color; \"class=\"fa fa-check-square fa-1x \"></i> </a>&nbsp
                                <a href=\"$lien_controleur/terminer_dossier/$dossier->id_dossier\" onclick=\"if (confirm('Vous Confirmez ?')) return true; return false;\" title=\"$terminer_text\" class='$terminer_est_possible'> <i style=\"color:$terminer_color; \"class=\"fa fa-lock fa-1x \"></i> </a>&nbsp
                                <a href=\"$lien_controleur2/suiviliste/$dossier->id_dossier/\"  title=\"Suivre le parcours de ce dossier\"> <i class=\"fa  fa-fast-forward fa-1x \"></i> </a>&nbsp
                                <a href=\"$lien_controleur/detail/$dossier->id\" title ='Voir les détails'><i class=\"fa  fa-list fa-1x\"></i></a>&nbsp
                                <a href=\"$lien_controleur#  \"  title ='Exporter ce en PDF'><i class=\"fa  fa-pdf fa-1x\"></i> </a>
                        </td>
                </tr>
                         ";
                $tableau_est_vide = true;
            }

            $data['key_word'] = ($filtre_url=="f")?"Dossiers Financiers":(($filtre_url=="a")?"Dossiers Admnistratifs":(($filtre_url=="t")?"Dossiers Techniques":""));    
        $data['tableau'] = $liste_tableau;
        if ($tableau_est_vide == false)
            $data['tableau'] = "<tr><td colspan='9 '> Pas de dossier disponible pour vous </td> </tr>";

        $this->load->view('dossier_administratif/liste', $data);
    }
    
    //*==============================================================

    public function valider_dossier($id_dos = 0) {
        if ($id_dos != 0) {
            $cot = $this->model_suivi_dossier->get_by_id('dossiers', $id_dos);
            if (isset($cot)) {
                $this->model_suivi_dossier->update_Where('dossiers', 'id', $id_dos, array('statut' => TRUE, 'date_dernier_modif' => Date()));
                redirect('dossiers/liste/');
            }
        } else {
            redirect('welcome');
        }
    }

    /**
     * Terminer un dossier
     * @param type $id_dos
     */
    public function terminer_dossier($id_dos = 0) {
        if ($id_dos != 0) {
            $cot = $this->model_suivi_dossier->get_by_id('dossiers', $id_dos);
            if (isset($cot)) {
                $this->model_suivi_dossier->update_Where('dossiers', 'id', $id_dos, array('terminer' => TRUE));
                redirect('dossiers/liste/');
            }
        } else {
            redirect('welcome');
        }
    }

    /**
     * fonction de recherche et listing 
     * 
     */
    public function research($key_word = "") {
//           print_r("pogo");
//           $URI = $this->uri->segment(1)."; 2 = ".$this->uri->segment(2)."; 3 = ".$this->uri->segment(3);
//           print_r($URI);
//           exit();
        $segment = $this->uri->segment(3);
        $key_word = !empty($segment) ? ($segment != 'ras' ? $segment : "") : "";
        $uri_segment = 4;
        $offside = $this->uri->segment($uri_segment);
        if ($offside == null)
            $offside = 0;
        $lim = 10;
        $id_user_connected = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
        $id_user_connected = $id_user_connected[0]->uacc_id; //id utilisateur connecté 
        $req = "";

        if ($key_word == "")
            $key_word = isset($_POST['search1']) ? $_POST['search1'] : "";
        if ($key_word == "")
            $key_word = isset($_POST['search2']) ? $_POST['search2'] : "";

        $key_word = trim(strtolower($key_word));
        $req = "SELECT * FROM 
            dossiers LEFT OUTER JOIN cotation_dossier ON dossiers.id = cotation_dossier.id_dossier 
            LEFT OUTER JOIN user_accounts ON user_accounts.uacc_id=dossiers.id_utilisateur 
            LEFT OUTER JOIN user_profiles ON user_profiles.upro_uacc_fk=user_accounts.uacc_id 
            WHERE dossiers.id_utilisateur = " . $id_user_connected . " and
            ( LOWER(user_profiles.upro_first_name) like '%" . $key_word . "%' 
                or LOWER(user_profiles.upro_last_name) like '%" . $key_word . "%' or 
                    LOWER(dossiers.code_dossier) like '%" . $key_word . "%' or 
                    LOWER(dossiers.objet) like '%" . $key_word . "%') "
                . "GROUP BY dossiers.id";

        $key_word = empty($key_word) ? "ras" : $key_word;
//        print_r($req);
//        exit();

        $data['key_word'] = $key_word;
        $nbagrements = $this->model_suivi_dossier->countEntities($req);
        $nbdossier_cote = $this->model_suivi_dossier->countEntities($req);
        $list_dossier_cote = $this->model_suivi_dossier->getEntities($req . " LIMIT " . $offside . "," . $lim);
//          $list_dossier_cote = $this->model_suivi_dossier->getEntities("SELECT * FROM cotation_dossier c, dossiers d WHERE c.`id_dossier` = d.`id` and d.type_dossier like '$filtre_url%' and (c.id_utilisateur_depart = $id_user_connected or c.id_utilisateur_arrive = $id_user_connected) GROUP BY c.`id_dossier` ORDER BY c.date_cotation  DESC LIMIT $offside,$lim");
        //on compte le nombre de candidats
//           $nbagrements = $this->model_suivi_dossier->count_all('cotation_dossier');
        //Chargement de la librairie pagination
        $this->load->library('pagination');

        //Défini l'URL de base
        $config['base_url'] = 'http://' . $_SERVER['HTTP_HOST'] . '/dossier/dossiers/research/' . $key_word;
        $lien_controleur = base_url('dossiers');
        $lien_controleur2 = base_url('suivredossier');
        //Initialise le nombre total d'enregistrements
        //Initialise le nombre d'enregistrements à afficher par page
        $config['per_page'] = $lim;
        $config['uri_segment'] = $uri_segment;
//        $config['num_links'] = $lim;
        $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '&laquo; First';
        $config['first_tag_open'] = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Last &raquo;';
        $config['last_tag_open'] = '<li class="next page">';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = 'Next &rarr;';
        $config['next_tag_open'] = '<li class="next page">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&larr; Previous';
        $config['prev_tag_open'] = '<li class="prev page">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active"><a href="">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li class="page">';
        $config['num_tag_close'] = '</li>';


        //Initialisation de notre pagination
        $config['total_rows'] = $nbagrements;
        $this->pagination->initialize($config);




        if ($nbdossier_cote > 0)
            $data["list_dossier_cote"] = $list_dossier_cote;
        else
            $data["list_dossier_cote"] = array();
        $data["valeur"] = "RAS";

        $liste_tableau = "";

        $count = $offside;
        $tableau_est_vide = false;
        if (empty($list_dossier_cote) == false)
            foreach ($list_dossier_cote as $dossier) {
                $count++;
//                -----------------------  Statut d'un dossier ------------------------------------------
                $statut = $dossier->statut == 0 ? "En cours" : ($dossier->statut == 1 ? "Traité" : "Rejetée");
                $statut_color = $dossier->statut == 0 ? "#D9534F" : ($dossier->statut == 1 ? "#5CB85C" : "");
                $statut_css = $dossier->statut == 0 ? "danger" : ($dossier->statut == 1 ? "success" : "danger");
                $statut_text = $dossier->statut == 0 ? "Marquer ce dossier comme traité" : ($dossier->statut == 1 ? "Dossier traité" : "danger");
//                ------------------------ Etat terminé d'un dossier --------------------------------------------'
                $terminer_est_possible = $dossier->statut == 0 ? "not-active" : ($dossier->statut == 1 ? "" : "");
                $terminer_text = $dossier->terminer == 0 ? "Ce dossier n'est pas encore cloturé" : ($dossier->terminer == 1 ? "Dossier cloturé" : "");
                $terminer_color = $dossier->terminer == 0 ? "#D9534F" : ($dossier->terminer == 1 ? "#5CB85C" : "");
                if ($dossier->statut == 0 && $dossier->terminer == 0) {
                    $terminer_color = "#7F7F7F";
                }
//                ----------------  on interdi à l'utilisateur de valider ses propres dossiers coté ----------------------------
                $autorisation_de_validation = "";
                if ($id_user_connected == $dossier->id_utilisateur_depart) {
                    $autorisation_de_validation = "not-active";
                    $statut_text = "Vous ne pouvez pas valider ce dossier";
                }

                $statut_action = $dossier->statut == 1 ? "" : "none";
                //selection des info des utilisateurs".$agrement
                $user_origine = $this->model_suivi_dossier->getEntity("SELECT p.`upro_first_name` , p.`upro_last_name` FROM user_profiles p WHERE  p.`upro_uacc_fk` =   " . $dossier->id_utilisateur_depart);
                $user_cible = $this->model_suivi_dossier->getEntity("SELECT p.`upro_first_name` , p.`upro_last_name` FROM user_profiles p WHERE  p.`upro_uacc_fk` =   " . $dossier->id_utilisateur_arrive);

                $liste_tableau .="<tr><td style=\"width: 40px;\" > $count</td>
                         <td style=\"width: 150px;\"> $dossier->code_dossier </td>
                         <td style=\"width: 200px;\"> $user_origine->upro_last_name $user_origine->upro_first_name </td>
                         <td style=\"width: 200px;\">$user_cible->upro_last_name  $user_cible->upro_first_name</td>
                        <td style=\"width: 300px;\">$dossier->objet </td>
                        <td style=\"width: 50px;\"> $dossier->duree H </td>
                        <td style=\"width: 90px;\">$dossier->date_cotation </td>
                         
                        <td style=\"width: 80px;\"><span class=\"label label-$statut_css\">$statut</span></td>
                        <td style=\"\">
                                <a href=\"$lien_controleur/init_BoitedialogueCotationDossier/$dossier->id_dossier/\"  title=\"Coter ce dossier à un collaborateur\"> <i class=\"fa fa-external-link fa-1x\"></i> </a>&nbsp
                                <a href=\"dossiers/valider_dossier/\"$dossier->id_dossier onclick=\"if (confirm('Vous Confirmez ?')) return true; return false;\"  title=\"$statut_text\" class='$autorisation_de_validation'> <i style=\"color:$statut_color; \"class=\"fa fa-check-square fa-1x \"></i> </a>&nbsp
                                <a href=\"dossiers/terminer_dossier/\"$dossier->id_dossier onclick=\"if (confirm('Vous Confirmez ?')) return true; return false;\" title=\"$terminer_text\" class='$terminer_est_possible'> <i style=\"color:$terminer_color; \"class=\"fa fa-lock fa-1x \"></i> </a>&nbsp
                                <a href=\"$lien_controleur2/suiviliste/$dossier->id_dossier/\"  title=\"Suivre le parcours de ce dossier\"> <i class=\"fa  fa-fast-forward fa-1x \"></i> </a>&nbsp
                                <a href=\"$lien_controleur/detail/$dossier->id\" title ='Voir les détails'><i class=\"fa  fa-list fa-1x\"></i></a>&nbsp
                                <a href=\"$lien_controleur#  \"  title ='Exporter ce en PDF'><i class=\"fa  fa-pdf fa-1x\"></i> </a>
                        </td>
                </tr>
                         ";
                $tableau_est_vide = true;
            }


        $data['tableau'] = $liste_tableau;
        if ($tableau_est_vide == false)
            $data['tableau'] = "<tr><td colspan='9 '> Pas de dossier disponible pour vous </td> </tr>";

        $this->load->view('dossier_administratif/liste', $data);
    }

    /**
     * Retourne un dossier
     * @param type $id
     */
    public function detail($id) {
        $req = "SELECT * FROM dossiers d WHERE d.`id` = $id";
        $dossier = $this->model_suivi_dossier->getEntity($req);
        $data["dossier"] = $dossier;
        
        $req = "SELECT * FROM pieces_jointes d WHERE d.`id_dossier` = $id";
        $pieces_jointes = $this->model_suivi_dossier->getEntities($req);
        $data["pieces_jointes"] = $pieces_jointes;
        
        $this->session->set_userdata('dossier', $dossier); //mise en session
        // $fichier_joint  = $this->model_dnassurance->getEntity("SELECT * from fichiers_joint where id_demande =  ".$agrement->id);
        // $data["fichier_joint"] = $fichier_joint;

        $this->load->view('dossier_administratif/detail_dossier', $data);
    }

//       ================================== gestion des cotation de dossier ==========================================================

    public function init_BoitedialogueCotationDossier($s = 0) {
        $offside = $this->uri->segment(3);

        $this->form_data->id_utilisateur = $this->flexi_auth->get_user_id();
        $this->form_data->id_utilisateur_arrive = '';
        $this->form_data->duree = '';
        $this->form_data->id_dossier = $offside;
        $this->form_data->message = '';
        $id_user_connected = $this->flexi_auth->get_user_id(); //id utilisateur connecté
        $data['liste_collaborateurs'] = $liste_collaborateurs = $this->model_suivi_dossier->getEntities("SELECT * FROM user_accounts a, user_profiles p WHERE a.`uacc_id` = p.`upro_uacc_fk` AND a.`uacc_id` <> $id_user_connected  ");  //selection des utilisateurs
        $data['action'] = site_url('dossiers/liste/');
        if ($s == 1) {
            $data["confirm"] = "Vous avez coté ce dossier à l'utilisateur xxxx";
        }
//            $data['contents']='dossier_administratif/form_coter_dossier_collaborateur';
//            $data['message']='Vous avez coter ce dossier à un autre utilisateur';
        //$data['modal_activate']='show';
        $this->load->view('dossier_administratif/form_coter_dossier_collaborateur', $data);
    }

    /**
     * Initialise le formulaire de agrements aux informations saisies précédement 
     */
    function init_SobmitedBoitedialogueCotationDossier() {

        $this->form_data->id_utilisateur = $this->flexi_auth->get_user_id();
        $this->form_data->id_utilisateur_arrive = $this->input->post('id_utilisateur_arrive');
        $this->form_data->duree = $this->input->post('duree');
        $this->form_data->id_dossier = $this->input->post('id_dossier');
        $this->form_data->message = $this->input->post('message');
        $this->form_data->date_cotation = date('Y-m-d H:m:s');
    }

    // fonctions de validation du formulaire d'enregistrement
    function validation_FormBoitedialogueCotationDossier() {

        $this->form_validation->set_rules('id_utilisateur_arrive', 'Choisir un collaborateur', 'trim|required');
        //        block 2
        $this->form_validation->set_rules('duree', 'Durée dossier', 'trim|required');
//           $this->form_validation->set_rules('niveau_progression', 'niveau_progression', 'trim');
        $this->form_validation->set_rules('message', 'message', 'trim');
        $this->form_validation->set_rules('id_dossier', 'id_dossier', 'trim');
    }

    /**
     * Fonction qui permet de coter un dossier à une nouvelle personne
     * @param type $id
     */
    public function coter_dossier() {

        //       validation du formulaire
        $this->validation_FormBoitedialogueCotationDossier();

        $this->form_validation->set_message('required', '* Champ obligatoire');
        if ($this->form_validation->run() == FALSE) {
            $data['error'] = "Erreur de valisation";
            $data['action'] = site_url('dossiers/creerDossier/');
            $data['message'] = 'Echec de l\'operation';
            // chargement des valeurs initiales
            $id_user_connected = $this->flexi_auth->get_user_id(); //id utilisateur connecté
            $data['liste_collaborateurs'] = $liste_collaborateurs = $this->model_suivi_dossier->getEntities("SELECT * FROM user_accounts a, user_profiles p WHERE a.`uacc_id` = p.`upro_uacc_fk` AND a.`uacc_id` <> $id_user_connected  ");  //selection des utilisateurs
            $this->init_SobmitedBoitedialogueCotationDossier();
            $this->liste(1);
        } else {

            $cotation_dossier['id_utilisateur_depart'] = $this->flexi_auth->get_user_id();
            $cotation_dossier['id_utilisateur_arrive'] = $this->input->post('id_utilisateur_arrive');
            $cotation_dossier['id_dossier'] = $this->input->post('id_dossier');
            $cotation_dossier['message'] = $this->input->post('message');
            $cotation_dossier['duree'] = $this->input->post('duree');
            $cotation_dossier['date_cotation'] = date('Y-m-d H:m:s');

            $id = $this->model_suivi_dossier->save('cotation_dossier', $cotation_dossier);
            $data["confirm"] = "Enregistrement effectuer avec succès";
            $this->liste(2);
        }
    }

    public function sendmail($email_sender, $name_sender, $email_recipient, $subject, $content) {
        $this->load->library('email');
        $this->email->from($email_sender, $name_sender);
        $this->email->to($email_recipient);
        $this->email->subject($subject);
        $this->email->message($content);
        $this->email->send();
    }

}

