<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Semestre_crud_element extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
        //$this->load->library('dossier_lib');
		 $this->load->model('model_generic', 'model', TRUE);
		// Load CI benchmark and memory usage profiler.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		 
		// Load CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		$this->data = null;
        $this->load->library('grocery_CRUD');       
		// Load 'lite' flexi auth library by default.
		// If preferable, functions from this library can be referenced using 'flexi_auth' as done below.
		// This prevents needing to reference 'flexi_auth_lite' in some files, and 'flexi_auth' in others, everything can be referenced by 'flexi_auth'.
		$this->load->library('flexi_auth');	
        //$this->load->library('dossier_cotation', FALSE, 'dossiers');
                
		// Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
		$this->load->vars('base_url', "http://" . $_SERVER['HTTP_HOST'] . "/genot/");
		$this->load->vars('includes_dir', "http://" . $_SERVER['HTTP_HOST'] . "/genot/includes/");
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		if (!$this->flexi_auth->is_logged_in_via_password() && uri_string() != 'auth/logout') 
		{
			redirect('auth/index');
		}
		$this->data = null;
        $this->layout->set_theme('admin');
	}

    public function modules(){
        $semestre = $this->session->userdata('semestre');
        $this->grocery_crud->set_table('modules_'.$semestre->code);  
        $this->grocery_crud->columns("id","code","nom");
        $this->grocery_crud->required_fields("code","nom");
        $this->grocery_crud->set_theme('datatables');
        $this->grocery_crud->fields("code","nom");
        
        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des Modules du Semestre '.$semestre->code; 
        
        $this->layout->view('admin/semestre/admin_crud_element_semestre.php', $data); 
    }

    public function matiere(){
        $semestre = $this->session->userdata('semestre');
        $this->grocery_crud->set_table('matiere_'.$semestre->code);
        $this->grocery_crud->set_relation('id_module', 'modules_'.$semestre->code, 'nom'); 
        $this->grocery_crud->display_as('id_module', 'Module');  
        $this->grocery_crud->required_fields("code","nom");
        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des matieres du Semestre '.$semestre->code; 
        
        $this->layout->view('admin/semestre/admin_crud_element_semestre.php', $data); 
    }

    public function type_evaluation(){
        $semestre = $this->session->userdata('semestre');
        $this->grocery_crud->set_table('type_evaluation_'.$semestre->code);
        $this->grocery_crud->required_fields("nom"); 
        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des types d\'évaluation du Semestre '.$semestre->code; 
        
        $this->layout->view('admin/semestre/admin_crud_element_semestre.php', $data); 
    }

    public function evaluation(){
        $semestre = $this->session->userdata('semestre');
        $this->grocery_crud->set_table('evaluation_'.$semestre->code); 
        $this->grocery_crud->set_relation('id_type_evaluation', 'type_evaluation_'.$semestre->code, 'nom'); 
        $this->grocery_crud->display_as('id_type_evaluation', 'Type d\'Evaluation');  
        $this->grocery_crud->set_relation('id_matiere', 'matiere_'.$semestre->code, 'nom'); 
        $this->grocery_crud->display_as('id_matiere', 'Matière'); 
        $this->grocery_crud->required_fields("pourcentage","id_type_evaluation","id_matiere");

        $this->grocery_crud->columns("id","id_type_evaluation","id_matiere");

        $data = $this->grocery_crud->render(); 
        $data['titre'] = 'Gestion des Evaluations du Semestre '.$semestre->code; 
        
        $this->layout->view('admin/semestre/admin_crud_element_semestre.php', $data); 
    }

	public function index()
	{
            redirect('semestre/semestre_rud');
    }
             
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
