<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notes extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    function __construct() {
        parent::__construct();
        $this->load->model('model_generic', 'model', TRUE);
        // To load the CI benchmark and memory usage profiler - set 1==1.
        if (1 == 2) {
            $sections = array(
                'benchmarks' => TRUE, 'memory_usage' => TRUE,
                'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE,
                'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
            );
            $this->output->set_profiler_sections($sections);
            $this->output->enable_profiler(TRUE);
        }

        // Load required CI libraries and helpers.
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('form');

        // IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
        // It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
        $this->auth = new stdClass;

        // Load 'standard' flexi auth library by default.
        $this->load->library('flexi_auth');

        // Redirect users logged in via password (However, not 'Remember me' users, as they may wish to login properly).
        if ($this->flexi_auth->is_logged_in_via_password()) {
            ;
        } else
            redirect('welcome');

        // Define a global variable to store data that is then used by the end view page.
        $this->data = null;
        $this->load->library('grocery_CRUD');
        // Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
        $this->load->vars('base_url', "http://" . $_SERVER['HTTP_HOST'] . "/genot/");
        $this->load->vars('includes_dir', "http://" . $_SERVER['HTTP_HOST'] . "/genot/includes/");
        $this->load->vars('current_url', $this->uri->uri_to_assoc(1));

        // Define a global variable to store data that is then used by the end view page.
        $this->data = null;
        $this->layout->set_theme('admin');
    }

    public function index() {
        redirect("notes/classe");
    }

    

    public function classe(){
        if(!$this->session->userdata('id_anne_academique')) redirect("welcome");
        $classes = $this->model->getEntities("select * from classe where id_annee_aca=".$this->session->userdata('id_anne_academique'));
        $output = "<ul>";
        foreach ($classes as $classe) {
            # code...
            $link = anchor("notes/semestre/$classe->id",$classe->code." : ".$classe->nom);
            $output = $output . "<li>".$link."</li>";
        }
        $output = $output . "</ul>";
        $data["output"] = $output;
        $data["fil"] = "Année = ".anchor("notes/classe", $this->session->userdata('anne_academique'));
        //$this->load->view('mainCrud/mainCrud.php', $output);
        $data['titre'] = 'Choix de la classe'; 
        $this->layout->view('admin/notes/navigation_config_notes.php', $data); 
    }

    public function semestre($id_classe=0){
        if(!$this->session->userdata('id_anne_academique')) redirect("welcome"); 
        if($id_classe==0) redirect("notes/classe");
        $classe = $this->model->getEntities("select * from classe where id=".$id_classe)[0];
        $this->session->set_userdata('classe_note', $classe);
        $semestres = $this->model->getEntities("select * from semestre where id_classe=".$id_classe);

        $output = "<ul>";
        foreach ($semestres as $semestre) {
            # code...
            $output = $output . "<li>".anchor("notes/module/$semestre->id",$semestre->code." : ".$semestre->nom);
        }
        $output = $output . "</ul>";
        $data["output"] = $output;
        $data["fil"] = "Année = ".anchor("notes/classe", $this->session->userdata('anne_academique'))." -> ".
                        "Classe = ".anchor("notes/semestre/$id_classe", $classe->code." : ".$classe->nom) ;
        $data['titre'] = 'Selection du Semestre'; 
       $this->layout->view('admin/notes/navigation_config_notes.php', $data); 
    }

    public function module($id_semestre=0){
        if($id_semestre==0) redirect("notes/classe");
        if(!$this->session->userdata('id_anne_academique')) redirect("welcome");
        if(!$this->session->userdata('classe_note')) redirect("notes/classe");
        $semestre = $this->model->getEntities("select * from semestre where id=".$id_semestre)[0];
        $this->session->set_userdata('semestre_note', $semestre);
        $classe = $this->session->userdata('classe_note');
        //print_r($semestre);
        $modules = $this->model->getEntities("select * from modules_".$semestre->code);

        $output = "<ol>";
        foreach ($modules as $module) {
            # code...
            $matieres = $this->model->getEntities("select * from matiere_$semestre->code where id_module=".$module->id);
            $output2 = "<ul>";
            //$output2 = $output2 . "<li>".$module->code." : ".$module->nom;
            foreach ($matieres as $matiere) {
                $output2 = $output2 . "<li>".$matiere->code." : ".$matiere->nom." &nbsp;".anchor("notes/tabNotes/$matiere->id","<img src='".base_url()."/resources/admin/images/1465317522_compose.png' style='width:15px; height:15px' />"."Saisir ou Impoter");
            }
            $output2 = $output2 . "</ul>";
            $output = $output . "<li>Module ".$module->code." : ".$module->nom." &nbsp;".
                            anchor("notes/consulter_notes_modules/$module->id","<img src='".base_url()."/resources/admin/images/1465317591_view-content-window.png' style='width:15px; height:15px' />"."Voir ou Exporter les notes").
                            $output2."<br/>";
        }
        $output = $output . "</ol>";
        $data["output"] = $output;
        $data["fil"] = "Année = ".anchor("notes/classe", $this->session->userdata('anne_academique'))." -> ".
                        "Classe = ".anchor("notes/semestre/$classe->id", $classe->code." : ".$classe->nom)." -> ".
                        "Semestre = ".anchor("notes/module/$semestre->id", $semestre->code." : ".$semestre->nom) ;
        $data['titre'] = 'Selection du Module'; 
       $this->layout->view('admin/notes/navigation_config_notes.php', $data);
    }

    public function tabNotes($id_matiere=0){
        if($id_matiere==0) redirect("notes/classe");
        if(!$this->session->userdata('id_anne_academique')) redirect("welcome");
        if(!$this->session->userdata('classe_note')) redirect("notes/classe/".$this->session->userdata('id_anne_academique'));
        if(!$this->session->userdata('semestre_note')) redirect("notes/module/".$this->session->userdata('classe_note')->id);
        
        $semestre = $this->session->userdata('semestre_note');
        $matiere = $this->model->getEntities("select * from matiere_".$semestre->code." where id=".$id_matiere);
        if(!empty($matiere)) $matiere = $matiere[0];
        if(!empty($matiere))
        {   
            $this->session->set_userdata('matiere_note', $matiere);
            $semestre = $this->session->userdata('semestre_note');
            $classe = $this->session->userdata('classe_note');
            $auditeurs = $this->model->getEntities("select * from auditeurs where id_classe=".$this->session->userdata('id_anne_academique')." order by nom,prenom");
            $evaluations = $this->model->getEntities("select * from evaluation_".$semestre->code." e,type_evaluation_".$semestre->code." t ".
                            " where e.id_matiere=".$id_matiere." and e.id_type_evaluation=t.id");
            
            redirect("notes/saisieNotes");
        }
    }

    function Getfloat($str) {
          if(strstr($str, ",")) {
            $str = str_replace(".", "", $str); // replace dots (thousand seps) with blancs
            $str = str_replace(",", ".", $str); // replace ',' with '.'
          }         
          if(preg_match("#([0-9\.]+)#", $str, $match)) { // search for number that may contain '.'
            return floatval($match[0]);
          } else {
            return floatval($str); // take some last chances with floatval
          }
    }   

    public function saisieNotes($offset=0){ 
        if(!$this->session->userdata('id_anne_academique')) redirect("welcome");
        if(!$this->session->userdata('classe_note')) redirect("notes/classe/".$this->session->userdata('id_anne_academique'));
        if(!$this->session->userdata('semestre_note')) redirect("notes/module/".$this->session->userdata('classe_note')->id);
        if(!$this->session->userdata('matiere_note')) redirect("notes/module/".$this->session->userdata('semestre_note')->id);

        $matiere = $this->session->userdata('matiere_note');
        $semestre = $this->session->userdata('semestre_note');
        $classe = $this->session->userdata('classe_note');

        //print_r($matiere);

        $req_auditeurs = "select * from auditeurs where id_classe=".$this->session->userdata('id_anne_academique')." order by nom,prenom";
        $auditeurs = $this->model->getEntities($req_auditeurs);
        $evaluations = $this->model->getEntities("select e.*, t.code as code from evaluation_".$semestre->code." e,type_evaluation_".$semestre->code." t ".
                        " where e.id_matiere=".$matiere->id." and e.id_type_evaluation=t.id");
        /*$req = "select * from evaluation_".$semestre->code." e,type_evaluation_".$semestre->code." t ".
                        " where e.id_matiere=".$matiere->id." and e.id_type_evaluation=t.id";
        print_r("<br/>");print_r($req);print_r("<br/>");
        print_r("<br/>");print_r($evaluations);print_r("<br/>");print_r("<br/>");
*/
        if(!empty($this->input->post("enregistrer")))
        foreach ($auditeurs as $auditeur) {
            $moyen = 0;
            $bool = 0;
            foreach ($evaluations as $evaluation) {  
                if(!empty($this->input->post($evaluation->code.strval($auditeur->id)))){ 
                    $bool = 1;
                    $note_info = array("id_auditeur"=>$auditeur->id,"id_evaluation"=>$evaluation->id,"note"=>$this->Getfloat($this->input->post($evaluation->code.$auditeur->id)));
                    $ancien_notes = $this->model->getEntities("select * from notes_".$semestre->code." 
                        where id_evaluation=".$note_info["id_evaluation"]." and id_auditeur = ".$note_info["id_auditeur"]);
                    //if()
                    if (!empty($ancien_notes)) $ancien_notes = $ancien_notes[0];
                    if (!empty($ancien_notes)) {
                        $ancien_notes->note = $note_info["note"]; 
                        $this->model->update("notes_".$semestre->code,$ancien_notes->id,$ancien_notes); 
                    }
                    else {
                        $this->model->save("notes_".$semestre->code,$note_info);
                    }
                    $moyen = $moyen + ($evaluation->pourcentage/100)*$note_info["note"];
                }
            }
            if($bool == 1){
                $ancien_moyen = $this->model->getEntities("select * from moyenne_matiere_".$semestre->code." where id_matiere=".$matiere->id." and id_auditeur=".$auditeur->id);
                if (!empty($ancien_moyen)) $ancien_moyen = $ancien_moyen[0];
                if (!empty($ancien_moyen)) {
                    $ancien_moyen->moyenne = $moyen;
                    $this->model->update("moyenne_matiere_".$semestre->code,$ancien_moyen->id,$ancien_moyen); 
                }else {
                    $this->model->save("moyenne_matiere_".$semestre->code,array("moyenne"=>$moyen,"id_matiere"=>$matiere->id,"id_auditeur"=>$auditeur->id));
                } 
            }
        }
       //exit();
        $nb_auditeurs = $this->model->count_results_of_request($req_auditeurs);
         
         
        $lim = 25;        

        $offset = $this->uri->segment(3);
        if ($offset == null)
            $offset = 0;
        
        $auditeurs = $this->model->getEntities($req_auditeurs. " LIMIT $offset,$lim");
        $count = $offset;
        $liste_tableau = "";    

        //$evaluations = $this->model->getEntities("select * from evaluation_".$semestre->code." e,type_evaluation_".$semestre->code." t ".
        //                " where e.id_matiere=".$matiere->id." and e.id_type_evaluation=t.id group by t.id");
        //print_r($evaluations);
        $output_h = "";
        if(!empty($evaluations))
        foreach ($evaluations as $evaluation) {
            $output_h = $output_h."<th><center>".$evaluation->code."</center></th>";
        }
        $output_h = $output_h."<th><center>Moyenne</center></th>";
        if($offset+$lim < $nb_auditeurs)
            $output = form_open_multipart('notes/saisieNotes/'.strval($offset+$lim));
        else $output = form_open_multipart('notes/saisieNotes/'.strval($offset));
        $output = $output.
        '<table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Auditeurs</th>'.
                  $output_h
                .'</tr>
              </thead>
              <tbody>';
        $output2 = ""; 
        $count = $offset;             
        foreach ($auditeurs as $auditeur) {
            $count++;
            $output2 = $output2."<tr><td>$count</td><td>$auditeur->nom $auditeur->prenom</td>";
            if(!empty($evaluations))
            foreach ($evaluations as $evaluation) {
                $ancien_notes = $this->model->getEntities("select * from notes_$semestre->code 
                        where id_evaluation=".$evaluation->id." and id_auditeur = ".$auditeur->id);
                $req = "select * from notes_$semestre->code 
                        where id_evaluation=".$evaluation->id." and id_auditeur = ".$auditeur->id;
                /*print_r($req);        
                print_r($ancien_notes);
                print_r("<br/>");print_r("<br/>");*/
                //if()
                if (!empty($ancien_notes)) $ancien_notes = $ancien_notes[0];
                if (empty($ancien_notes))
                    $output2 = $output2.'<td><center><input   style="width:60px" type="text" name="'.$evaluation->code.$auditeur->id.'"></center></td>';
                else $output2 = $output2.'<td><center><input  value="'.$ancien_notes->note.'"  style="width:60px" type="text" name="'.$evaluation->code.$auditeur->id.'"></center></td>';
            }
            $ancien_moyen = $this->model->getEntities("select * from moyenne_matiere_".$semestre->code." where id_matiere=".$matiere->id." and id_auditeur=".$auditeur->id);
            if (!empty($ancien_moyen)) $ancien_moyen = $ancien_moyen[0];
            if (!empty($ancien_moyen)) {
                $output2 = $output2."<td><center>".$ancien_moyen->moyenne."</center></td>";    
            }else  $output2 = $output2.'<td></td>';

            $output2 = $output2."</tr>";
            
        }
        if($offset==0)
            $output=$output.$output2.'</tbody></table><input  type="submit" name="enregistrer" class="btn" value="Enrégistrer -> Suivant" /></form>';
        else if($offset>0 && $offset+$lim < $nb_auditeurs) $output=$output.$output2.'</tbody></table>'.anchor('notes/saisieNotes/'.strval($offset-$lim),'Précédent').'&nbsp;&nbsp;&nbsp;&nbsp;
                    <input  type="submit" name="enregistrer" class="btn" value="Enrégistrer -> Suivant" /></form>';
        else $output=$output.$output2.'</tbody></table>'.anchor('notes/saisieNotes/'.strval($offset-$lim),'Précédent').'&nbsp;&nbsp;&nbsp;&nbsp;
             <input  type="submit" name="enregistrer" class="btn" value="Enrégistrer" /></form>';
        $data["output"] = $output;
        $data["fil"] = "Année = ".anchor("notes/classe", $this->session->userdata('anne_academique'))." -> ".
                        "Classe = ".anchor("notes/semestre/$classe->id", $classe->code." : ".$classe->nom)." -> ".
                        "Semestre = ".anchor("notes/module/$semestre->id", $semestre->code." : ".$semestre->nom)." -> ".
                        "Matiere = ".anchor("notes/module/$semestre->id", $matiere->code." : ".$matiere->nom) ;
        $data["form"] = TRUE;                
        $data['titre'] = 'Saisie des notes'; 
        if($this->session->userdata('message')){
            $data['message'] = $this->session->userdata('message');
            $this->session->unset_userdata('message');
        }
       $this->layout->view('admin/notes/navigation_config_notes.php', $data); 
         
    }

    public function action_import_notes(){
        if(!$this->session->userdata('id_anne_academique')) redirect("welcome");
        if(!$this->session->userdata('classe_note')) redirect("notes/classe/".$this->session->userdata('id_anne_academique'));
        if(!$this->session->userdata('semestre_note')) redirect("notes/module/".$this->session->userdata('classe_note')->id);
        if(!$this->session->userdata('matiere_note')) redirect("notes/module/".$this->session->userdata('semestre_note')->id);

        $matiere = $this->session->userdata('matiere_note');
        $semestre = $this->session->userdata('semestre_note');
        $classe = $this->session->userdata('classe_note');

        $config = array();
        $config['upload_path'] = 'uploads';
        $config['allowed_types'] = 'xls|xlsx|csv|txt';
        $config['max_size'] = '204800';
        $config['max_filename'] = '100'; 

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file_notes'))
        {
            $error = $this->upload->display_errors(); 
            print_r($this->upload->data());
            $this->session->set_userdata('message', $error);
            redirect('notes/saisieNotes');
        }
        else
        {   //print_r($this->upload->data());
            $data = $this->upload->data();
            $csv = new SplFileObject($data['full_path']);
            $csv->setFlags(SplFileObject::READ_CSV);
            $csv->setCsvControl(';');
            //print_r($csv); 
             
            $return = "";
            $i=0; $j=0; $p=0;
            foreach($csv as $ligne){
                $i++;
                //print_r($ligne);
                if($i>1 && sizeof($ligne)>1){
                    $auditeur = $this->model->getEntities("select * from auditeurs where LOWER(matricule) = '".strtolower (trim((string)$ligne[1]))."'");
                    $evaluation = $this->model->getEntities("select * from evaluation_$semestre->code where id = ".(string)$ligne[0]." and id_matiere=".$matiere->id);
                    if(!empty(trim((string)$ligne[0])) && !empty(trim((string)$ligne[1])) && !empty(trim((string)$ligne[2])) && !empty($evaluation))  {  
                        if (!empty($auditeur)) $auditeur = $auditeur[0];
                        if (!empty($auditeur)){
                            $ancien_notes = $this->model->getEntities("select * from notes_".$semestre->code.
                                "  where id_evaluation=".trim($ligne[0])." and id_auditeur = ".$auditeur->id);
                            //if()
                            if (!empty($ancien_notes)) $ancien_notes = $ancien_notes[0];
                            if (!empty($ancien_notes)) {$ancien_notes->note = $this->Getfloat((string)$ligne[2]); $this->model->update("notes_".$semestre->code,$ancien_notes->id,$ancien_notes); }
                            else {
                                $note_info = array("note"=>$this->Getfloat((string)$ligne[2]),"id_auditeur"=>$auditeur->id,"id_evaluation"=>(string)$ligne[0]);
                                $this->model->save("notes_".$semestre->code,$note_info);
                            }
                            $j++;
                        }else {
                            $p++;
                            $return = $return.", ".(string)$i;
                        }
                    }else {
                        $p++;
                        $return = $return.", ".(string)$i;  
                    }    
                }
            }
            $message = "Nombre de lignes : ".$i;
            $message = $message."<br/>Nombre d'enrégistrement(s) : ".$j; 
            $message = $message."<br/>Nombre de rejet(s) : ".$p;
            $message = $message."<br/>Les lignes de rejet : ".$return;
            $this->session->set_userdata('message', $message);
            redirect('notes/saisieNotes');
        } 
    }

    public function consulter_notes_modules($id_module=0){
        if($id_module==0) redirect("welcome");
        if(!$this->session->userdata('id_anne_academique')) redirect("welcome");
        if(!$this->session->userdata('classe_note')) redirect("notes/classe/".$this->session->userdata('id_anne_academique'));
        if(!$this->session->userdata('semestre_note')) redirect("notes/module/".$this->session->userdata('classe_note')->id);

        $matiere = $this->session->userdata('matiere_note');
        $semestre = $this->session->userdata('semestre_note');
        $classe = $this->session->userdata('classe_note');
        $module = $this->model->getEntities("select * from modules_".$semestre->code." where id=".$id_module)[0];
        if(empty($module))redirect("notes/module/".$this->session->userdata('classe_note')->id);

        $req_auditeurs = "select * from auditeurs where id_classe=".$this->session->userdata('id_anne_academique')." order by nom,prenom";
        $auditeurs = $this->model->getEntities($req_auditeurs);

        $nb_auditeurs = $this->model->count_results_of_request($req_auditeurs);
         
        $lim = 25;        

        $offset = $this->uri->segment(4);
        if ($offset == null)
            $offset = 0;
        
        $auditeurs = $this->model->getEntities($req_auditeurs. " LIMIT $offset,$lim");
        $count = $offset;
        $liste_tableau = "";    

        $output = "";
        $matieres = $this->model->getEntities("select * from matiere_".$semestre->code." where id_module=".$id_module);
        if(!empty($matieres))
        foreach ($matieres as $matiere) {    
            $evaluations = $this->model->getEntities("select t.* from evaluation_".$semestre->code." e,type_evaluation_".$semestre->code." t ".
                        " where e.id_matiere=".$matiere->id." and e.id_type_evaluation=t.id");
            if(!empty($evaluations))  
            foreach ($evaluations as $evaluation) {
                $output = $output."<th><center>".$evaluation->code."</center></th>";
            }
            $output = $output."<th><center>".$matiere->code."</center></th>";
        }
        $output = '<table class="table">
              <thead>
                <tr>
                    <th>#</th>
                  <th>Auditeurs</th>'.
                  $output
                .'<th>Moyenne</th></tr>
              </thead>
              <tbody>';
        $output2 = ""; 
        $count = $offset;             
        foreach ($auditeurs as $auditeur) {
            $count++;
            $output2 = $output2."<tr><td>$count</td><td>$auditeur->nom $auditeur->prenom</td>";
            $matieres = $this->model->getEntities("select * from matiere_".$semestre->code." where id_module=".$id_module);
            $moyenne = 0;
            $nb_mat = 0;
            if(!empty($matieres))   
            foreach ($matieres as $matiere) {  
                $nb_mat++;   
                $evaluations = $this->model->getEntities("select t.* 
                            from evaluation_".$semestre->code." e,type_evaluation_".$semestre->code." t ".
                            " where e.id_matiere=".$matiere->id." and e.id_type_evaluation=t.id group by t.id"); 
                if(!empty($evaluations)) 
                foreach ($evaluations as $evaluation) {
                    $ancien_notes = $this->model->getEntities("select * from notes_".$semestre->code." n, evaluation_".$semestre->code.  
                        " e where n.id_evaluation=e.id and e.id_type_evaluation=".$evaluation->id." and n.id_auditeur = ".$auditeur->id." and e.id_matiere=".$matiere->id);
                    if (!empty($ancien_notes)) $ancien_notes = $ancien_notes[0];
                    if (!empty($ancien_notes)) {
                        $output2 = $output2."<td><center>".$ancien_notes->note."</center></td>";
                    }else $output2 = $output2."<td></td>";
                }
                $ancien_moyenne = $this->model->getEntities("select * from moyenne_matiere_".$semestre->code." n
                     where  n.id_auditeur = ".$auditeur->id." and n.id_matiere=".$matiere->id);
                    if (!empty($ancien_moyenne)) $ancien_moyenne = $ancien_moyenne[0];
                    if (!empty($ancien_moyenne)) {
                        $output2 = $output2."<td><center>".$ancien_moyenne->moyenne."</center></td>";
                        $moyenne = $moyenne + $ancien_moyenne->moyenne;
                    }else $output2 = $output2."<td></td>";
            }
            $output2 = $output2."<td><center>".$moyenne/$nb_mat ."</center></td></tr>";
        }
        $output=$output.$output2."</tbody></table>";
        if($offset==0)
            $output=$output.anchor("notes/consulter_notes_modules/$id_module/".strval($offset+$lim),'Suivant')."&nbsp;&nbsp;";
        else if($offset>0 && $offset+$lim < $nb_auditeurs) 
            $output=$output.anchor("notes/consulter_notes_modules/$id_module/".strval($offset-$lim),'Précédent')."&nbsp;&nbsp;&nbsp;&nbsp;".
                        anchor("notes/consulter_notes_modules/$id_module/".strval($offset+$lim),'Suivant');
        else $output=$output.anchor("notes/consulter_notes_modules/$id_module/".strval($offset-$lim),'Précédent').'&nbsp;&nbsp;&nbsp;&nbsp;';


        $output = anchor("notes/export_csv/$id_module/","<img src='".base_url()."/resources/admin/images/export_excel.png' style='width:15px; height:15px' />Exporter",array("style"=>"float:right")).'<br/>'.$output;
        $data["output"] = $output;
        $data["fil"] = "Année = ".anchor("notes/classe", $this->session->userdata('anne_academique'))." -> ".
                        "Classe = ".anchor("notes/semestre/$classe->id", $classe->code." : ".$classe->nom)." -> ".
                        "Semestre = ".anchor("notes/module/$semestre->id", $semestre->code." : ".$semestre->nom)." -> ".
                        "Module = ".anchor("notes/module/$semestre->id", $matiere->code." : ".$matiere->nom) ;
        //$data["form"] = TRUE;                
        $data['titre'] = 'Recapitulatif des notes du module '.$module->code.": ".$module->nom; 
        if($this->session->userdata('message')){
            $data['message'] = $this->session->userdata('message');
            $this->session->unset_userdata('message');
        }
       $this->layout->view('admin/notes/recap_notes_semestre_view.php', $data); 

    }

    public function export_csv($id_module=0){
        include "simple_html_dom.php";
        
        if($id_module==0) redirect("welcome");
        if(!$this->session->userdata('id_anne_academique')) redirect("welcome");
        if(!$this->session->userdata('classe_note')) redirect("notes/classe/".$this->session->userdata('id_anne_academique'));
        if(!$this->session->userdata('semestre_note')) redirect("notes/module/".$this->session->userdata('classe_note')->id);
        //if(!$this->session->userdata('matiere_note')) redirect("notes/module/".$this->session->userdata('semestre_note')->id);

        $matiere = $this->session->userdata('matiere_note');
        $semestre = $this->session->userdata('semestre_note');
        $classe = $this->session->userdata('classe_note');
        $module = $this->model->getEntities("select * from modules_".$semestre->code." where id=".$id_module)[0];
        if(empty($module))redirect("notes/module/".$this->session->userdata('classe_note')->id);

        $req_auditeurs = "select * from auditeurs where id_classe=".$this->session->userdata('id_anne_academique')." order by nom,prenom";
        $auditeurs = $this->model->getEntities($req_auditeurs);

        $nb_auditeurs = $this->model->count_results_of_request($req_auditeurs);
        
        $auditeurs = $this->model->getEntities($req_auditeurs);
        $count = 0;
        $liste_tableau = "";    

        $output = "";
        $matieres = $this->model->getEntities("select * from matiere_".$semestre->code." where id_module=".$id_module);
        if(!empty($matieres))
        foreach ($matieres as $matiere) {    
            $evaluations = $this->model->getEntities("select t.* from evaluation_".$semestre->code." e,type_evaluation_".$semestre->code." t ".
                        " where e.id_matiere=".$matiere->id." and e.id_type_evaluation=t.id");
            if(!empty($evaluations))  
            foreach ($evaluations as $evaluation) {
                $output = $output."<th>".$evaluation->code."</th>";
            }
            $output = $output."<th>".$matiere->code."</th>";
        }
        $output = '<table class="table">              
                <tr>
                    <th>Matricules</th>
                  <th>Auditeurs</th>'.
                  $output
                .'<th>Moyenne</th></tr>';
        $output2 = ""; 
        $count = 0;             
        foreach ($auditeurs as $auditeur) {
            $count++;
            $output2 = $output2."<tr><td>$auditeur->matricule</td><td>$auditeur->nom $auditeur->prenom</td>";
            $matieres = $this->model->getEntities("select * from matiere_".$semestre->code." where id_module=".$id_module);
            $moyenne = 0;
            $nb_mat = 0;
            if(!empty($matieres))   
            foreach ($matieres as $matiere) {  
                $nb_mat++;   
                $evaluations = $this->model->getEntities("select t.* 
                            from evaluation_".$semestre->code." e,type_evaluation_".$semestre->code." t ".
                            " where e.id_matiere=".$matiere->id." and e.id_type_evaluation=t.id group by t.id"); 
                if(!empty($evaluations)) 
                foreach ($evaluations as $evaluation) {
                    $ancien_notes = $this->model->getEntities("select * from notes_".$semestre->code." n, evaluation_".$semestre->code.  
                        " e where n.id_evaluation=e.id and e.id_type_evaluation=".$evaluation->id." and n.id_auditeur = ".$auditeur->id." and e.id_matiere=".$matiere->id);
                    if (!empty($ancien_notes)) $ancien_notes = $ancien_notes[0];
                    if (!empty($ancien_notes)) {
                        $output2 = $output2."<td>".$ancien_notes->note."</td>";
                    }else $output2 = $output2."<td></td>";
                }
                $ancien_moyenne = $this->model->getEntities("select * from moyenne_matiere_".$semestre->code." n
                     where  n.id_auditeur = ".$auditeur->id." and n.id_matiere=".$matiere->id);
                    if (!empty($ancien_moyenne)) $ancien_moyenne = $ancien_moyenne[0];
                    if (!empty($ancien_moyenne)) {
                        $output2 = $output2."<td>".$ancien_moyenne->moyenne."</td>";
                        $moyenne = $moyenne + $ancien_moyenne->moyenne;
                    }else $output2 = $output2."<td></td>";
            }
            $output2 = $output2."<td>".$moyenne/$nb_mat ."</td></tr>";
        }
        $table=$output.$output2."</table>";




        $html = str_get_html($table);
        //print_r($html);


        header('Content-type: application/ms-excel');
        header('Content-Disposition: attachment; filename=sample.csv');

        $fp = fopen("php://output", "w+");

        fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
        fputs($fp,'Recapitulatif des notes du module '.$module->code.": ".$module->nom);  fputs($fp, "\n");
        $delimiteur = ';';

        foreach($html->find('tr') as $element)
        {
            $td = array(); $std = "";
            foreach( $element->find('th') as $row)  { $td [] = $row->plaintext;   }
            if(!empty($td)){
                foreach ($td as $t) {
                    $std = $std.$t.";";
                }
                fputs($fp, $std);
                //fputcsv($fp, $td, $delimiteur);
            } 
            $td = array();
            foreach( $element->find('td') as $row) {  $td [] = $row->plaintext; }
            if(!empty($td)){
                foreach ($td as $t) {
                    $std = $std.$t.";";
                }
                fputs($fp, $std);
                //fputcsv($fp, $td, $delimiteur);
            } 
            fputs($fp, "\n");
        }
        fclose($fp);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */