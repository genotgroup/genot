<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class EtatDossier extends CI_Controller {

    /**
     * Example: DOMPDF 
     *
     * Documentation: 
     * http://code.google.com/p/dompdf/wiki/Usage
     *
     */
    function __construct() {
        parent::__construct();

        // Load CI benchmark and memory usage profiler.
        if (1 == 2) {
            $sections = array(
                'benchmarks' => TRUE, 'memory_usage' => TRUE,
                'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE,
                'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
            );
            $this->output->set_profiler_sections($sections);
            $this->output->enable_profiler(TRUE);
        }

        // Load CI libraries and helpers.
        $this->load->database();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('Model_print', 'model', TRUE);

        // IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
        // It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
        $this->auth = new stdClass;

        // Load 'lite' flexi auth library by default.
        // If preferable, functions from this library can be referenced using 'flexi_auth' as done below.
        // This prevents needing to reference 'flexi_auth_lite' in some files, and 'flexi_auth' in others, everything can be referenced by 'flexi_auth'.
        $this->load->library('flexi_auth_lite', FALSE, 'flexi_auth');

        // Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
        $this->load->vars('base_url', "http://" . $_SERVER['HTTP_HOST'] . "/dossier/");
        $this->load->vars('includes_dir', "http://" . $_SERVER['HTTP_HOST'] . "/dossier/includes/");
        $this->load->vars('current_url', $this->uri->uri_to_assoc(1));
        if (!$this->flexi_auth->is_logged_in_via_password() && uri_string() != 'auth/logout') {
            redirect('auth/index');
        }
        $this->data = null;
    }

    public function index() {
        redirect('welcome');
        // Load all views as normal 
//        $data['contents'] = 'welcome';
//        $html = $this->load->view('print/print_listdossier', $data, TRUE);
//        print_r($html);
        // Get output html
//        $html = $this->output->get_output();
//
//        // Load library
//        $this->load->library('dompdf_gen');
//
//        // Convert to PDF
//        $this->dompdf->load_html($html);
//        $this->dompdf->render();
//        $this->dompdf->stream("welcome.pdf");
    }

    public function etat_list_dossier() {


        $id_user = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
        $id_user = $id_user[0]->uacc_id;
		$req_calcul_nb_heure_passee = "";
        $req_calcul_nb_heure_passee = "TIMESTAMPDIFF(DAY, d.date_creation, d.date_dernier_modif) AS nbjour, ";
        $req_calcul_nb_heure_passee .= " TIMESTAMPDIFF(DAY, d.date_creation, CURDATE()) AS nbjour_si_traite ";
        $req = 'SELECT d.date_creation,c.id_utilisateur_arrive,d.code_dossier,d.objet,d.statut,' . $req_calcul_nb_heure_passee .
                'FROM `dossiers` d , `cotation_dossier` c
            WHERE d.id = c.id_dossier and (d.id_utilisateur = ' . $id_user . ' or 
               c.id_utilisateur_depart=' . $id_user . ' or c.id_utilisateur_arrive = ' . $id_user . ' )
            GROUP BY d.id';
        $list = $this->model->getEntities($req);

//        print_r($list);
        $this->createpdf($list);
    }

    public function etat_dossier_admin() {
        if ($this->flexi_auth->is_privileged('Print Global')) {
			$req_calcul_nb_heure_passee = "";
            $req_calcul_nb_heure_passee = "TIMESTAMPDIFF(DAY, d.date_creation, d.date_dernier_modif) AS nbjour, ";
            $req_calcul_nb_heure_passee .= " TIMESTAMPDIFF(DAY, d.date_creation, CURDATE()) AS nbjour_si_traite ";
            $req = 'SELECT d.date_creation,c.id_utilisateur_arrive,d.code_dossier,d.objet,d.statut,' . $req_calcul_nb_heure_passee .
                    'FROM `dossiers` d , `cotation_dossier` c
            WHERE d.id = c.id_dossier';
			
			//print_r($req."  ");
            $list = $this->model->getEntities($req);  
            //print_r($list);
			$this->createpdf($list);
        } else { 
            redirect('/');
        }
    }

    public function createpdf($liste) {
        //$data['liste'] = $liste;
        $data['titre'] = 'Liste des dossiers';

        $entete = ' <table  cellpadding="5" cellspacing="0" style="border-color: #000;border-collapse:collapse;text-align: left;width: 100%;font-size:12px;">
            <tr style="text-align: center;" >
                <th>&nbsp;N� </th>
                <th>&nbsp;Code </th>
                <th>&nbsp;Objet</th>
                <th>&nbsp;Date Enr</th>
                <th>&nbsp;Dur�e(JOUR)</th>
                <th>&nbsp;Utilisateur Cot�</th>
                <th>&nbsp;Etat</th>

            </tr>';
        $content = "";
        if (count($liste) > 0) {
            $n = 0;
            foreach ($liste as $dossier) {
                $Weddingdate = new DateTime($dossier->date_creation);
                $formattedweddingdate = date_format($Weddingdate, 'd-m-Y');
                $statu = $dossier->statut?"Trait�":"En cours";
//                $user = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
//                $user_org = $user[0]->upro_first_name . " " . $user[0]->upro_last_name;
                $user = $this->flexi_auth->get_user_by_id($dossier->id_utilisateur_arrive)->result();
                $user_u = $user[0]->upro_first_name . " " . $user[0]->upro_last_name;
				$nbjour = $dossier->statut?$dossier->nbjour:$dossier->nbjour_si_traite;
                $content = $content . '<tr>
                                                                    <td>' . ++$n . '</td>
                                                                    <td>' . $dossier->code_dossier . '</td>
                                                                    <td>' . $dossier->objet . '</td>
                                                                    <td>' . $formattedweddingdate . '</td>
                                                                    <td >' . $nbjour . '</td>
                                                                    <td>' . $user_u . '</td>								
                                                            <td>' . $statu . '</td></tr>';
            }
        }
        if (count($liste) > 0)
            $last_lign = '<tr><td colspan="7">Nombres de dossiers :  <strong><?php echo count($liste); ?></strong></td></tr></table>';
        else
            $last_lign = '<tr><td colspan="7"> <strong>Aucun dossier enr�gistr�</strong></td></tr></table>';

        $data['tableau'] = utf8_decode($entete . $content . $last_lign) ; 
	
        $html = $this->load->view('print/print_listdossier', $data, TRUE);

//        print_r($req);
//        print_r($html);

        $this->load->library('dompdf_gen');

        // Convert to PDF
        //$html = utf8_encode($html);
        $this->dompdf->load_html($html);
        $this->dompdf->render();
        $this->dompdf->stream("list_dossier.pdf");
    }

}
