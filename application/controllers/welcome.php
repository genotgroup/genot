<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
        //$this->load->library('dossier_lib');
		 $this->load->model('model_generic', 'model', TRUE);
		// Load CI benchmark and memory usage profiler.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		 
		// Load CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'lite' flexi auth library by default.
		// If preferable, functions from this library can be referenced using 'flexi_auth' as done below.
		// This prevents needing to reference 'flexi_auth_lite' in some files, and 'flexi_auth' in others, everything can be referenced by 'flexi_auth'.
		$this->load->library('flexi_auth');	
        //$this->load->library('dossier_cotation', FALSE, 'dossiers');
                
		// Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
		$this->load->vars('base_url', "http://" . $_SERVER['HTTP_HOST'] . "/genot/");
		$this->load->vars('includes_dir', "http://" . $_SERVER['HTTP_HOST'] . "/genot/includes/");
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		if (!$this->flexi_auth->is_logged_in_via_password() && uri_string() != 'auth/logout') 
		{
			redirect('auth/index');
		}
		$this->data = null;
        $this->layout->set_theme('admin');
	}
	
        
            /**
             * Fonction qui calcul la différence de temps entre deux intervales de temps
             * @param type $start
             * @param type $end
             * @return type
             */
            function date_getFullTimeDifference( $start, $end )
            {
            $uts['start']      =    strtotime( $start );
                    $uts['end']        =    strtotime( $end );
                    if( $uts['start']!==-1 && $uts['end']!==-1 )
                    {
                        if( $uts['end'] >= $uts['start'] )
                        {
                            $diff    =    $uts['end'] - $uts['start'];
                            if( $years=intval((floor($diff/31104000))) )
                                $diff = $diff % 31104000;
                            if( $months=intval((floor($diff/2592000))) )
                                $diff = $diff % 2592000;
                            if( $days=intval((floor($diff/86400))) )
                                $diff = $diff % 86400;
                            if( $hours=intval((floor($diff/3600))) )
                                $diff = $diff % 3600;
                            if( $minutes=intval((floor($diff/60))) )
                                $diff = $diff % 60;
                            $diff    =    intval( $diff );
                            
                            
                            return( array('years'=>$years,'months'=>$months,'days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
                        }
                        else
                        {
                            echo "Ending date/time is earlier than the start date/time";
                        }
                    }
                    else
                    {
                        echo "Invalid date/time data detected";
                    }
            }
            

        
	public function index()
	{
            $req = "SELECT * from anne_academique WHERE en_cour=1";
            $anne_academique = $this->model->getEntities($req);
            if(isset($anne_academique) and !empty($anne_academique)) {
                $this->session->set_userdata('id_anne_academique', $anne_academique[0]->id);
                $this->session->set_userdata('anne_academique', $anne_academique[0]->annee);
                $this->session->set_userdata('anne_academique_precedente', $anne_academique[0]->id_precedente);
                //$data['contents']='welcome';
                $this->layout->view('welcome', $this->data);
            }else{
                redirect('mainCrud/anneAcademique');
            } 
    }
             
    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
