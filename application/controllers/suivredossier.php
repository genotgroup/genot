<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suivredossier extends CI_Controller {

	function __construct() 
    {
        parent::__construct();
		
                $this->load->library(array('table', 'form_validation'));
                $this->load->helper('url');
                $this->load->helper('date');
                $this->load->library('security');
                $this->load->library('dossier_lib');
//                $this->load->library('utility_lib');
                $this->load->helper('download');
        //        $this->load->library('candidature_lib');
                $this->load->library('session');
                $this->load->model('model_suivi_dossier', '', TRUE);
        
		// Load CI benchmark and memory usage profiler.
		if (1==2) 
		{
			$sections = array(
				'benchmarks' => TRUE, 'memory_usage' => TRUE, 
				'config' => FALSE, 'controller_info' => FALSE, 'get' => FALSE, 'post' => FALSE, 'queries' => FALSE, 
				'uri_string' => FALSE, 'http_headers' => FALSE, 'session_data' => FALSE
			); 
			$this->output->set_profiler_sections($sections);
			$this->output->enable_profiler(TRUE);
		}
		
		// Load CI libraries and helpers.
		$this->load->database();
		$this->load->library('session');
 		$this->load->helper('url');

  		// IMPORTANT! This global must be defined BEFORE the flexi auth library is loaded! 
 		// It is used as a global that is accessible via both models and both libraries, without it, flexi auth will not work.
		$this->auth = new stdClass;
		
		// Load 'lite' flexi auth library by default.
		// If preferable, functions from this library can be referenced using 'flexi_auth' as done below.
		// This prevents needing to reference 'flexi_auth_lite' in some files, and 'flexi_auth' in others, everything can be referenced by 'flexi_auth'.
		$this->load->library('flexi_auth_lite', FALSE, 'flexi_auth');	
                $this->load->library('dossier_cotation', FALSE, 'dossiers');
                
		// Note: This is only included to create base urls for purposes of this demo only and are not necessarily considered as 'Best practice'.
		$this->load->vars('base_url', "http://" . $_SERVER['HTTP_HOST'] . "/dossier/");
		$this->load->vars('includes_dir', "http://" . $_SERVER['HTTP_HOST'] . "/dossier/includes/");
		$this->load->vars('current_url', $this->uri->uri_to_assoc(1));
		if (!$this->flexi_auth->is_logged_in_via_password() && uri_string() != 'auth/logout') 
		{
			redirect('auth/index');
		}
		$this->data = null;
	}
	
	public function index()
	{
		redirect('dossiers/liste');
	}
	

            /**
        * Fonction qui retourne la liste de tous les demandes d'agréments
        */
       public function suiviliste($id_d){
            //on compte le nombre de candidats
           $nbagrements = $this->model_suivi_dossier->count_all('cotation_dossier');
           //Chargement de la librairie pagination
           

            $lien_controleur = site_url('dossiers');
           //Initialise le nombre total d'enregistrements
         


           $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
           $config['full_tag_close'] = '</ul>';

           $config['first_link'] = '&laquo; First';
           $config['first_tag_open'] = '<li class="prev page">';
           $config['first_tag_close'] = '</li>';

           $config['last_link'] = 'Last &raquo;';
           $config['last_tag_open'] = '<li class="next page">';
           $config['last_tag_close'] = '</li>';

           $config['next_link'] = 'Next &rarr;';
           $config['next_tag_open'] = '<li class="next page">';
           $config['next_tag_close'] = '</li>';

           $config['prev_link'] = '&larr; Previous';
           $config['prev_tag_open'] = '<li class="prev page">';
           $config['prev_tag_close'] = '</li>';

           $config['cur_tag_open'] = '<li class="active"><a href="">';
           $config['cur_tag_close'] = '</a></li>';

           $config['num_tag_open'] = '<li class="page">';
           $config['num_tag_close'] = '</li>';

          $list_dossier_cote = $this->model_suivi_dossier->getEntities("SELECT * FROM cotation_dossier c, dossiers d WHERE c.`id_dossier` = $id_d and c.`id_dossier` = d.`id` ORDER BY c.date_cotation  DESC ");
           
           $liste_tableau ="";
           $dossier= $this->model_suivi_dossier->get_by_id("dossiers", $id_d);
           $data['code_dossier']=$dossier[0]->code_dossier;    
           
            $count = 0;
            foreach ($list_dossier_cote as $dossier) {
                $count++;
                
                //selection des info des utilisateurs".$agrement
                $user_origine  = $this->model_suivi_dossier->getEntity("SELECT p.`upro_first_name` , p.`upro_last_name` FROM user_profiles p WHERE  p.`upro_uacc_fk` =   ".$dossier->id_utilisateur_depart);
                $user_cible  = $this->model_suivi_dossier->getEntity("SELECT p.`upro_first_name` , p.`upro_last_name` FROM user_profiles p WHERE  p.`upro_uacc_fk` =   ".$dossier->id_utilisateur_arrive);
                
                $liste_tableau .="<tr><td style=\"width: 40px;\" > $count</td>
                         <td style=\"width: 200px;\"> $user_origine->upro_last_name $user_origine->upro_first_name </td>
                         <td style=\"width: 200px;\">$user_cible->upro_last_name  $user_cible->upro_first_name</td>
                        <td style=\"width: 50px;\"> $dossier->duree </td>
                        <td style=\"width: 90px;\">$dossier->date_cotation </td>
                </tr>
                         ";
            }
             $data['tableau']=$liste_tableau;
             $data['total_collaboraateur']=$count;
           $this->load->view('dossier_administratif/suivi_dossier_liste', $data);
       } 
       
       public function voire_contation($id_cot=0,$id_dos=0){
           if($id_cot!=0 and $id_dos!=0){
               $cot = $this->model_suivi_dossier->get_by_id('cotation_dossier',$id_cot);
               if(isset($cot)){
                   $this->model_suivi_dossier->update_Where('cotation_dossier','id',$id_cot,array('dossier_lu' => TRUE));
               
                   redirect('dossiers/detail/'.$id_dos);
               }
           }  else {
               redirect('welcome');
            }
       }
        
}


