<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Site_model
 *
 * @author Anatole ABE
 */

class model_generic extends CI_Model{
    //put your code here
    function __construct() {
        parent::__construct();
    }
    
	function list_all($table){
		$q = $this->db->get($table);
                return $this->conversion($q);
	}
	
	function count_all($table){
		return $this->db->count_all($table);
	}
        
	function count_with_criteria($table, $where){
                $req = "select count(id) as total from $table where $where";
                $query = $this->db->query($req);
                return $query->row();
	}
        /**
         * Compte le nombre de résultat d'une requette
         * @param type $req
         * @return type
         */
	function count_results_of_request($req){                
                $query = $this->db->query($req);
                return $query->num_rows();
	}
	
	function get_paged_list($table, $limit = 10, $offset = 0){
		 $this->db->order_by('id','desc');
               $q = $this->db->get($table, $limit, $offset);
               return $this->conversion($q);
	}
	
	function get_by_id($table,$id){
		$this->db->where('id', $id);
		$q = $this->db->get($table);
                return $this->conversion($q);
	}
        
	function get_where_attrib($table, $attrib, $value){
		$this->db->where($attrib, $value);
		$q = $this->db->get($table);
                return $this->conversion($q);
	}
	
	function save($table,$object){
		$this->db->insert($table, $object);
		return $this->db->insert_id();
	}
	
	function update($table, $id, $object){
		$this->db->where('id', $id);
		$this->db->update($table, $object);
	}
        
	function update_Where($table, $attrib, $value, $object){
		$this->db->where($attrib, $value);
		$this->db->update($table, $object);
	}
	
	function delete($table,$id){
		$this->db->where('id', $id);
		$this->db->delete($table);
	}
        
        function getEntities($req){
           return $this->conversion($this->db->query($req));
//             return $this->db->query($req);
//             return $this->conversion($q);
            
        }
        function getEntity($req){
            return $this->db->query($req)->row();
//           return $this->conversion($this->db->query($req));
        }

        function execQuery($req){
        	return $this->db->query($req);
        }
        
        function countEntities($req){
          $query = $this->db->query($req);
           return $query->num_rows();
        }
        
        function conversion($q){
            if ($q->num_rows()>0){
                    
                    foreach ($q->result() as $row){
                        
                        $data[]=$row;
                    }
                    return $data;
                }
        }
}

?>
