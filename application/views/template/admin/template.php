<!DOCTYPE html>
<html lang="en">
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>GENOT PSSFP - Programme Supérieur de Spécialisation en Finances Publiques</title>
        <link type="text/css" href="<?php echo base_url(); ?>resources/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="<?php echo base_url(); ?>resources/admin/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="<?php echo base_url(); ?>resources/admin/css/theme.css" rel="stylesheet">
        <link type="text/css" href="<?php echo base_url(); ?>resources/admin/images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
              rel='stylesheet'>
        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png" />
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse"><i class="icon-reorder shaded"></i></a>
                    <a class="brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>/resources/admin/images/home.png"/>GENOT</a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav nav-icons">
                            <li class="active"><a href="#"><i class="icon-envelope"></i></a></li>
                            <li><a href="#"><i class="icon-eye-open"></i></a></li>
                            <li><a href="#"><i class="icon-bar-chart"></i></a></li>
                        </ul>
                        <form class="navbar-search pull-left input-append" action="#">
                            <input type="text" class="span3">
                            <button class="btn" type="button">
                                <i class="icon-search"></i>
                            </button>
                        </form>
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <?php echo anchor('welcome', '<i class="icon-eye-open"></i>Public', array()); ?>
                            </li>
                            <li>&nbsp;&nbsp;&nbsp;</li>
                            <?php
                            if ($this->flexi_auth->is_logged_in()) {
                                $data = $this->flexi_auth->user_data_view();
                                ?>
                                <li class="nav-user dropdown">
                                    <?php
                                    if (isset($data))
                                        echo anchor('', '<img src="' . base_url() . 'resources/admin/images/user.png" class="nav-avatar" />
                                     ' . $data['uacc_username'], array('class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'));
                                    ?> 
                                    <ul class="dropdown-menu">
                                        <li><?php echo anchor('auth_public/view_data_profile', 'Profile'); ?></li>
                                        <!--<li><a href="#">Account Settings</a></li>-->
                                        <li class="divider"></li>
                                        <li><?php echo anchor('auth/logout', 'Deconnexion'); ?></li>
                                    </ul>
                                </li>
<?php } ?>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><?php if($this->session->userdata('anne_academique'))$msg = '<i class="menu-icon icon-dashboard"></i>Tableau de Bord  '.$this->session->userdata('anne_academique');
                                                         else $msg = '<i class="menu-icon icon-dashboard"></i>Tableau de Bord'   
                                                      ?>
<?php echo anchor('welcome',$msg , array()); ?>
                                </li> 
                            </ul>
                             
                            <ul class="widget widget-menu unstyled"> 
                                   
                                <li>
                                    <?php echo anchor('notes/', '<i class="menu-icon icon-bold"></i>Gestion des Notes', array()); ?>
                                </li>
                                <li>
                                    <?php echo anchor('semestre/semestre_rud', '<i class="menu-icon icon-bar-chart"></i>Parcours et Semestres', array()); ?>
                                </li>
                                <li>
                                    <?php echo anchor('mainCrud/auditeurs', '<i class="menu-icon icon-table"></i>Gestion des auditeurs', array()); ?>
                                </li>
                                <li>
                                    <?php echo anchor('mainCrud', '<i class="menu-icon icon-cog"></i>
                                    <i class="icon-chevron-down pull-right"></i>Configuration', array()); ?>
                                </li>
                                 
                            </ul>
                            <!--/.widget-nav-->


                            <ul class="widget widget-menu unstyled">
                                <?php if ($this->flexi_auth->is_admin()) { ?>   
                                <li>
                                    <?php echo anchor('auth_admin/manage_user_accounts', '<i class="icon-inbox"></i>Gerer les compte utilisateur', array()); ?>
                                </li>
                                <li>
                                    <?php echo anchor('auth_admin/manage_user_groups', '<i class="icon-inbox"></i>Gerer les groupes', array()); ?>
                                </li>
                                <li>
                                    <?php echo anchor('auth_admin/manage_privileges', '<i class="icon-inbox"></i>Gerer les Privileges', array()); ?>
                                </li>
                                <li>
                                    <?php echo anchor('auth_admin/creation_account', '<i class="icon-inbox"></i>Creer un nouveau compte', array()); ?>
                                </li>
                                <?php } ?>
<!--                                        <li><a href="<?php echo base_url(); ?>auth_admin/list_user_status/active">List Active Users</a> </li>
                <li><a href="<?php echo base_url(); ?>auth_admin/list_user_status/inactive">List Inactive Users</a> </li>
                <li><a href="<?php echo base_url(); ?>auth_admin/delete_unactivated_users">List Unactivated Users</a> </li>
                <li><a href="<?php echo base_url(); ?>auth_admin/failed_login_users">List Failed Logins</a></li>-->

                            </ul>
                            <!--/.widget-nav-->
                            <ul class="widget widget-menu unstyled">
                                <li><?php echo anchor('auth_public/view_data_profile', '<i class="icon-inbox"></i>Mon Profile'); ?></li>
                                <li><?php echo anchor('auth/logout', '<i class="menu-icon icon-signout"></i>Deconnexion'); ?></li>
                            </ul>
                        </div>
                        <!--/.sidebar-->
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <div class="content">
                            <?php
                            if (isset($content)) {
                                echo $content;
                            }
                            ?>
                        </div>
                        <!--/.content-->
                    </div>
                    <!--/.span9-->
                </div>
            </div>
            <!--/.container-->
        </div>
        <!--/.wrapper-->
        <div class="footer">
            <div class="container">
                <b class="copyright">&copy; 2014 Edmin - EGrappler.com </b>All rights reserved.
            </div>
        </div>
<?php if (!isset($output)) { ?>    
            <script src="<?php echo base_url(); ?>resources/admin/scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>resources/admin/scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>resources/admin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>resources/admin/scripts/flot/jquery.flot.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>resources/admin/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>resources/admin/scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
            <script src="<?php echo base_url(); ?>resources/admin/scripts/common.js" type="text/javascript"></script>
<?php } ?> 
    </body>
