
			<!-- Slider Carousel
			================================================== -->
        	<div class="flexslider">
              <ul class="slides">
                <li><a href="gallery-single.htm"><img src="<?php echo base_url(); ?>resources/img/gallery/slider-img-1.jpg" alt="slider" /></a></li>
                <li><a href="gallery-single.htm"><img src="<?php echo base_url(); ?>resources/img/gallery/slider-img-2.jpg" alt="slider" /></a></li>
                <li><a href="gallery-single.htm"><img src="<?php echo base_url(); ?>resources/img/gallery/slider-img-1.jpg" alt="slider" /></a></li>
                <li><a href="gallery-single.htm"><img src="<?php echo base_url(); ?>resources/img/gallery/slider-img-1.jpg" alt="slider" /></a></li>
                <li><a href="gallery-single.htm"><img src="<?php echo base_url(); ?>resources/img/gallery/slider-img-1.jpg" alt="slider" /></a></li>
              </ul>
            </div>

			<!-- Blog Posts
        ================================================== -->
	<h4 class="title-bg ab-blog-magin-top"><a href="blog-single.htm">Les dernières innovations</a></h4>
        <div class="blog ">

            <!-- Blog Post 1 -->
            <?php echo $lates_innovations; ?>
            
            
            
            
            <!-- Blog Post 1 -->
            <article class="clearfix">
                <a href="blog-single.htm"><img src="<?php echo base_url(); ?>resources/img/gallery/gallery-img-1-4col.jpg" alt="Post Thumb" class="align-left"></a>
                <h4 class="title-bg"><a href="blog-single.htm">A subject that is beautiful in itself</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus fermentum ipsum molestie. </p>
                    <div class="post-summary-footer">
                        <ul class="post-data-3">
                            <li><button class="btn btn-mini btn-inverse" type="button">Read more</button></li>
                            <li><i class="icon-calendar"></i> 09/04/15</li>
                            <li><i class="icon-user"></i> <a href="#">Pr. Daniel TOGMA et All, Université de Douala</a></li>
                            <li><i class="icon-calendar"></i> 09/04/15</li> &nbsp;&nbsp;&nbsp;<i class="icon-eye-open"></i> <a href="#">5 374 </a></li>
                        </ul>
                    </div>
            </article>

            <!-- Blog Post 1 -->
            <article class="clearfix">
                <a href="blog-single.htm"><img src="<?php echo base_url(); ?>resources/img/gallery/gallery-img-1-4col.jpg" alt="Post Thumb" class="align-left"></a>
                <h4 class="title-bg"><a href="blog-single.htm">A great artist is always before his time</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus fermentum ipsum molestie. </p>
                    <div class="post-summary-footer">
                        <ul class="post-data-3">
                            <li><button class="btn btn-mini btn-inverse" type="button">Read more</button></li>
                            <li><i class="icon-calendar"></i> 09/04/15</li>
                            <li><i class="icon-user"></i> <a href="#">Pr. Daniel TOGMA et All, Université de Douala</a></li>
                            <li><i class="icon-eye-open"></i> <a href="#">5 374 </a></li>
                        </ul>
                    </div>
            </article>

            <!-- Blog Post 1 -->
            <article class="clearfix">
                <a href="blog-single.htm"><img src="<?php echo base_url(); ?>resources/img/gallery/gallery-img-1-4col.jpg" alt="Post Thumb" class="align-left"></a>
                <h4 class="title-bg"><a href="blog-single.htm">Is art everything to anybody?</a></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis mattis lorem, quis gravida nunc iaculis ac. Proin tristique tellus in est vulputate luctus fermentum ipsum molestie. </p>
                    <div class="post-summary-footer">
                        <ul class="post-data-3">
                            <li><button class="btn btn-mini btn-inverse" type="button">Read more</button></li>
                            <li><i class="icon-calendar"></i> 09/04/15</li>
                            <li><i class="icon-user"></i> <a href="#">Pr. Daniel TOGMA et All, Université de Douala</a></li>
                            <li><i class="icon-eye-open"></i> <a href="#">5 374 </a></li>
                        </ul>
                    </div>
            </article>
            <!-- Pagination -->
            <button class="btn btn-mini ab-float-right" type="button">Voir plus d'innovation</button>
        </div>
		<br/>
		
        <!-- Client Area
        ================================================== -->
        <div class="span6 ">

            <h5 class="title-bg">Les Innovateurs
                
                <button id="btn-client-next" class="btn btn-inverse btn-mini" type="button">&raquo;</button>
                <button id="btn-client-prev" class="btn btn-inverse btn-mini" type="button">&laquo;</button>
            </h5>

            <!-- Client Testimonial Slider-->
            <div id="clientCarousel" class="carousel slide no-margin">
            <!-- Carousel items -->
            <div class="carousel-inner">

                <div class="active item">
                    <p class="quote-text">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. In interdum felis fermentum ipsum molestie sed porttitor ligula rutrum. Morbi blandit ultricies ultrices. Vivamus nec lectus sed orci molestie molestie."<cite>- Client Name, Big Company</cite></p>
                </div>

                <div class="item">
                    <p class="quote-text">"Adipiscing elit. In interdum felis fermentum ipsum molestie sed porttitor ligula rutrum. Morbi blandit ultricies ultrices. Vivamus nec lectus sed orci molestie molestie."<cite>- Another Client Name, Company Name</cite></p>
                </div>

                <div class="item">
                    <p class="quote-text">"Mauris eget tellus sem. Cras sollicitudin sem eu elit aliquam quis condimentum nulla suscipit. Nam sed magna ante. Ut eget suscipit mauris."<cite>- On More Client, The Company</cite></p>
                </div>
                
            </div>
            </div>

            <!-- Client Logo Thumbs-->
            <ul class="client-logos">
                <li><a href="#" class="client-link"><img src="<?php echo base_url(); ?>resources/img/gallery/client-img-1.png" alt="Client"></a></li>
                <li><a href="#" class="client-link"><img src="<?php echo base_url(); ?>resources/img/gallery/client-img-2.png" alt="Client"></a></li>
                <li><a href="#" class="client-link"><img src="<?php echo base_url(); ?>resources/img/gallery/client-img-3.png" alt="Client"></a></li>
                <li><a href="#" class="client-link"><img src="<?php echo base_url(); ?>resources/img/gallery/client-img-4.png" alt="Client"></a></li>
                <li><a href="#" class="client-link"><img src="<?php echo base_url(); ?>resources/img/gallery/client-img-5.png" alt="Client"></a></li>
            </ul>

        </div>
		
        