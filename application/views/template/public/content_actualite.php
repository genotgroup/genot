<!--=====  slider =======-->
<div class="news-holder cf">
    <ul class="news-headlines">
        <?php if(isset($publications)) foreach ($publications as $publication) { ?>
			<li ><?php echo substr($publication->titre, 0, 75); if(strlen($publication->titre)>75) echo ' ...'; ?></li>
        <?php }   ?>
		<?php if(isset($innovations)) foreach ($innovations as $innovation) { ?>
			<li ><?php echo substr($innovation->titre, 0, 75); if(strlen($innovation->titre)>75) echo ' ...'; ?></li>
        <?php }   ?>
    
        <!-- li.highlight gets inserted here -->
    </ul>
</div><!-- .news-holder -->
