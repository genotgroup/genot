<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>SNRICA - Système National de Recherche et d'Innovation du Cameroun</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSS
================================================== 
<link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>-->
<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/bootstrap-responsive.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/prettyPhoto.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/flexslider.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/custom-styles.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>resources/css/style-abe.css">

<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <link rel="stylesheet" href="css/style-ie.css"/>
<![endif]--> 

<!-- Favicons
================================================== -->
<link rel="shortcut icon" href="<?php echo base_url(); ?>resources/img/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo base_url(); ?>resources/img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>resources/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>resources/img/apple-touch-icon-114x114.png">

<!-- JS
================================================== -->
<script src="<?php echo base_url(); ?>resources/js/jquery-1.8.3.min.js"></script>
<script src="<?php echo base_url(); ?>resources/js/bootstrap.js"></script>
<script src="<?php echo base_url(); ?>resources/js/jquery.prettyPhoto.js"></script>
<script src="<?php echo base_url(); ?>resources/js/jquery.flexslider.js"></script>
<script src="<?php echo base_url(); ?>resources/js/jquery.custom.js"></script>
<script type="text/javascript">
$(document).ready(function () {

    $("#btn-blog-next").click(function () {
      $('#blogCarousel').carousel('next')
    });
     $("#btn-blog-prev").click(function () {
      $('#blogCarousel').carousel('prev')
    });

     $("#btn-client-next").click(function () {
      $('#clientCarousel').carousel('next')
    });
     $("#btn-client-prev").click(function () {
      $('#clientCarousel').carousel('prev')
    });
    
});

 $(window).load(function(){

    $('.flexslider').flexslider({
        animation: "slide",
        slideshow: true,
        start: function(slider){
          $('body').removeClass('loading');
        }
    });  
});

</script>

</head>

<body class="home grad-home">
    <!-- Color Bars (above header)-->
	<div class="color-bar-1"></div>
    <div class="color-bar-2 color-bg"></div>
    
    <div class="container ab-main-contener">
    
      
      <div class="row "><!-- debut lien utils -->
      
			<!-- Lien util
			================================================== -->
			<div class="span12">
				&nbsp; <a href="#"> FAQ</a> | 
				<a href="#"> Téléchargements</a> | 
				<a href="#"> Annuaire</a> | 
				<a href="#"> Forum</a> | 
				<a href="#"> Contact</a> | 
				<a href="#"> Aide</a> | 
				<span class="connexion_bdica" style="float: right; font-size">
                            <?php if ($this->flexi_auth->is_logged_in()) { ?>
                                <?php
                                $data = $this->flexi_auth->user_data_view();
                                if (isset($data))
                                    echo anchor('authentified/auth_public/view_data_profile', $data['nom'] . ' As ' . $data['uacc_username']);
                                ?>  |  
    <?php echo anchor('auth/logout', 'Deconnexion'); ?> 
                            <?php } else { echo anchor('auth', (isset($user_account))? $user_account:'Connexion '); }
                                
                            ?>
                               
                        </span>
			</div>
	  </div>
	    
      <div class="row header"><!-- debut Header -->
      
			<!-- Logo
			================================================== -->
			<div class="span12 logo">
				<a href="www.minresi.cm"><img src="<?php echo base_url(); ?>resources/img/banner.jpg" alt="" /></a>
				
			</div>
	  </div>
	  
	   <div class="row "><!-- Debut barre de menu -->
        <!-- Main Navigation
        ================================================== -->
        <div class="span12 navigation">
            <?php require("include/mega_menu.php"); ?>

            <!-- Mobile Nav
            ================================================== -->
            <form action="#" id="mobile-nav" class="visible-phone">
                <div class="mobile-nav-select">
                <select onchange="window.open(this.options[this.selectedIndex].value,'_top')">
                    <option value="">Navigate...</option>
                    <option value="index.htm">Home</option>
                        <option value="index.htm">- Full Page</option>
                        <option value="index-gallery.htm">- Gallery Only</option>
                        <option value="index-slider.htm">- Slider Only</option>
                    <option value="features.htm">Features</option>
                    <option value="page-full-width.htm">Pages</option>
                        <option value="page-full-width.htm">- Full Width</option>
                        <option value="page-right-sidebar.htm">- Right Sidebar</option>
                        <option value="page-left-sidebar.htm">- Left Sidebar</option>
                        <option value="page-double-sidebar.htm">- Double Sidebar</option>
                    <option value="gallery-4col.htm">Gallery</option>
                        <option value="gallery-3col.htm">- 3 Column</option>
                        <option value="gallery-4col.htm">- 4 Column</option>
                        <option value="gallery-6col.htm">- 6 Column</option>
                        <option value="gallery-4col-circle.htm">- Gallery 4 Col Round</option>
                        <option value="gallery-single.htm">- Gallery Single</option>
                    <option value="blog-style1.htm">Blog</option>
                        <option value="blog-style1.htm">- Blog Style 1</option>
                        <option value="blog-style2.htm">- Blog Style 2</option>
                        <option value="blog-style3.htm">- Blog Style 3</option>
                        <option value="blog-style4.htm">- Blog Style 4</option>
                        <option value="blog-single.htm">- Blog Single</option>
                    <option value="page-contact.htm">Contact</option>
                </select>
                </div>
                </form>
            
        </div>
        
      </div><!-- End Header -->
     
    <div class="row headline"><!-- Begin Headline -->
        <div class="span8">
            <?php echo $content; ?>
        </div>
        
        <!-- Headline Text
        ================================================== -->
         <div class="span4 sidebar">

            <!--Search -->
            <section>
                <div class="input-append ab-recherche-bar">
                    <form action="#">
                        <input id="appendedInputButton" size="16" type="text" placeholder="Search"><button class="btn" type="button"><i class="icon-search"></i></button>
                    </form>
                </div>
            </section>

            <!--Categories-->
            <h5 class="title-bg">Recherche & Innovations</h5> 
            <ul class="post-category-list">
                <li>
                    <?php echo anchor('publication_public/list_publication/', '<i class="icon-plus-sign"></i>Publications', array()); ?>
                </li>
                <li>
                    <?php echo anchor('recherche/chercheur', '<i class="icon-plus-sign"></i>Chercheurs', array()); ?>
                </li>
                <li>
                    <?php echo anchor('innovation_public/annuaire_innovation', '<i class="icon-plus-sign"></i>Innovations', array()); ?>
                </li>
                <li>
                    <?php echo anchor('innovation_public/annuaire_innovateurs', '<i class="icon-plus-sign"></i>Innovateurs', array()); ?>
                </li>
            </ul>
            <br/>
            <!--Publication les plus visités-->
            <h5 class="title-bg">Les publications les plus consultées</h5>
            <ul class="popular-posts ">
                <?php echo $publication_plus_vue; ?>
            </ul>
			
			<!--Innovation plus visité-->
            <h5 class="title-bg">Quelques Innovations</h5>
            <ul class="popular-posts">
                <li>
                    <a href="#"><img src="<?php echo base_url(); ?>resources/img/gallery/cafetiereindustriellejersic2015.jpg" alt="Popular Post" width="70" height="70" ></a>
                    <h6><a href="#">Cafetière industrielle et présentée au JERSIC 2015 par le CRRI de l'Ouest</a></h6>
                    <em>12 novembre 2015</em>
                </li>
                <li>
                    <a href="#"><img src="<?php echo base_url(); ?>resources/img/gallery/Collemaniocjersic2015.jpg" alt="Popular Post" width="70" height="70" ></a>
                    <h6><a href="#">Produit UTRAMAC: Colle à eau à base de l'amidon de manioc et présenté au JERSIC 2015 par le CRRI du Centre</a></h6>
                    <em>12 novembre 2015</em>
                <li>
                    <a href="#"><img src="<?php echo base_url(); ?>resources/img/gallery/Sechoirsolairejersic2015.jpg" alt="Popular Post" width="70" height="70" ></a>
                    <h6><a href="#">Séchoir solaire construit par l'ONG AFISAF et présenté au JERSIC 2015 par le CRRI du Centre</a></h6>
                    <em>12 novembre 2015</em>
                </li>
            </ul>
			
            <!--Tabbed Content-->
            <h5 class="title-bg">Plus d'informations</h5>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#about" data-toggle="tab">Bulletins Info</a></li>
                <li> <a href="#comments" data-toggle="tab">Actualités</a></li>
                <li><a href="#tweets" data-toggle="tab">Evènements</a></li>
            </ul>

            <div class="tab-content active">
				<div class="tab-pane " id="about">
                     <ul>
                        <li><i class="icon-comment"></i>Echo de la recherche: N°008 / Août 2014 <a href="#">Le jeudi 14 août 2014, s’est tenue  dans  la  salle  des conférences du MINRESI, en présence de Madame le Secré-taire Général.</a></li>
                        <li><i class="icon-comment"></i>Echo de la recherche: N°009 / Septembre 2014 <a href="#">Dans le cadre de la 2ème campagne agricole 2014 dans la région du Centre, le Ministre</a></li>
                        <li><i class="icon-comment"></i>Echo de la recherche: N°010 / Octobre 2014 <a href="#">Le mercredi 24 septembre 2014 à 10 heures, s’est tenue dans la Salle des conférences</a></li>
                        <li><i class="icon-comment"></i>Echo de la recherche: N°011 / Novembre 2014<a href="#">L’IRAD dont la vocation repose sur le développement agricole, mène des activités à </a></li>
                        <li><i class="icon-comment"></i>Echo de la recherche: N°012 / Decembre 2014 <a href="#">Le mercredi 29 octobre 2014 à 10 heures, s’est tenue dans la Salle des conférences du MINRESI</a></li>
                    </ul>
                </div>
                <div class="tab-pane " id="comments">
                     <ul>
                        <li><i class="icon-comment"></i>admin on <a href="#">Ethnobotanical uses of medicinal plants of two ethnoecological regions</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Consectetur adipiscing elit</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Consectetur adipiscing elit</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Consectetur adipiscing elit</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Consectetur adipiscing elit</a></li>
                    </ul>
                </div>
                <div class="tab-pane " id="tweets">
                     <ul>
                        <li><i class="icon-comment"></i>admin on <a href="#">The epidemiology of malaria among pregnant women in Garoua, Northern Camero</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Machine à écraser les feuilles de manioc,ndolè gombo macabo… (LEGUMINE</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Machine à écraser les feuilles de manioc,ndolè gombo macabo… (LEGUMINE</a></li>
                        <li><i class="icon-comment"></i>admin on <a href="#">Machine à écraser les feuilles de manioc,ndolè gombo macabo… (LEGUMINE</a></li>
                    </ul>
                </div>
            </div>

            <!--Video Widget
            <h5 class="title-bg">Video Widget</h5>
            <iframe src="http://player.vimeo.com/video/24496773" width="370" height="208"></iframe> -->
			<img src="img/gallery/vimeo.JPG" alt="" width="370"  />
        </div>
    
    </div><!-- End Headline -->
    
    <div class="row"><!-- Begin Bottom Section -->
    
        
        
    </div><!-- End Bottom Section -->
    
    </div> <!-- End Container -->

    <!-- Footer Area
        ================================================== -->

	<div class="footer-container"><!-- Begin Footer -->
    	<div class="container">
        	<div class="row footer-row">
                <div class="span3 footer-col">
                    <h5>A propos de nous</h5>
                    <img src="<?php echo base_url(); ?>resources/img/logo.png" alt="MINRESI" width="80" /><br /><br />
                  
                    <ul class="social-icons">
                        <li><a href="#" class="social-icon facebook"></a></li>
                        <li><a href="#" class="social-icon twitter"></a></li>
                        <li><a href="#" class="social-icon dribble"></a></li>
                        <li><a href="#" class="social-icon rss"></a></li>
                        <li><a href="#" class="social-icon forrst"></a></li>
                    </ul>
                </div>
                <div class="span3 footer-col">
                    <h5>Actualité & évènement</h5>
                    <ul>
                        <li><a href="#">11 Nov 2015</a> Journées d Excellence de la Recherche Scientifique et de l Innovation au Cameroun, Yaoundé </li>
                    </ul>
                </div>
                <div class="span3 footer-col">
                    <h5>Accès rapides</h5>
                     <ul class="post-list">
                        <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                        <li><a href="#">Consectetur adipiscing elit est lacus gravida</a></li>
                    </ul>
                </div>
                <div class="span3 footer-col">
                    <h5>Nos partenaires</h5>
                    <ul class="img-feed">
                        <li><a href="http://www.ird.fr"><img src="<?php echo base_url(); ?>resources/img/ird.png" alt="Image Feed"></a></li>
                        <li><a href="#"><img src="<?php echo base_url(); ?>resources/img/kets.png" width="50" alt="Image Feed"></a></li>
                        
                    </ul>
                </div>
            </div>

            <div class="row"><!-- Begin Sub Footer -->
                <div class="span12 footer-col footer-sub">
                    <div class="row no-margin">
                        <div class="span6"><span class="left">Copyright 2016 MINRESI . All rights reserved.</span></div>
                        <div class="span6">
                            <span class="right">
                            <a href="#">Home</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#">Features</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#">Gallery</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#">Blog</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="#">Contact</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div><!-- End Sub Footer -->

        </div>
    </div><!-- End Footer -->

    <!-- Scroll to Top -->  
    <div id="toTop" class="hidden-phone hidden-tablet">Back to Top</div>
    
</body>
</html>