<div class="navbar hidden-phone">

    <ul class="nav " >
        <li class="dropdown active"><?php echo anchor('welcome', 'Accueil'); ?></li>


        <li class="dropdown">
            <?php echo anchor('presentation/presentation_projet', 'Présentation<b class="caret"></b>', array("class" => "dropdown-toggle", "data-toggle" => "dropdown"));
            ?>
            <ul class="dropdown-menu ">
                <li><?php echo anchor('presentation/mot_du_minresi', 'Le mot du MINRESI', array()); ?></li>
                <li >
                    <?php echo anchor('presentation/architecture', 'Architecture globale de la recherche au Cameroun', array()); ?>
                </li>
                <li >
                    <?php echo anchor('presentation/orientations', 'Nouvelles orientations de la recherche', array()); ?>
                </li>
               <li >
                    <?php echo anchor('presentation/crri', 'Centre Régionaux Recherche et Innovation', array()); ?>
                </li>
            </ul>
            <!--<a href="features.htm">Présentation</a>-->
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="gallery-4col.htm">Activités de recherche <b class="caret"></b></a>
            <ul class="dropdown-menu ">
                <li>

                    <?php echo anchor('recherche/lis_axe_recherche', 'Axes de recherche', array()); ?>
                </li>
                <li>
                    <?php echo anchor('recherche/lis_domaine_recherche', 'Domaines de recherche', array()); ?>
                </li>
                <li>
                    <?php echo anchor('recherche/chercheur', 'Chercheurs', array()); ?>
                    <!--<ul class="dropdown">
                        <li><?php echo anchor('recherche/chercheur_institutionnel', 'Chercheurs institutionnels', array()); ?></li>  
                        <li class=" dropdown-submenu"><?php echo anchor('recherche/chercheur_prive', 'Chercheurs privés', array()); ?>
                            <ul class="dropdown">
                                <li><?php echo anchor('recherche/chercheur_prive_en_activite', 'En activité dans les structures privées', array()); ?></li>
                                <li><?php echo anchor('recherche/chercheur_prive_isole', 'Isolés', array()); ?></li>
                                <li><?php echo anchor('recherche/chercheur_prive_independant', 'Indépendants', array()); ?></li>  
                                <li><?php echo anchor('recherche/chercheur_prive_diaspora', 'Diaspora', array()); ?></li>  
                            </ul>
                        </li>
                        <li><?php echo anchor('recherche/chercheur_enseignant', 'Enseignants chercheurs', array()); ?></li>
                    </ul>-->
                </li>
                <li>
                    <?php echo anchor('recherche/institut_de_recherche', 'Instituts de recherche', array());
                    ?>
                    <!--<ul class="dropdown">
                        <li><?php echo anchor('recherche/institut_de_recherche_sous_tutelle', 'Sous tutelle du MINRESI', array()); ?></li>
                        <li><?php echo anchor('recherche/institut_de_recherche_universite', 'Universités', array()); ?></li>
                        <li><?php echo anchor('recherche/institut_de_recherche_prive', 'Privés', array()); ?></li>	 
                    </ul>-->
                </li>

            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="blog-style1.htm">Publications <b class="caret"></b></a>
            <!--            <ul class="dropdown-menu">
                            <li><a href="blog-style1.htm">Blog Style 1</a></li>
                            <li><a href="blog-style2.htm">Blog Style 2</a></li>
                            <li><a href="blog-style3.htm">Blog Style 3</a></li>
                            <li><a href="blog-style4.htm">Blog Style 4</a></li>
                            <li><a href="blog-single.htm">Blog Single</a></li>
                        </ul>-->
            <ul class="dropdown-menu">
                <li>
                    <?php echo anchor('publication_public/list_publication/', 'Annuaire des publications', array()); ?>

                </li>
                <li>
                    <?php echo anchor('publication_public/domaine_publication/', 'Par domaines', array()); ?> 
                </li>
                <li>
                    <?php echo anchor('publication_public/chercheur_publication/', 'Par chercheur', array()); ?>
                </li>

                <li><?php echo anchor('publication_public/nature_publication', 'Par types', array()); ?>

                </li>

            </ul>
        </li>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="blog-style1.htm">Innovations <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <!--<li><?php echo anchor('innovation_public/lis_axe_innovations', 'Axes d\'innovations', array()); ?></li>-->
                <!--<li><?php echo anchor('innovation_public/lis_domaine_innovation', 'Domaines d\'innovations', array()); ?></li>-->
                <li><?php echo anchor('innovation_public/annuaire_innovation', 'Annuaire des innovations', array()); ?></li>
                <li><?php echo anchor('innovation_public/annuaire_innovateurs', 'Annuaire des innovateurs', array()); ?></li>
                <li><?php echo anchor('innovation_public/lis_brevet', 'Brevets d\'inventions', array()); ?></li>
                <!--<li><?php echo anchor('innovation_public/lis_centre_innovation', 'Centres d\'innovations', array()); ?></li>-->
            </ul>
        </li> 
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Actualités <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><?php echo anchor('actualite_public/actualite', 'Actualités', array()); ?></li>
                <li><?php echo anchor('actualite_public/evenements', 'Evènements', array()); ?></li>
                <li><?php echo anchor('actualite_public/bulletin_info', 'Bulletins d\'information', array()); ?>
                </li>
            </ul>
        </li>

<?php 
if ($this->flexi_auth->is_logged_in_via_password()) { 
    $group = $this->flexi_auth->get_user_group();
    if ($this->flexi_auth->is_admin() || $group == "Point Focal Innov" || $group == "Point Focal Pub") {
    ?>
        <li class="dropdown">
            <?php echo anchor('authentified/welcome', 'Administration', array('class'=>'dropdown-toggle')); ?>
        </li>
<?php }
}    ?>
       
    </ul>

</div>
