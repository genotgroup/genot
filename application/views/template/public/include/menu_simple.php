<div class="row-fluid" >
    <div class="col-md-12" >
        <ul class="nav-menu clearfix animated">
            <li><?php echo anchor('welcome', 'Acceuil', array('' => '')); ?></li>
            <li>
                <?php echo anchor('welcome', 'Présentation', array('' => '')); ?>
                <div class="container-1">
                    <div class="col1 bordure_sous_menu" style="">

                        <ul>
                            <li >
                                <?php echo anchor('presentation/mot_minresi', 'Le mot du MINRESI', array('' => '')); ?>
                            </li>
                            <li >
                                <?php echo anchor('presentation/presentation_projet', 'Le Mot de Présentation du Projet', array('' => '')); ?>
                            </li>
                             
                        </ul>
                    </div>

                </div>	
            </li>
            <li><a href="#">Activité de Recherche</a>
                <div class="container-1">
                    <div class="col1 bordure_sous_menu">
                        <!--<h4 >Thèmes de recherche</h4>-->
                        <ul>
                            <li><a  href="#">Axes de recherche</a></li>
                            <li><a  href="#">Thèmes de recherche</a></li>
                            <li><a  href="#">Domaines de recherches</a></li>
                            <li><a  href="#">Annuaire des Chercheur</a></li>
                            <li><a  href="#">Annuaire des enseignants Chercheur</a></li>
                            <li><a  href="#">Instituts de recherche</a></li>
                        </ul>
                    </div>
                    <div class="col1 bordure_sous_menu">
                        <h4>Productions Scientifiques</h4>
                        <ul>
                            <li><a href="#">Articles </a></li>
                            <li><a href="#">Rapports</a></li>
                        </ul>
                    </div>

                </div>	
            </li>
            <li><a href="#">Publication</a>
                <div class="container-1">
                    <div class="col1 bordure_sous_menu">
                        <!--<h4 >Thèmes de recherche</h4>-->
                        <ul>
                            <li><a  href="#">Domaines de Publications</a></li>
                            <li><a  href="#">Auteurs des publications</a></li>
                            <li><a  href="#">Annuaire des publications</a></li>
                            <li><a  href="#">Rapports</a></li>
                            <li><a  href="#">Mémoires</a></li>
                            <li><a  href="#">Articles</a></li>
                            <li><a  href="#">Livres</a></li>
                        </ul>
                    </div>

                </div>	

            </li>
            <li><a href="#">Innovations</a>
                <div class="container-1">
                    <div class="col1 bordure_sous_menu">
                        <!--<h4 >Thèmes de recherche</h4>-->
                        <ul>
                            <li><a  href="#">Innovations Techniques</a></li>
                            <li><a  href="#">Innovations Scientifiques</a></li>
                        </ul>
                    </div>

                </div>	
            </li>
            <li>
                <a href="#">Actualité</a>
                <div class="container-1">
                    <div class="col1 bordure_sous_menu">
                        <!--<h4 >Thèmes de recherche</h4>-->
                        <ul>
                            <li><a  href="#">A la une</a></li>
                            <li><a  href="#">Evènements</a></li>
                        </ul>
                    </div>

                </div>	
            </li>
            <?php echo $sidebar; ?>
        </ul>
    </div> 
</div> 