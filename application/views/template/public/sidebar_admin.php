<div class="menu-gauche">
    <div class="box-menu">Données de Base</div>
    <ul class="nav nav-pills nav-stacked">
        <li><?php echo anchor('authentified/mainCrud/arrondissement', 'Localités', array('class' => 'back')); ?></li>
            <li><?php echo anchor('authentified/mainCrud/institut_de_recherche', 'Institutions', array('class' => 'back')); ?></li>
            <li><?php echo anchor('authentified/publications/crud_publication', 'Publications', array('class' => 'back')); ?></li>
            <li><?php echo anchor('authentified/innovations/crud_innovation', 'Innovations', array('class' => 'back')); ?></li>
                <!--<li><a href="<?php ?>">Localités</a></li>-->
            <!--<li><a href="#">Innovations</a></li>-->
            <li><a href="#">Recherches</a></li>
    </ul>

</div>
<div class="menu-gauche">
    <div class="box-menu">Utilisateurs</div>
    <ul class="nav nav-pills nav-stacked">
        <li><?php echo anchor('authentified/auth_admin/manage_user_accounts', 'Comptes Utilisateurs', array('class' => 'back')); ?></li>
            <li><?php echo anchor('authentified/auth_admin/manage_user_groups', 'Groupes Utilisateurs', array('class' => 'back')); ?></li>
            <li><?php echo anchor('authentified/auth_admin/manage_privileges', 'Privillèges', array('class' => 'back')); ?></li>
            <li><?php echo anchor('authentified/auth_admin/list_user_status/active', 'Utilisateurs Actifs', array('class' => 'back')); ?></li>
            <li><?php echo anchor('authentified/auth_admin/list_user_status/inactive', 'Utilisateurs Inactifs', array('class' => 'back')); ?></li>
    </ul>

</div>