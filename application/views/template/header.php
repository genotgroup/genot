<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Procratinate - Suivre et gérer vos dossiers de façon professionnel</title>

        <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png" />
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?php echo base_url(); ?>assets/css/metisMenu.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/sb-admin-2.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/css/style-perso.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/datepicker/jquery-ui.css">

        <script src="<?php echo base_url(); ?>assets/datepicker/jquery-1.10.2.js"></script>
        <script src="<?php echo base_url(); ?>assets/datepicker/jquery-ui.js"></script> 
        <script>
            $(function () {
                $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});
                $("#datepicker2").datepicker();
            });
        </script> 

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> <img src="<?php echo base_url(); ?>assets/img/logo5.png"  height="40"></a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope fa-fw"></i>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">
                            
                            <li>
                                <a class="text-center" href="#">
                                    <strong>Lire tous les Messages</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks">
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Dossier 1</strong>
                                            <span class="pull-right text-muted">40% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Dossier 2</strong>
                                            <span class="pull-right text-muted">20% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Dossier 3</strong>
                                            <span class="pull-right text-muted">60% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                                <span class="sr-only">60% Complete (warning)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Dossier 4</strong>
                                            <span class="pull-right text-muted">80% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>Voirs tous les dossiers</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Comment
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="pull-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-tasks fa-fw"></i> New Task
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-alerts -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw">
                            
                            </i><?php $user = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                                    echo $user[0]->upro_last_name . " " . $user[0]->upro_first_name
                                    ?>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><?php echo anchor('auth_public/update_account', '<i class="fa fa-gear fa-fw"></i> Update Profil'); ?> </li> 
                            <!--<li class="divider"></li>-->
                            <li><?php echo anchor('auth_public/update_email', '<i class="fa fa-gear fa-fw"></i> Update Email Address'); ?> </li> 
                            <li><?php echo anchor('auth_public/change_password', '<i class="fa fa-gear fa-fw"></i> Update Password'); ?>  </li> 
                            <!--<li><a href="<?php echo $base_url; ?>auth_public/manage_address_book"><i class="fa fa-gear fa-fw"></i>Manage Address Book</a></li>--> 

                            <li class="divider"></li>
                            <li><?php echo anchor('auth/logout', 'Logout'); ?></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <form method="post" action="<?php echo $base_url; ?>dossiers/research">
                                        <input type="text" name="search1" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </form>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="<?php echo $base_url; ?>"><i class="fa fa-dashboard fa-fw"></i> Tableau de bord</a>
                            </li>
                            <li ><?php echo anchor('dossiers', '<i class="fa  fa-folder-open fa-fw "></i> Créer un nouveau dossier'); ?>  
                            </li>
                            <li ><?php echo anchor('dossiers/filter/f', '<i class="fa fa-bar-chart-o fa-fw "></i> Voir tous les dossiers <b style="color: #D9534F;">('.')</b>'); ?>   
                            </li>
                            <li ><?php echo anchor('dossiers/filter/f', '<i class="fa fa-bar-chart-o fa-fw "></i> Voirs les dossiers financiers'); ?>   
                            </li>
                            <li ><?php echo anchor('dossiers/filter/a', '<i class="fa fa-bar-chart-o fa-fw "></i> Voirs les dossiers administratifs'); ?>   
                            </li>
                            <li ><?php echo anchor('dossiers/filter/t', '<i class="fa fa-bar-chart-o fa-fw "></i> Voirs les dossiers techniques'); ?>  
                            </li>
                        <?php if ($this->flexi_auth->is_logged_in_via_password() && $this->flexi_auth->is_admin()) { ?>    
                            <li ><?php echo anchor('rapport_synthese/liste_utilisateur/', '<i class="fa fa-bar-chart-o fa-fw "></i> Rapport Synthese'); ?>  
                            </li>
                        <?php } ?>    
                            <li>
                                <a href="#"><i class="fa fa-sitemap fa-fw"></i> Rapports<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li><?php echo anchor('etatDossier/etat_list_dossier', 'Liste des Dossiers'); ?> 
                                    </li>
<?php 
if ($this->flexi_auth->is_privileged('Print Global')) { ?>                                    
                                        <li><?php echo anchor('etatDossier/etat_dossier_admin', 'Liste Globale des Dossiers'); ?>
                                        </li>
<?php } ?>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
<?php if ($this->flexi_auth->is_logged_in_via_password() && $this->flexi_auth->is_admin()) { ?>
                                    <a href="#"><i class="fa fa-sitemap fa-fw"></i> Gestion des Utilisateurs<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li><?php echo anchor('auth_admin/manage_user_accounts', 'Gerer les compte utilisateur'); ?></li>
                                        
                                        <!--<li><?php echo anchor('auth_admin/manage_user_groups', 'Gerer les groupes'); ?></li>-->
                                        <li><?php echo anchor('auth_admin/manage_privileges', 'Gerer les Privileges'); ?></li>
                                        <li><?php echo anchor('auth_admin/creation_account', 'Creer un nouveau compte'); ?></li>
    <!--                                        <li><a href="<?php echo $base_url; ?>auth_admin/list_user_status/active">List Active Users</a> </li>
                                        <li><a href="<?php echo $base_url; ?>auth_admin/list_user_status/inactive">List Inactive Users</a> </li>
                                        <li><a href="<?php echo $base_url; ?>auth_admin/delete_unactivated_users">List Unactivated Users</a> </li>
                                        <li><a href="<?php echo $base_url; ?>auth_admin/failed_login_users">List Failed Logins</a></li>	 -->

                                    </ul>
                                    <!-- /.nav-second-level -->
<?php } ?>
                            </li>

                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>


                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- /.row -->


                    <!-- Content ici -->
