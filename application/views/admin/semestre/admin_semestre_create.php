<h4><?php if (isset($titre)) echo $titre; ?>  </h4>
<form role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/semestre/createSemestre">
        <fieldset disabled>

            <span style="color: red"><?php echo form_error('message'); ?> </span> 

            <h4  style="color: red"><?php if (isset($message)) echo $message; ?>  </h4>
        </fieldset>
        <div class="form-group ">
            <label class="control-label" for="nom">Nom : </label>
            <input type="text"  name='nom' value="<?php echo set_value('nom', $this->form_data->nom); ?>" class="form-control  taille_champ" id="nom">
        </div>
        <div class="form-group">
            <label  for="code">code *: <?php echo form_error('code'); ?></label>
            <input  value="<?php echo set_value('code', $this->form_data->code); ?>" name="code" class="form-control  taille_champ" id="code"  type="text">
        </div>                          
        <div class="form-group">
            <label>Classe * : <?php echo form_error('id_classe'); ?></label>
            <select name="id_classe" class="form-control"  value="<?php echo set_value('id_classe', $this->form_data->id_classe); ?>" >
                <option value="">--------- Choisir la classe -------</option>
                <?php  foreach ($classe_aca as $classe) { ?>
                    <option value="<?php echo $classe->id; ?>" <?php if($this->form_data->id_classe==$classe->id) echo 'selected'; ?>><?php echo $classe->code; ?></option>
                <?php } ?>
            </select> 
        </div>
        <div  style="text-align: right;">
            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <button type="submit" class="btn btn-info">Enregistrer</button> 
                    <button class="btn btn-warning">Annuler</button> 
                </div>
            </div>
        </div>
</form>