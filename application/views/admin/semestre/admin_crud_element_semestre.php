 
        <?php if (isset($css_files)) {
            foreach ($css_files as $file):
                ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
            <?php endforeach;
        }
        ?>
        <?php if (isset($js_files)) {
            foreach ($js_files as $file):
                ?>
                <script src="<?php echo $file; ?>"></script>
        <?php endforeach;
            }
        ?>
         
         <h2><?php if(isset($titre)) echo $titre;  ?></h2>
         <?php echo anchor('semestre_crud_element/modules', '<i class="menu-icon icon-tasks"></i>Modules', array()); ?> | 
         <?php echo anchor('semestre_crud_element/matiere', '<i class="menu-icon icon-tasks"></i>Matière', array()); ?> | 
         <?php echo anchor('semestre_crud_element/type_evaluation', '<i class="menu-icon icon-tasks"></i>Type D\'évaluation', array()); ?> | 
         <?php echo anchor('semestre_crud_element/evaluation', '<i class="menu-icon icon-tasks"></i>Evaluation', array()); ?>  

        <div>
            <?php if (isset($output)) echo $output; ?>
        </div>
    