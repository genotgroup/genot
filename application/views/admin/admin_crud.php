 
        <?php if (isset($css_files)) {
            foreach ($css_files as $file):
                ?>
                <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
            <?php endforeach;
        }
        ?>
        <?php if (isset($js_files)) {
            foreach ($js_files as $file):
                ?>
                <script src="<?php echo $file; ?>"></script>
        <?php endforeach;
            }
        ?>
         
         <h2><?php if(isset($titre)) echo $titre;  ?></h2>
         <?php echo anchor('mainCrud/anneAcademique', '<i class="menu-icon icon-tasks"></i>Années Academiques', array()); ?> | 
         <?php echo anchor('mainCrud/promotion', '<i class="menu-icon icon-tasks"></i>Promotions Academiques', array()); ?> | 
         <?php echo anchor('mainCrud/anneSpecialite', '<i class="menu-icon icon-tasks"></i>Spécialités de Formation', array()); ?> | 
         <?php echo anchor('mainCrud/niveau', '<i class="menu-icon icon-tasks"></i>Niveaux de Formation', array()); ?> |
         <?php echo anchor('mainCrud/classe', '<i class="menu-icon icon-tasks"></i>Classe', array()); ?>

        <div>
            <?php if (isset($output)) echo $output; ?>
        </div>
    