<!DOCTYPE html>
<html lang="en">

    <head>
<!--        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/stylepdf.css">-->

        <style>

            table {text-align: left;min-width: 500px;border-collapse:collapse; max-width: 100%}
            table th {text-align: left;border-collapse:collapse; background-color:#183152; color:#fff;}
            table td {text-align: left;border-collapse:collapse; }
            table tr {border:1px solid #606060; }
            table tr.bg td {background-color:#e8f6ff;min-height: 100px;}

            .table_content{
                border:1px;
            }


            .header{
                text-align: center;
                alignment-baseline: central;
            }

            .header_left{
                text-align: center;
                width: 300px;
                font-size: 11px;
            }

            .header_middle{
                background-image: url(assets/img/logo_pnlt.png);
                background-repeat:no-repeat;
                text-align: center;
                width: 140px;
                height: 65px;
            }

            .info{
                font-size:13px;
            }

            .info_fin{
                font-size:13px;
                margin-bottom: 20px;
            }

            .header_right{

                text-align: center;
                width: 300px;
                font-size: 11px;
            }

            .photo{

                border: 1px solid;
                border-radius: 10px 10px 10px 10px;
                height: 130px;
                line-height: 75px;
                text-align: center;
                width: 150px;
                font-style: italic;
                font-size:12px;
            }
            @page {
                margin-top: 1em;
                margin-left: 1em;
                margin-right: 1em;
                margin-bottom: 1em;
            }
            .content{
                text-align: center;
            }

            #content table{  border:1px solid #000;width: 100%; font-size:14px;}
            #content table th {border:1px solid #000; text-align: left;border-collapse:collapse; background-color:#EFECCA; color:#000;}
            #content table tr{border:1px solid #000;}
            #content table td{border: 1px solid #000;border-right : 1px solid #000;}

            body{
                font-size:12px;
                margin-top: 1em;
                margin-left: 1em;
                margin-right: 1em;
                margin-bottom: 1em;
            }
        </style>
        
    </head>

    <body>
    <center>
        <div class='header'>
            <table>	
                <tr>
                    <td>
                        <div class='header_left'>
                            REPUBLIQUE DU CAMEROUN <br />
                            Paix- Travail- Patrie<br />
                            --------------<br /> 
                            MINISTERE DE LA SANTE PUBLIQUE <br />
                            --------------<br /> 
                            SECRETARIAT GENERAL <br />
                            --------------<br /> 
                            DIRECTION DE LUTTE CONTRE LA TUBERCULOSE <br />
                            --------------<br /> 
                            Programme National de Lutte Contre la Tuberculose <br />
                            --------------<br /> 
                            Groupe Technique Central <br />
                            --------------<br /> 
                            Secrétariat Permanent <br />
                        </div>			
                    <td>				
                        <div class='header_middle'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <!--<img src="<?php echo base_url(); ?>assets/img/logo_pnlt.png" alt="Texte remplaçant l'image" title="" style="width: 120px">-->
                        </div>
                    </td>					

                    </td>
                    <td>
                        <div class='header_right'>
                            REPUBLIC OF CAMEROON<br />
                            Peace- Work- Fatherland<br />
                            --------------<br /> 
                            MINISTRY OF PUBLIC HEALTH <br />
                            --------------<br /> 
                            SECRETARIAT GENERAL <br />
                            --------------<br /> 
                            DEPARTMENT DISEASE CONTROL <br />
                            --------------<br /> 
                            National Tuberculis Control Program <br />
                            --------------<br /> 
                            Central Technical Group <br />
                            --------------<br /> 
                            Permanent Secretariat  <br />
                        </div>
                    </td>				
                </tr>				
            </table>
        </div>
    </center>
    <div id="content" >
        <h3 style ='text-align: center;font-size:14px'> <font style="color: #dd5600;">PROGRAMME NATIONAL DE LUTTE CONTRE LA TUBERTUCULOSE </font></h3>
        <p style="font-size:14px" ><strong> Liste des dossiers 
            </strong><br/> 
           
        <p>
        <?php echo $tableau; ?>
    </div>
    <p style="font-size:14px" style="">
         
        <span >Par : <strong >
                <?php
                $user = $this->flexi_auth->get_user_by_id($this->flexi_auth->get_user_id())->result();
                 $user = $user[0]->upro_first_name . " " .$user[0]->upro_last_name;
                echo $user;
                ?>
            </strong><span style="float: right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date : <strong><?php echo date('d-m-Y'); ?> </span></span>
    </body>
</html>

<?php if (isset($contents)) echo $contents; ?>


