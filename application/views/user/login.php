<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Procratinate - Suivre et gérer vos dossiers de façon professionnel </title>
    <link rel="icon" href="<?php echo base_url(); ?>assets/img/favicon.png" />
    <!-- Bootstrap Core CSS -->
  <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>assets/css/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>assets/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<center> <div style=""><a  href="#"> </a></div></center>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <img src="<?php echo base_url(); ?>assets/img/logo5.png" width="80" style="float:left">
                        <span><h4>Programme Supérieur de Spécialisation en Finances Publiques</h4></span>
                    </div>
                    <div class="panel-body">
	
			<?php if (! empty($message)) { ?>
				<div id="message">
					<?php echo $message; ?>
				</div>
			<?php } ?>
				
				<?php echo form_open(current_url(), 'class="parallel"');?>  	
					<fieldset class="w50 parallel_target">
						<legend>Veillez vous connecter SVP</legend>
						 
									<!--value="<?php echo set_value('login_identity', 'admin@admin.com');?>"--> 
							 
								<div class="form-group"> 
                                                                    <lable>Login ou Email:</lable>
									<input type="text" id="identity" name="login_identity" 
									value="<?php echo set_value('login_identity');?>" 
									class="form-control" placeholder="E-mail"/> 
								</div>
								
								
								<span class="tooltip width_400">
									<!--<h6>Example Users</h6>
									<p>There are 3 example users setup, login to each account using the following details.</p>
									<table>
										<thead>
											<tr>
												<th>Email</th>
												<th>Username</th>
												<th>Password</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>admin@admin.com</td>
												<td>admin</td>
												<td>password123</td>
											</tr>
											<tr>
												<td>moderator@moderator.com</td>
												<td>moderator</td>
												<td>password123</td>
											</tr>
											<tr>
												<td>public@public.com</td>
												<td>public</td>
												<td>password123</td>
											</tr>
										</tbody>
									</table>-->
								</span>
							 
									<!--value="<?php echo set_value('login_password', 'password123');?>"-->
								<div class="form-group"> 
                                                                    <lable>Mot de passe:</lable>
									<input type="password" id="password" name="login_password" 
									value="<?php echo set_value('login_password');?>"
									class="form-control" placeholder="Password"/>
								</div>		
                                                               <!-- <strong>Veillez confirmer que vous n'etes pas un robot</strong>-->
						<?php 
							# Below are 2 examples, the first shows how to implement 'reCaptcha' (By Google - http://www.google.com/recaptcha),
							# the second shows 'math_captcha' - a simple math question based captcha that is native to the flexi auth library. 
							# This example is setup to use reCaptcha by default, if using math_captcha, ensure the 'auth' controller and 'demo_auth_model' are updated.
						
							# reCAPTCHA Example
							# To activate reCAPTCHA, ensure the 'if' statement immediately below is uncommented and then comment out the math captcha 'if' statement further below.
			 				# You will also need to enable the recaptcha examples in 'controllers/auth.php', and 'models/demo_auth_model.php'.
							#/*
							if (isset($captcha)) 
							{ 
								echo "<li>\n";
								echo $captcha;
								echo "</li>\n";
							}
							#*/
							
							/* math_captcha Example
							# To activate math_captcha, ensure the 'if' statement immediately below is uncommented and then comment out the reCAPTCHA 'if' statement just above.•••••••••••
							# You will also need to enable the math_captcha examples in 'controllers/auth.php', and 'models/demo_auth_model.php'.
							if (isset($captcha))
							{
								echo "<li>\n";
								echo "<label for=\"captcha\">Captcha Question:</label>\n";
								echo $captcha.' = <input type="text" id="captcha" name="login_captcha" class="width_50"/>'."\n";
								echo "</li>\n";
							}
							#*/
						?>
							 
								<div class="checkbox">
								    <label>
								        <input name="remember_me" type="checkbox" value="1">Se souvenir de moi
								    </label>
								</div>
							 
								<div class="form-group">  
                                                                    <input type="submit" name="login_user" id="submit" value="Submit" class="btn btn-info"/>
                                                                </div>
								<hr/>
                                                                <a href="<?php echo $base_url;?>auth/forgotten_password">Mot de passe oublié</a> 
							 
                                                                <br/><a href="<?php echo $base_url;?>auth/resend_activation_token">Réactiver votre compte</a>
							 
						 
					</fieldset>
				<?php echo form_close();?>
				</div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>assets/js/sb-admin-2.js"></script>

</body>

</html>
