
<h2>Update Password</h2><hr/>
<?php echo anchor('auth_public/update_account', 'Update Account Details'); ?>  

<?php if (!empty($message)) { ?>
    <div id="message">
        <?php echo $message; ?>
    </div>
<?php } ?>

<?php echo form_open(current_url()); ?>  	
<div class="w100 frame">
    <ul>
        <li>
            <small> 
                Password length must be more than <?php echo $this->flexi_auth->min_password_length(); ?> characters in length.<br/>
                Only alpha-numeric, dashes, underscores, periods and comma characters are allowed.
            </small>
        </li>
    </ul>
    <div class="form-group"> 
        <label for="current_password">Current Password:</label>
        <input type="password" id="current_password" name="current_password" value="<?php echo set_value('current_password'); ?>"/>
    </div>
    <div class="form-group"> 
        <label for="new_password">New Password:</label>
        <input type="password" id="new_password" name="new_password" value="<?php echo set_value('new_password'); ?>"/>
    </div>
    <div class="form-group"> 
        <label for="confirm_new_password">Confirm New Password:</label>
        <input type="password" id="confirm_new_password" name="confirm_new_password" value="<?php echo set_value('confirm_new_password'); ?>"/>
    </div> 
    <div class="form-group">  
        <input type="submit" name="change_password" id="submit" value="Change Password" class="btn btn-info"/>
    </div>  

</div>
<?php echo form_close(); ?>
			