            <h2>Update Account Details</h2>
            <!--<a href="<?php echo $base_url; ?>auth_public/change_password">Change Password</a>-->

            <?php if (!empty($message)) { ?>
                <div id="message">
                    <?php echo $message; ?>
                </div>
            <?php } ?>

            <?php echo form_open(current_url()); ?>  	
            <fieldset>
                <legend>Personal Details</legend>
                <div class="form-group">
                    <label for="first_name">First Name:</label>
                    <input type="text" id="first_name" name="update_first_name" value="<?php echo set_value('update_first_name', $user['upro_first_name']); ?>"/>
                </div>
                <div class="form-group">
                    <label for="last_name">Last Name:</label>
                    <input type="text" id="last_name" name="update_last_name" value="<?php echo set_value('update_last_name', $user['upro_last_name']); ?>"/>
                </div>
            </fieldset>

            <fieldset>
                <legend>Contact Details</legend>
                <div class="form-group">
                    <label for="phone_number">Phone Number:</label>
                    <input type="text" id="phone_number" name="update_phone_number" value="<?php echo set_value('update_phone_number', $user['upro_phone']); ?>"/>
                </div>
                <div class="form-group">
                    <?php $newsletter = ($user['upro_newsletter'] == 1); ?>
                    <label for="newsletter">Subscribe to Newsletter:</label>
                    <input type="checkbox" id="newsletter" name="update_newsletter" value="1" <?php echo set_checkbox('update_newsletter', 1, $newsletter); ?>/>
                </div>
            </fieldset>

            <fieldset>
                <legend>Login Details</legend>
                <div class="form-group">
                    <label>Email Address:</label>
                    <input type="text" id="email" name="update_email" value="<?php echo set_value('update_email', $user[$this->flexi_auth->db_column('user_acc', 'email')]); ?>" class="tooltip_trigger"
                           title="Set an email address that can be used to login with."
                           />
                </div>
                <div class="form-group">
                    <p class="note">
                        Note: This method simply updates the users email address, if you want to verify the user has spelt their new email address correctly, you can send them a verification email to their new email address.<br/> 
                        <a href="<?php echo $base_url; ?>auth_public/update_email">Click here to see an example of updating a users email via email verification</a>.
                    </p>
                </div>
                <hr/>
                <div class="form-group">                
                    <label for="username">Username:</label>
                    <input type="text" id="username" name="update_username" value="<?php echo set_value('update_username', $user[$this->flexi_auth->db_column('user_acc', 'username')]); ?>" class="tooltip_trigger"
                           title="Set a username that can be used to login with."
                           />
                </div>
                <div class="form-group">
                    <label>Password:</label>
                    <?php echo anchor('auth_public/change_password', 'Click here to change your password'); ?>  
                </div>
            </fieldset>

            <fieldset>
                <legend>Update Account</legend>
                <div class="form-group"> 
                    <input type="submit" name="update_account" id="submit" value="Update Account" class="btn btn-info"/>
                </div>
            </fieldset>
            <?php echo form_close(); ?>
 