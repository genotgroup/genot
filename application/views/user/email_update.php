
<h2>Change Email</h2><hr>
<?php echo anchor('auth_public/update_account', 'Update Account Details'); ?> 
<br/><br/> 
<?php if (!empty($message)) { ?>
    <div id="message">
        <?php echo $message; ?>
    </div>
<?php } ?>

<?php echo form_open(current_url()); ?>  	
<div class="form-group">
            <input type="text" placeholder="New Email Address" id="email_address" name="email_address" value="<?php echo set_value('email_address'); ?>"/>
        </div> 
<div class="form-group">
            <input type="submit" name="update_email" id="submit" value="Submit" class="link_button large"/>
</div>
<?php echo form_close(); ?>
			