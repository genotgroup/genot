<?php
/* Translated by Tony LAURENT */
	$lang['list_add'] 				= 'Ajouter';
	$lang['list_actions'] 			= 'Actions';
	$lang['list_page'] 				= 'Page';
	$lang['list_paging_of'] 		= 'de';
	$lang['list_displaying']		= 'Affichage de {start} à {end} de {results} enregistrements';
	$lang['list_filtered_from']		= '(filtr&eacute; de {total_results} entr&eacute;es au total )';
	$lang['list_show_entries']		= 'Afficher {paging} entr&eacute;es';
	$lang['list_no_items']			= 'Aucun enregistrement à afficher';
	$lang['list_zero_entries']		= 'Affichage de 0 à 0 de 0 enregistrement';
	$lang['list_search'] 			= 'Recherche';
	$lang['list_search_all'] 		= 'Tous crit&egrave;res';
	$lang['list_clear_filtering'] 	= 'Initialiser le filtrage';
	$lang['list_delete'] 			= 'Supprimer';
	$lang['list_edit'] 				= '&eacute;diter';
	$lang['list_paging_first'] 		= 'Premier';
	$lang['list_paging_previous'] 	= 'Pr&eacute;c&eacute;dent';
	$lang['list_paging_next'] 		= 'Suivant';
	$lang['list_paging_last'] 		= 'Dernier';
	$lang['list_loading'] 			= 'En attente...';

	$lang['form_edit'] 				= '&eacute;diter';
	$lang['form_back_to_list'] 		= 'Retour à la liste';
	$lang['form_update_changes'] 	= 'Sauvegarder les modifications';
	$lang['form_cancel'] 			= 'Annuler';
	$lang['form_update_loading'] 	= 'En attente, mise à jour des changements...';
	$lang['update_success_message'] = 'Vos donn&eacute;es sont mises à jour avec succ&egrave;s.';
	$lang['form_go_back_to_list'] 	= 'Retourner à la liste';

	$lang['form_add'] 				= 'Ajouter';
	$lang['insert_success_message'] = 'Vos donn&eacute;es sont ajout&eacute;es à la base de donn&eacute;es avec succ&egrave;s.';
	$lang['form_or']				= 'ou';
	$lang['form_save'] 				= 'Enregistrer';
	$lang['form_insert_loading'] 	= 'En attente, entregistrement des donn&eacute;es...';

	$lang['form_upload_a_file'] 	= 'Chargement d\'un fichier';
	$lang['form_upload_delete'] 	= 'Supprimer';
	$lang['form_button_clear'] 		= 'Initialiser';

	$lang['delete_success_message'] = 'Vos donn&eacute;es sont supprim&eacute;es de la base de donn&eacute;es avec succ&egrave;s.';
	$lang['delete_error_message'] 	= 'Vos donn&eacute;es ne sont pas supprim&eacute;es de la base de donn&eacute;es.';

	/* Javascript messages */
	$lang['alert_add_form']			= 'Les donn&eacute;es que vous avez inser&eacute;es ne sont pas enregist&eacute;es.\\nÊtes vous sûr de vouloir retourner à la liste ?';
	$lang['alert_edit_form']		= 'Les donn&eacute;es que vous avez modifi&eacute;es ne sont pas enregist&eacute;es.\\nÊtes vous sûr de vouloir retourner à la liste ?';
	$lang['alert_delete']			= 'Êtes-vous sûr de vouloir supprimer cet enregistrement ?';

	$lang['insert_error']			= 'Une erreur est survenue lors de l\'op&eacute;ration d\'insertion.';
	$lang['update_error']			= 'Une erreur est survenue lors de l\'op&eacute;ration d\'enregistrement.';

	/* Added in version 1.2.1 */
	$lang['set_relation_title']		= 'Selectionner {field_display_as}';
	$lang['list_record']			= 'Enregistrement';
	$lang['form_inactive']			= 'inactif';
	$lang['form_active']			= 'actif';

	/* Added in version 1.2.2 */
	$lang['form_save_and_go_back']	= 'Enregistrer et retourner à la liste';
	$lang['form_update_and_go_back']= 'Mettre à jour et retourner à la liste';

	/* Upload functionality */
	$lang['string_delete_file'] 	= "Supprimer le fichier";
	$lang['string_progress'] 		= "Progression : ";
	$lang['error_on_uploading'] 	= "Une erreur est survenue lors du t&eacute;l&eacute;chargement";
	$lang['message_prompt_delete_file'] 	= "Êtes-vous sûr de vouloir supprimer ce fichier ?";

	$lang['error_max_number_of_files'] 	= "Vous pouvez t&eacute;l&eacute;charger un seul fichier à la fois.";
	$lang['error_accept_file_types'] 	= "Vous n'êtes pas autoris&eacute; à t&eacute;l&eacute;charger ce type de fichier.";
	$lang['error_max_file_size'] 		= "Le fichier d&eacute;passe la taille maximale autoris&eacute;e de {max_file_size}.";
	$lang['error_min_file_size'] 		= "Vous ne pouvez pas t&eacute;l&eacute;charger un fichier vide.";

	/* Added in version 1.3.1 */
	$lang['list_export'] 	= "Exporter";
	$lang['list_print'] 	= "Imprimer";
	$lang['minimize_maximize'] = 'R&eacute;duire/Agrandir';

	/* Added in version 1.4 */
	$lang['list_view'] = 'View';
