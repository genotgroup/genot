-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 26 Mai 2016 à 18:08
-- Version du serveur :  10.1.13-MariaDB
-- Version de PHP :  5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `genot`
--

-- --------------------------------------------------------

--
-- Structure de la table `anne_academique`
--

CREATE TABLE `anne_academique` (
  `id` int(11) NOT NULL,
  `annee` varchar(255) DEFAULT NULL,
  `id_precedente` int(11) DEFAULT NULL,
  `date_creation` date NOT NULL,
  `en_cour` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `anne_academique`
--

INSERT INTO `anne_academique` (`id`, `annee`, `id_precedente`, `date_creation`, `en_cour`) VALUES
(1, '2016-2017', NULL, '2016-05-01', 1),
(2, '2015-2016', NULL, '2015-05-03', 0),
(3, '2015-2016', 1, '2015-05-03', 0);

-- --------------------------------------------------------

--
-- Structure de la table `auditeurs`
--

CREATE TABLE `auditeurs` (
  `id` int(11) NOT NULL,
  `matricule` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `lieu_naiss` varchar(255) DEFAULT NULL,
  `date_naiss` date DEFAULT NULL,
  `id_specialite` int(11) DEFAULT NULL,
  `id_promotion` int(11) DEFAULT NULL,
  `id_annee_aca` int(11) DEFAULT NULL,
  `id_niveau` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `auditeurs`
--

INSERT INTO `auditeurs` (`id`, `matricule`, `nom`, `prenom`, `email`, `lieu_naiss`, `date_naiss`, `id_specialite`, `id_promotion`, `id_annee_aca`, `id_niveau`) VALUES
(263, '14EFP006', 'ABANDA MINKADA', 'JEAN JACQUES', '', 'MBALMAYO', '0000-00-00', 1, 1, 1, 1),
(264, '14EFP010', 'ABDOU', 'MAHAMA', '', 'MOZOGO', '0000-00-00', 2, 1, 1, 1),
(265, '14FPL02', 'ABONDO', 'MOISE ADONIS', '', 'SANGMELIMA', '0000-00-00', 3, 1, 1, 1),
(266, '14FPL09', 'AHMADOU ', 'TIDJANI MAYARAH', '', 'BANYO', '0000-00-00', 1, 1, 1, 1),
(267, '14CP006', 'AKEBANA', 'LUCIENNE', '', 'BETARE OYA', '0000-00-00', 2, 1, 1, 1),
(268, '14FPL24', 'AMBANI EBINI', 'ALAIN ROGER', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(269, '14EFP021', 'AMENLE EKOMANE', 'GERARD', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(270, '14EFP060', 'AMOUGOU BELINGA', 'DOMINIQUE', '', 'MVOUTESSI I', '0000-00-00', 2, 1, 1, 1),
(271, '14FPL28', 'ANGO NLOM', 'STEVE', '', 'EBOLOWA', '0000-00-00', 3, 1, 1, 1),
(272, '14EFP067', 'ANKA', 'CHRISTELLE', '', 'ANGUENGUE', '0000-00-00', 1, 1, 1, 1),
(273, '13GB004', 'ASSENE MAXIME SEVERIN', '-', '', 'BEKOUDOU', '0000-00-00', 2, 1, 1, 1),
(274, '14ECP040', 'ATANGANA BESSALA', 'NORBERT GASTIEN', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(275, '14CP023', 'ATANGANA ELOUNA', 'PIERRE  ', '', 'EVODOULA', '0000-00-00', 1, 1, 1, 1),
(276, '14EFP042', 'ATANGANA NGOA', 'THIERRY GABRIEL', '', 'YAOUNDE', '0000-00-00', 2, 1, 1, 1),
(277, '14EFP014', 'AWONO  ', 'FREDERIC', '', 'ELOM', '0000-00-00', 3, 1, 1, 1),
(278, '14FPL25', 'AYISSI AWONO', 'JEAN SIMEON', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(279, '14EFP024', 'BANGUE BANGKONBA', 'CHARLES', '', 'DOUALA', '0000-00-00', 2, 1, 1, 1),
(280, '14FPL10', 'BENE', 'HENOCK', '', 'NKONGSAMBA', '0000-00-00', 3, 1, 1, 1),
(281, '14CP001', 'BENGONO ESSINDI', 'RENE GRATIEN', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(282, '14CP021', 'BILOO MENGUE', 'GUY PATRICK', '', 'ZINGUI', '0000-00-00', 2, 1, 1, 1),
(283, '14EFP073', 'BLAOURA MAHAMOUDOU', 'BLAISE', '', 'MAGA', '0000-00-00', 3, 1, 1, 1),
(284, '14CP010', 'BODO', 'RAYMOND', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(285, '14CP011', 'BOMO', 'THIERRY LANDRY', '', 'NDEN', '0000-00-00', 2, 1, 1, 1),
(286, '14EFP031', 'BOURMEKE ', 'GHISLAIN NOEL', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(287, '14EFP043', 'CHECO NOUBISSI', 'YOLANDE', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(288, '14CP007', 'CHEGUE ', 'SYLVESTRE', '', 'NKONGSAMBA', '0000-00-00', 2, 1, 1, 1),
(289, '14FPL26', 'DEFO FOUEKA', 'FRANCIS GALLIOS', '', 'BADJOUN', '0000-00-00', 3, 1, 1, 1),
(290, '14EFP025', 'DJOMO ', 'LYNA CHRISTELLE', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(291, '14EFP003', 'DJONA ', 'ALPHONSE GUIDANG', '', 'VIRI', '0000-00-00', 2, 1, 1, 1),
(292, '14ECP045', 'DJOUALE TSAFACK', 'ALAIN GERALDIN', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(293, '14CP017', 'DOCKO DOCKO', 'EMILE LAMBERT', '', 'NKONGSAMBA', '0000-00-00', 1, 1, 1, 1),
(294, '14CP016', 'DOGUIMI', 'FRANCOIS', '', 'GAROUA', '0000-00-00', 2, 1, 1, 1),
(295, '14EFP044', 'DOKO EDJIANE', 'MATHIEU ALAIN', '', 'NKOLO EBOO', '0000-00-00', 3, 1, 1, 1),
(296, '14EFP040', 'EBOGO', 'LEONCE CLEMENT', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(297, '14EFP005', 'EBOLO', 'GEORGE ERIC', '', 'AYOS', '0000-00-00', 2, 1, 1, 1),
(298, '14EFP045', 'EBONGO', 'ARISBER TRESOR', '', 'DOUALA', '0000-00-00', 3, 1, 1, 1),
(299, '14CP019', 'ELIMBI NDOUMBE', 'EMMANUEL', '', 'DOUALA', '0000-00-00', 1, 1, 1, 1),
(300, '14EFP036', 'ESSENGUE ESSENGUE', 'VITUS', '', 'MBAKOMO', '0000-00-00', 2, 1, 1, 1),
(301, '14EFP046', 'ESSOUMA ', 'SALOME DIANE LAURE', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(302, '14EFP032', 'EVA MEYO', '-', '', 'KRIBI', '0000-00-00', 1, 1, 1, 1),
(303, '14EFP001', 'EVA MIMBE', 'MARTIN GUY', '', 'YAOUNDE', '0000-00-00', 2, 1, 1, 1),
(304, '14ECP043', 'EYELOKOBA ENDAMEYO', 'EMILE RENE', '', 'BATOURI', '0000-00-00', 3, 1, 1, 1),
(305, '14FPL11', 'EYENGA AZOMBO', 'ESTELLE EPSE ATANGANA', '', 'METET', '0000-00-00', 1, 1, 1, 1),
(306, '14EFP047', 'EYENGA NDJOMO', 'ELISEE AMOUR', '', 'SANGMELIMA', '0000-00-00', 2, 1, 1, 1),
(307, '14FPL23', 'EYENGA ONAMBELE', 'BARTHELEMY', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(308, '14CP013', 'EYIKE BAENLA', 'EMILE  ', '', 'EDEA', '0000-00-00', 1, 1, 1, 1),
(309, '14EFP048', 'FOSSO THUANKAM', 'ROSE CLAIRE ARMELLE MARTINE', '', 'MBO', '0000-00-00', 2, 1, 1, 1),
(310, '14CP020', 'FOUDA YANA', 'BASILE PATRICK', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(311, '14CP014', 'HYGINUS BINYUYVIDZEM', '-', '', 'KUMBO', '0000-00-00', 1, 1, 1, 1),
(312, '14FPL27', 'JOMBWEDE EMBOLA ', 'VICTOR EMMANUEL 2', '', 'DOUALA', '0000-00-00', 2, 1, 1, 1),
(313, '14EFP063', 'KALDA TCHETCHOULAI', 'DAVID', '', 'MOKOLO', '0000-00-00', 3, 1, 1, 1),
(314, '14EFP034', 'KAMKOUM YOYA', 'BETHEL', '', 'NDOUNGUE', '0000-00-00', 1, 1, 1, 1),
(315, '14FPL12', 'KAMTE POUENE', 'GUY BERTRAND', '', 'NKONGSAMBA', '0000-00-00', 2, 1, 1, 1),
(316, '14EFP065', 'KETCHA NANA', 'ODILE ', '', 'NDOUNGUE', '0000-00-00', 3, 1, 1, 1),
(317, '14CP08', 'KINGUE', 'ERIC MARC VICTOR', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(318, '14FPL22', 'KOUAMOU WANDJI', 'CEDRIC', '', 'YAOUNDE', '0000-00-00', 2, 1, 1, 1),
(319, '14FPL01', 'KOUNDI', 'PATRICK SERGE', '', 'MESSAMENA', '0000-00-00', 3, 1, 1, 1),
(320, '14CP022', 'KOUNGANG ', 'FRANCIS CLAUDE', '', 'DOUALA', '0000-00-00', 1, 1, 1, 1),
(321, '14EFP004', 'LOABALBE ', 'FLAVIEN', '', 'EBOLOWA', '0000-00-00', 2, 1, 1, 1),
(322, '13CP018', 'LONO', 'MIASSE', '', 'DOUME', '0000-00-00', 3, 1, 1, 1),
(323, '14FPL21', 'LOTY', 'PIERRE JEAN DANIEL', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(324, '14CP018', 'LOUIS ZACHEE', 'MBOCK', '', 'AKOM II', '0000-00-00', 2, 1, 1, 1),
(325, '14CP002', 'MANGA', 'MARC', '', 'AKONOLINGA', '0000-00-00', 3, 1, 1, 1),
(326, '14CP026', 'MANGELE ', 'SEBASTIEN THEOPHILE', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(327, '14EFP049', 'MANTSANA MISS', 'HERMINE', '', 'YAOUNDE', '0000-00-00', 2, 1, 1, 1),
(328, '14FPL13', 'MATEO SESE NCHAMA', '-', '', 'BATA', '0000-00-00', 3, 1, 1, 1),
(329, '14EFP016', 'MBALA MBIE', 'MARCELIN', '', 'NKONGNTSAM', '0000-00-00', 1, 1, 1, 1),
(330, '14EFP050', 'MBARGA ', 'IGNACE BLAISE', '', 'AKOM', '0000-00-00', 2, 1, 1, 1),
(331, '14FPL19', 'MBELLE', 'ELISABETH YOLLANDE', '', 'NGOME DZAP', '0000-00-00', 3, 1, 1, 1),
(332, '14EFP002', 'MBOH', 'PATRICE LUMUMBA', '', 'BAMENDA', '0000-00-00', 1, 1, 1, 1),
(333, '14EFP072', 'MBOUMO NJOYA', 'SOULEMANOU', '', 'NJISSE', '0000-00-00', 2, 1, 1, 1),
(334, '14EFP051', 'MEKONGO ONDOUA', 'ADALBERT SERAPHINS', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(335, '14EFP033', 'MELINGUI EPSE ESSOMBA', 'GERMAINE', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(336, '14ECP042', 'MENDANA BALLA', 'FRANCK ACHILLE', '', 'YAOUNDE', '0000-00-00', 2, 1, 1, 1),
(337, '14FPL04', 'MERSUH', 'LAETITIA', '', 'BAMENDA', '0000-00-00', 3, 1, 1, 1),
(338, '14EFP052', 'MFONO MBA', 'ANNE MARIE', '', 'OLAM ZE', '0000-00-00', 1, 1, 1, 1),
(339, '14ECP044', 'MFOUAPON NJUAMSHETKY', 'HENOCK', '', 'FOUBAN', '0000-00-00', 2, 1, 1, 1),
(340, '14CP029', 'MOUKAM MENDJIADEU', 'M.MAKEBA', '', 'KEMKEM', '0000-00-00', 3, 1, 1, 1),
(341, '14CP003', 'MOUSSA SALI', '-', '', 'DIMEO', '0000-00-00', 1, 1, 1, 1),
(342, '14EFP022', 'MOZOGO', 'ANDRE', '', 'MOKOLO', '0000-00-00', 2, 1, 1, 1),
(343, '14EFP053', 'MPABE FADIMATOU', 'MIREILLE', '', 'MOKOLO', '0000-00-00', 3, 1, 1, 1),
(344, '14CP034', 'MVELE', 'TLESPHORE IRENEE', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(345, '14ECP041', 'MVO MVO', 'FRANCOIS', '', 'ZOETELE', '0000-00-00', 2, 1, 1, 1),
(346, '14EFP026', 'MVONDO EYA', 'SIMON PIERRE', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(347, '14FPL16', 'MVONDO JULES RODRIGUE', '-', '', 'DJOUM', '0000-00-00', 1, 1, 1, 1),
(348, '14EFP070', 'NANA FADIMATOU', 'NOUHOU ADAMOU', '', 'MAROUA', '0000-00-00', 2, 1, 1, 1),
(349, '14FPL17', 'NDANA MEKOGO', 'RAYMOND', '', 'ABONG MBANG', '0000-00-00', 3, 1, 1, 1),
(350, '14EFP015', 'NDONGO MOLLO', 'MICHEL', '', 'NKOM 1', '0000-00-00', 1, 1, 1, 1),
(351, '14EFP054', 'NGAH ASSENE', 'HONORINE NADINE', '', 'YAOUNDE', '0000-00-00', 2, 1, 1, 1),
(352, '14FPL05', 'NGAH RACHEL', '-', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(353, '14EFP055', 'NGAVANGA ', 'NICAISE MAGLOIRE', '', 'YAOUNDE DJOUNGOLO', '0000-00-00', 1, 1, 1, 1),
(354, '14CP004', 'NGO BILLONG', 'CARINE', '', 'SANGMELAMA', '0000-00-00', 2, 1, 1, 1),
(355, '14EFP018', 'NGO MISSAM-HAN', 'MARGUERITE CHANTAL', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(356, '14EFP009', 'NGO O DJOB ', 'DESIRE', '', 'EDEA', '0000-00-00', 1, 1, 1, 1),
(357, '14EFP056', 'NJI ', 'PASCAL NDIKUM', '', 'MBATU', '0000-00-00', 2, 1, 1, 1),
(358, '14EFP057', 'NKAZIE MOUCHIPOU', 'MOHAMED DESIRE', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(359, '14EFP064', 'NKE', 'CHRISTINE VIVIANE', '', 'MBANKOMO', '0000-00-00', 1, 1, 1, 1),
(360, '14FPL18', 'NKENE ', 'ESTHER NICOLE', '', 'ESEKA', '0000-00-00', 2, 1, 1, 1),
(361, '14EFP028', 'NKENG MULUH', '-', '', 'BANGWA', '0000-00-00', 3, 1, 1, 1),
(362, '14EFP058', 'NKOA ESSOMBA', 'ELISEE', '', 'SANGMELIMA', '0000-00-00', 1, 1, 1, 1),
(363, '14EFP011', 'NKOLO EBENE', 'CREPAIN', '', 'OLEKOTSING', '0000-00-00', 2, 1, 1, 1),
(364, '14EFP059', 'NKOUMBA ESSEBE', 'JEAN DIDIER', '', 'FANG-BILOUN', '0000-00-00', 3, 1, 1, 1),
(365, '14EFP037', 'NNA EDIMENGO', 'JOE MARIUS', '', 'ENONGAL EBOLOWA', '0000-00-00', 1, 1, 1, 1),
(366, '14EFP038', 'NYNYING', 'MOSES NYINCHIA', '', 'OKU', '0000-00-00', 2, 1, 1, 1),
(367, '14CP028', 'NZHIE', 'ARSENE SIDOINE', '', 'KRIBI', '0000-00-00', 3, 1, 1, 1),
(368, '14FPL06', 'OMGBA AMOUGOU', 'JEAN', '', 'MANENGOTENG', '0000-00-00', 1, 1, 1, 1),
(369, '14FPL20', 'ONDOBOE EPSE ESSOMO EDOUA', 'GENEVIEVE LOUISETTE', '', 'NKOMO 1', '0000-00-00', 2, 1, 1, 1),
(370, '14FPL14', 'ONGTOUON', 'MELANIE CELINE', '', 'NDIKINEMEKI', '0000-00-00', 3, 1, 1, 1),
(371, '14FPL15', 'OTIA', 'FERDINAND BOJA', '', 'EBOLOWA', '0000-00-00', 1, 1, 1, 1),
(372, '14EFP019', 'OUSMANOU NASSOUROU', '-', '', 'MAROUA', '0000-00-00', 2, 1, 1, 1),
(373, '14EFP039', 'PETER GIYO', 'YERIMA', '', 'NWA', '0000-00-00', 3, 1, 1, 1),
(374, '14CP024', 'REGINA AGBEKOME', 'MBU', '', 'NGUTI', '0000-00-00', 1, 1, 1, 1),
(375, '14FPL07', 'SOULEYMAN', '-', '', 'MOULVOUDAYE', '0000-00-00', 2, 1, 1, 1),
(376, '14FPL08', 'TADJUIDJE GUIASSU', 'ISAAC', '', 'BAYAM', '0000-00-00', 3, 1, 1, 1),
(377, '14EFP061', 'TAMBE SANGAYA EYONG', '-', '', 'YAOUNDE', '0000-00-00', 1, 1, 1, 1),
(378, '14CP025', 'TANANG TCHOUALA', 'PATRICE', '', 'ENONGAL', '0000-00-00', 2, 1, 1, 1),
(379, '14EFP035', 'TCHIDJO SOBGUE', 'PIERRE ERIC', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(380, '14EFP013', 'TCHIENGANG DJUEKOU', 'ADRIEN EMERIC', '', 'GAROUA', '0000-00-00', 1, 1, 1, 1),
(381, '14FPL03', 'TIAMBOU', 'NATHALIE CLAIRE', '', 'BAFOUSSAM', '0000-00-00', 2, 1, 1, 1),
(382, '14EFP041', 'TITA SHIPUH ', 'VICTOR MBANYA', '', 'BABA 1', '0000-00-00', 3, 1, 1, 1),
(383, '14CP008', 'TOH MAH', 'BRIDGET', '', 'BENGWI', '0000-00-00', 1, 1, 1, 1),
(384, '14EFP023', 'TOURE SOUMAILA ', 'EL MADANE', '', 'GOUNDAM-MALI', '0000-00-00', 2, 1, 1, 1),
(385, '14CP015', 'TSOUNGUI', 'JEAN CLAUDE', '', 'YAOUNDE', '0000-00-00', 3, 1, 1, 1),
(386, '14EFP062', 'WAMBA BERTOING', 'JEAN CLAUDE', '', 'KAELE', '0000-00-00', 1, 1, 1, 1),
(387, '14EFP029', 'WANGSO TCHOBWE', 'DIEUDONNE  ', '', 'DOUKOULA', '0000-00-00', 2, 1, 1, 1),
(388, '14EFP020', 'WILLIAM', 'AROUNG', '', 'DIAMARE', '0000-00-00', 3, 1, 1, 1),
(389, '14CP005', 'YAOUBA', 'SAYOU', '', 'GUIDIGUYS', '0000-00-00', 1, 1, 1, 1),
(390, '14EFP017', 'YAYA', 'ADJI', '', 'MAROUA', '0000-00-00', 2, 1, 1, 1),
(391, '14EFP012', 'ZE ', 'BERTIN', '', 'FANG-BIKANG 2', '0000-00-00', 3, 1, 1, 1),
(392, '14EFP071', 'ZEH ZANGA ', 'ALBERT STEVE', '', 'EDOM', '0000-00-00', 0, 0, 0, 0),
(393, '14EFP008', 'ZOUA', 'ALBERT', '', 'MALANEGOME', '0000-00-00', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('3b0c5dbd93c5843e960d97dc1d422ccb', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:46.0) Gecko/20100101 Firefox/46.0', 1464278656, 'a:5:{s:9:"user_data";s:0:"";s:10:"flexi_auth";a:7:{s:15:"user_identifier";s:15:"admin@admin.com";s:7:"user_id";s:2:"34";s:5:"admin";b:1;s:5:"group";a:1:{i:3;s:12:"Master Admin";}s:10:"privileges";a:10:{i:1;s:10:"View Users";i:3;s:15:"View Privileges";i:4;s:18:"Insert User Groups";i:5;s:17:"Insert Privileges";i:6;s:12:"Update Users";i:7;s:18:"Update User Groups";i:8;s:17:"Update Privileges";i:9;s:12:"Delete Users";i:10;s:18:"Delete User Groups";i:11;s:17:"Delete Privileges";}s:22:"logged_in_via_password";b:1;s:19:"login_session_token";s:40:"1be3c7c36d37c3727c8cffa180c31176d049588b";}s:18:"id_anne_academique";s:1:"1";s:15:"anne_academique";s:9:"2016-2017";s:26:"anne_academique_precedente";N;}');

-- --------------------------------------------------------

--
-- Structure de la table `evaluation`
--

CREATE TABLE `evaluation` (
  `id` int(11) NOT NULL,
  `pourcentage` float DEFAULT NULL,
  `id_type_evaluation` int(11) DEFAULT NULL,
  `id_matiere` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

CREATE TABLE `matiere` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_module` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_semestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `moyenne`
--

CREATE TABLE `moyenne` (
  `id` int(11) NOT NULL,
  `moyenne` float DEFAULT NULL,
  `id_auditeur` int(11) DEFAULT NULL,
  `id_module` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `niveau`
--

CREATE TABLE `niveau` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `niveau`
--

INSERT INTO `niveau` (`id`, `nom`, `code`) VALUES
(1, 'Tronc Commun', 'TC');

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `note` float DEFAULT NULL,
  `id_auditeur` int(11) DEFAULT NULL,
  `id_evaluation` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_annee_aca` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `promotion`
--

INSERT INTO `promotion` (`id`, `nom`, `id_annee_aca`) VALUES
(1, 'Elit One', 1);

-- --------------------------------------------------------

--
-- Structure de la table `semestre`
--

CREATE TABLE `semestre` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `id_annee_aca` int(11) DEFAULT NULL,
  `id_niveau` int(11) DEFAULT NULL,
  `id_specialite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `semestre`
--

INSERT INTO `semestre` (`id`, `nom`, `code`, `id_annee_aca`, `id_niveau`, `id_specialite`) VALUES
(1, 'TODO Semestre', 'Sem1', 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

CREATE TABLE `specialite` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `specialite`
--

INSERT INTO `specialite` (`id`, `nom`, `code`) VALUES
(1, 'Gestion Budgetaire', 'GB'),
(2, 'Comptabilité Publique', 'CP'),
(3, 'Finances Publiques Locales', 'FPL');

-- --------------------------------------------------------

--
-- Structure de la table `type_evaluation`
--

CREATE TABLE `type_evaluation` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `uacc_id` int(11) UNSIGNED NOT NULL,
  `uacc_group_fk` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `uacc_email` varchar(100) NOT NULL DEFAULT '',
  `uacc_username` varchar(15) NOT NULL DEFAULT '',
  `uacc_password` varchar(60) NOT NULL DEFAULT '',
  `uacc_ip_address` varchar(40) NOT NULL DEFAULT '',
  `uacc_salt` varchar(40) NOT NULL DEFAULT '',
  `uacc_activation_token` varchar(40) NOT NULL DEFAULT '',
  `uacc_forgotten_password_token` varchar(40) NOT NULL DEFAULT '',
  `uacc_forgotten_password_expire` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uacc_update_email_token` varchar(40) NOT NULL DEFAULT '',
  `uacc_update_email` varchar(100) NOT NULL DEFAULT '',
  `uacc_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `uacc_suspend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `uacc_fail_login_attempts` smallint(5) NOT NULL DEFAULT '0',
  `uacc_fail_login_ip_address` varchar(40) NOT NULL DEFAULT '',
  `uacc_date_fail_login_ban` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Time user is banned until due to repeated failed logins',
  `uacc_date_last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uacc_date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_accounts`
--

INSERT INTO `user_accounts` (`uacc_id`, `uacc_group_fk`, `uacc_email`, `uacc_username`, `uacc_password`, `uacc_ip_address`, `uacc_salt`, `uacc_activation_token`, `uacc_forgotten_password_token`, `uacc_forgotten_password_expire`, `uacc_update_email_token`, `uacc_update_email`, `uacc_active`, `uacc_suspend`, `uacc_fail_login_attempts`, `uacc_fail_login_ip_address`, `uacc_date_fail_login_ban`, `uacc_date_last_login`, `uacc_date_added`, `name`) VALUES
(1, 3, 'anatoleabe@gmail.com', 'admin', '$2a$08$lSOQGNqwBFUEDTxm2Y.hb.mfPEAt/iiGY9kJsZsd4ekLJXLD.tCrq', '154.72.161.122', 'XKVT29q2Jr', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 2, '127.0.0.1', '0000-00-00 00:00:00', '2016-05-09 11:10:54', '2011-01-01 00:00:00', 'ABENA Jean Louis'),
(5, 1, 'asanga.achidi@pnlt.cm', 'asanga', '$2a$08$XeM4l5dxmtCeG17V1wofQOaOqxoo0fVBLru7vYu87vdzLeKdvHmke', '::1', 'Sq28yKNmTV', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-19 14:40:32', '2015-05-19 14:40:32', NULL),
(6, 1, 'balla.henri@pnlt.cm', 'balla1', '$2a$08$D5FX800323JKVZ0iJyxMoOPfHPr4IzqrYMnBJM6IGN82bCawNf25a', '::1', 'w899ggMFBD', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-19 15:43:25', '2015-05-19 14:42:28', NULL),
(7, 1, 'efangon.philemon@pnlt.cm', 'efangon', '$2a$08$5iqXOxknl32QbAFh6/h.4e1KpIaUAfIrZcxUZlWXjIpL5HwWODofC', '154.72.134.225', 'm4WwcPScbS', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 15:47:20', '2015-05-19 14:43:11', NULL),
(8, 1, 'efouzou.eveline@pnlt.cm', 'efouzou', '$2a$08$a/O6t1KWjS8cPvSb2KaWXeqIYADMKo29hySC8f.6kx5rJsxs4ETgC', '154.72.132.228', '4cKqdRQCw4', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-07-13 14:47:19', '2015-05-19 14:44:01', NULL),
(9, 1, 'ella.therese.bella@pnlt.cm', 'ella.therese', '$2a$08$0kHNfw5M1OKaT/SPaLS/tetfy5GTsRCONoWdD1gpvV29o25rsCe1y', '41.205.81.234', 'GP84Cw5M6K', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 1, '41.204.86.76', '0000-00-00 00:00:00', '2015-06-01 09:25:00', '2015-05-19 14:44:47', NULL),
(10, 1, 'fouda.olivier@pnlt.cm', 'fouda1', '$2a$08$Wk7DS2GJLkqCtC4LYTCI3e1bKYsgZ.KkpkLH0uOPaONK.VrHL96/m', '154.72.132.86', 'WhV2vrDY5w', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-07-18 17:23:03', '2015-05-19 14:45:32', NULL),
(11, 1, 'mbassa.vincent@pnlt.cm', 'mbassa', '$2a$08$t1HQwxHyAG3qY6jIlORLHO7Z8zrAijCWz4DttyC5EkqSPYo3cfRCW', '154.72.132.126', 'FWM82dSTGG', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-06-05 11:32:26', '2015-05-19 14:46:13', NULL),
(12, 1, 'ndi.ndi.joseph@pnlt.cm', 'ndi.ndi', '$2a$08$I9dW8WB5JeYDWJXLiVylBOLG31H28g6ii4M0w.b7fvlskUv2JGm/6', '::1', 'Wh6skpGGSp', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2016-05-16 20:36:11', '2015-05-19 14:47:09', NULL),
(13, 1, 'nguafack.dubliss@pnlt.cm', 'nguafack', '$2a$08$MQ5PDVFysjWIXnrLTCadLeEwM79SN3Kuw5nx5e0KY6z7Pa6lBpNoG', '41.204.86.31', 'kZvQYpWR38', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 5, '154.72.132.95', '2016-04-01 17:05:59', '2015-08-05 11:00:36', '2015-05-19 14:47:46', NULL),
(14, 1, 'tagnenousse.jean@pnlt.cm', 'tagnenousse', '$2a$08$aY/wNB.MEx10XyJ9dumdn.P9rrVTfWfcqinlj2w1el4pXiT2b8PTq', '::1', 'ZtkPmj362m', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 12, '154.72.133.90', '2015-09-16 12:46:50', '2015-05-19 14:57:10', '2015-05-19 14:48:32', NULL),
(15, 1, 'etoundi.antoine@pnlt.cm', 'etoundi', '$2a$08$HOFmo/gGUxoyLE2L1GTJ3.9HHu1oPaaAg842XCc5EgBVfXDDFh1yq', '154.72.135.173', 'D79zKQKHcy', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-09-28 16:28:18', '2015-05-19 14:49:18', NULL),
(16, 1, 'nantchouang.alain@pnlt.cm', 'nantchouang', '$2a$08$4OD6v.3BRXwt95ovVFtjQOJX1pnpwk99LrdMJg5Qy4Ajq6iJZ7nTm', '154.72.132.135', 'RbxFbZcpWK', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-06-02 13:23:25', '2015-05-19 14:49:53', NULL),
(17, 1, 'abena.jeanlouis@pnlt.cm', 'jlabena', '$2a$08$9xotc3sNuBg/8JSFOvS7pedjuAEqfNq5nRbEscNg72LRzEkRrB24.', '154.72.161.122', 'FDMq9MYbQZ', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2016-05-09 10:30:39', '2015-05-19 14:53:05', NULL),
(18, 1, 'belinga.mvondo.edwige@pnlt.cm', 'belinga', '$2a$08$KP5hpG/OGX7g5Dqju/Lv9OyokbQP3hbEaQeUjOCggeuBsUNseLF3G', '154.72.134.225', 'WnWvhNQ8c7', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:50:20', '2015-05-22 10:50:20', NULL),
(19, 1, 'kemenang.edie@pnlt.cm', 'kemenang', '$2a$08$RAe49VJBpKRaJW4XJZUtg.nhDcqE7zGRm8M5Ctof0GJvVtGgUkHva', '154.72.134.225', 'fr5v7cf6zT', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:52:21', '2015-05-22 10:52:21', NULL),
(20, 1, 'tsawo.paulin@pnlt.cm', 'tsawop', '$2a$08$VovNohwGkemVlzP1J48As.vtdx9WkseAWmycZtgBrfu2PpbHqzBTq', '154.72.134.225', '7rsVB7R3gr', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:54:53', '2015-05-22 10:54:53', NULL),
(21, 1, 'kaoussiri.brekmo@pnlt.cm', 'kaoussiri', '$2a$08$mV.92qVoBS0aS2O4VP7JteplwQ5Lq6Zhbh51Aoau5Mlu4r2FaztSK', '154.72.134.225', 'R75Yy2C7BB', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:55:44', '2015-05-22 10:55:44', NULL),
(22, 1, 'nkou.bikoe@pnlt.cm', 'nkou.bikoe', '$2a$08$cYgqGLcK6E.iXbuuwmguxO1jA3QxJNrhPUIpUPbuqgust0MGtGUoq', '154.72.134.225', 'rJXwW49TJD', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:57:39', '2015-05-22 10:57:39', NULL),
(23, 1, 'nju.felix@pnlt.cm', 'nju.felix', '$2a$08$up5Ge9Erej8kcVlAS.4VRuo7Yo/LRNckMigjuv1EwFbUatGrTPQmO', '154.72.134.225', 'Y3s284fHD4', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:00:46', '2015-05-22 11:00:46', NULL),
(24, 1, 'ossima.gilles.andre@pnlt.cm', 'ossima', '$2a$08$2dcU8zBDRHkRTRHyn1nCM.ElCLvkLYGqaSgA1Gjk568rN2.K0yrAK', '154.72.134.225', '8zTz7nvMJp', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:01:47', '2015-05-22 11:01:47', NULL),
(25, 1, 'fon.elisabeth@pnlt.cm', 'elisabeth', '$2a$08$QRYnc5GGCjzOif7f5ut/duk/by0zAaQbWcjKz0UBKj2wQBmrI3q5a', '154.72.134.225', 'cTphFZpy9b', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:02:49', '2015-05-22 11:02:49', NULL),
(26, 1, 'tchieche.charlotte@pnlt.cm', 'tchieche', '$2a$08$XIFbCPgg1M18yuoLZV9iS.9r7a5PIjIriHOtuxsKvOYCBi7t4AdwS', '154.72.134.225', 'CbnpvHr9Wf', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:27:39', '2015-05-22 11:27:39', NULL),
(27, 1, 'njock.ayuk@pnlt.cm', 'njock.ayuk', '$2a$08$tIuo.PtQLllmVYmmf.RsQeCv1QLDk7RC0YIr.lEPBWDxqL0YSqGvO', '154.72.134.225', 'k29BSYVxbV', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:30:17', '2015-05-22 11:30:17', NULL),
(28, 1, 'lum.pascaline@pnlt.cm', 'pascaline', '$2a$08$KrcBLCtxv/c6sSV1vnqjR.4wIuw3pW1cNYs/eAnYHKjUv5a6XyskG', '154.72.134.225', '9fctYVtGJS', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:31:01', '2015-05-22 11:31:01', NULL),
(29, 1, 'moungou.tsondo@pnlt.cm', 'moungou', '$2a$08$9e2Rv8VkBeTsSD2J.EXr7./R6Xdhkcq109Muc6RG0jBmQw/93bmg.', '154.72.134.225', 'hPq2xpfgk8', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:32:36', '2015-05-22 11:32:36', NULL),
(30, 1, 'melody.emann@pnlt.cm', 'melody', '$2a$08$KxuivaRSzOMa8BdxZMDAAuWw84EACHhIT2IP/xNW8bK0YJfJNPZF2', '41.204.86.76', 'jSzrsPzgqf', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-07-22 13:31:21', '2015-05-22 11:34:43', NULL),
(31, 1, 'noumo@pnlt.cm', 'onoumo', '$2a$08$sLK7lO0vGbUcnRr1ezonKuGp/i3u2g6rG.euxTC0l.7WNnjTn45yG', '154.72.134.225', 'pbYGPwXcRR', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:37:04', '2015-05-22 11:37:04', NULL),
(32, 1, 'simo.nenwouo@pnlt.cm', 'nenwouo', '$2a$08$VL38AEeP5PzWtJ3EBi2sLOC1QG9O7XnQh974hLVY8we20QSWrFoJ6', '154.72.134.225', '3XGCNQ3QPv', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:38:14', '2015-05-22 11:38:14', NULL),
(34, 3, 'admin@admin.com', 'adminflex', '$2a$08$lSOQGNqwBFUEDTxm2Y.hb.mfPEAt/iiGY9kJsZsd4ekLJXLD.tCrq', '127.0.0.1', 'XKVT29q2Jr', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2016-05-26 17:34:34', '2011-01-01 00:00:00', '');

-- --------------------------------------------------------

--
-- Structure de la table `user_address`
--

CREATE TABLE `user_address` (
  `uadd_id` int(11) NOT NULL,
  `uadd_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `uadd_alias` varchar(50) NOT NULL DEFAULT '',
  `uadd_recipient` varchar(100) NOT NULL DEFAULT '',
  `uadd_phone` varchar(25) NOT NULL DEFAULT '',
  `uadd_company` varchar(75) NOT NULL DEFAULT '',
  `uadd_address_01` varchar(100) NOT NULL DEFAULT '',
  `uadd_address_02` varchar(100) NOT NULL DEFAULT '',
  `uadd_city` varchar(50) NOT NULL DEFAULT '',
  `uadd_county` varchar(50) NOT NULL DEFAULT '',
  `uadd_post_code` varchar(25) NOT NULL DEFAULT '',
  `uadd_country` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_groups`
--

CREATE TABLE `user_groups` (
  `ugrp_id` smallint(5) NOT NULL,
  `ugrp_name` varchar(20) NOT NULL DEFAULT '',
  `ugrp_desc` varchar(100) NOT NULL DEFAULT '',
  `ugrp_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_groups`
--

INSERT INTO `user_groups` (`ugrp_id`, `ugrp_name`, `ugrp_desc`, `ugrp_admin`) VALUES
(1, 'Public', 'Public User : has no admin access rights.', 0),
(2, 'Moderator', 'Admin Moderator : has partial admin access rights.', 1),
(3, 'Master Admin', 'Master Admin : has full admin access rights.', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_login_sessions`
--

CREATE TABLE `user_login_sessions` (
  `usess_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `usess_series` varchar(40) NOT NULL DEFAULT '',
  `usess_token` varchar(40) NOT NULL DEFAULT '',
  `usess_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_login_sessions`
--

INSERT INTO `user_login_sessions` (`usess_uacc_fk`, `usess_series`, `usess_token`, `usess_login_date`) VALUES
(12, '', '024f31989cf327048c9360fdd7d28714b1fe2e78', '2016-05-16 20:36:12'),
(34, '', '1be3c7c36d37c3727c8cffa180c31176d049588b', '2016-05-26 18:05:01'),
(12, '', '7b09b73fd01e1ed9f36a8067ab0f7d001051c0e7', '2016-05-16 16:49:37'),
(12, '', '93e1c58d87a52f50059870e54c700ad40fcd0f66', '2016-05-15 23:47:31'),
(34, '', 'f6ea3d6332480e031abc0151b1af5736427ee0c0', '2016-05-26 14:49:01');

-- --------------------------------------------------------

--
-- Structure de la table `user_privileges`
--

CREATE TABLE `user_privileges` (
  `upriv_id` smallint(5) NOT NULL,
  `upriv_name` varchar(20) NOT NULL DEFAULT '',
  `upriv_desc` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_privileges`
--

INSERT INTO `user_privileges` (`upriv_id`, `upriv_name`, `upriv_desc`) VALUES
(1, 'View Users', 'User can view user account details.'),
(2, 'View User Groups', 'User can view user groups.'),
(3, 'View Privileges', 'User can view privileges.'),
(4, 'Insert User Groups', 'User can insert new user groups.'),
(5, 'Insert Privileges', 'User can insert privileges.'),
(6, 'Update Users', 'User can update user account details.'),
(7, 'Update User Groups', 'User can update user groups.'),
(8, 'Update Privileges', 'User can update user privileges.'),
(9, 'Delete Users', 'User can delete user accounts.'),
(10, 'Delete User Groups', 'User can delete user groups.'),
(11, 'Delete Privileges', 'User can delete user privileges.'),
(12, 'Print Global', 'Print Global');

-- --------------------------------------------------------

--
-- Structure de la table `user_privilege_groups`
--

CREATE TABLE `user_privilege_groups` (
  `upriv_groups_id` smallint(5) UNSIGNED NOT NULL,
  `upriv_groups_ugrp_fk` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `upriv_groups_upriv_fk` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_privilege_groups`
--

INSERT INTO `user_privilege_groups` (`upriv_groups_id`, `upriv_groups_ugrp_fk`, `upriv_groups_upriv_fk`) VALUES
(1, 3, 1),
(3, 3, 3),
(4, 3, 4),
(5, 3, 5),
(6, 3, 6),
(7, 3, 7),
(8, 3, 8),
(9, 3, 9),
(10, 3, 10),
(11, 3, 11),
(12, 2, 2),
(13, 2, 4),
(14, 2, 5);

-- --------------------------------------------------------

--
-- Structure de la table `user_privilege_users`
--

CREATE TABLE `user_privilege_users` (
  `upriv_users_id` smallint(5) NOT NULL,
  `upriv_users_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `upriv_users_upriv_fk` smallint(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_privilege_users`
--

INSERT INTO `user_privilege_users` (`upriv_users_id`, `upriv_users_uacc_fk`, `upriv_users_upriv_fk`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(16, 8, 12),
(17, 1, 12),
(18, 17, 12);

-- --------------------------------------------------------

--
-- Structure de la table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `upro_id` int(11) NOT NULL,
  `upro_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `upro_company` varchar(50) NOT NULL DEFAULT '',
  `upro_first_name` varchar(50) NOT NULL DEFAULT '',
  `upro_last_name` varchar(50) NOT NULL DEFAULT '',
  `upro_phone` varchar(25) NOT NULL DEFAULT '',
  `upro_newsletter` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user_profiles`
--

INSERT INTO `user_profiles` (`upro_id`, `upro_uacc_fk`, `upro_company`, `upro_first_name`, `upro_last_name`, `upro_phone`, `upro_newsletter`) VALUES
(1, 1, '', 'Anatole ', 'ABE', '237679694420', 0),
(5, 5, '', 'ZACCHEOUS', 'ASANGA ACHIDI ', '689456123', 1),
(6, 6, '', 'Henri', 'BALLA MBARGA', '689456123', 1),
(7, 7, '', 'Philémon', 'EFANGON AWOMO', '689456123', 1),
(8, 8, '', 'Eveline Sylvie', 'MBESSA épse EFOUZOU ', '689456123', 1),
(9, 9, '', 'Thérèse ', 'ELLA BELLA', '689456123', 1),
(10, 10, '', 'Olivier Désiré', 'FOUDA ', '689456123', 1),
(11, 11, '', 'Vincent', 'MBASSA ', '689456123', 1),
(12, 12, '', 'Joseph', 'NDI NDI', '689456123', 1),
(13, 13, '', 'Dubliss ', 'NGUAFACK NJIMOH', '689456123', 1),
(14, 14, '', 'JEAN JULES', 'TAGNE NOUSSE ', '689456123', 1),
(15, 15, '', 'Antoine de Padoue', ' ETOUNDI EVOUNA ', '689456123', 1),
(16, 16, '', 'Alain', 'NANTCHOUANG  PIEDJOU', '689456123', 1),
(17, 17, '', 'Jean Louis', 'ABENA FOE ', '689456123', 1),
(18, 18, '', 'EDWIGE', 'BELINGA Née MVONDO ABENG ', '689456123', 1),
(19, 19, '', 'Alain', ' Kemenang Edie ', '777777', 1),
(20, 20, '', 'Paulin Castro', ' TSAWO ', '777777', 1),
(21, 21, '', 'BREKMO ', ' KAOUSSIRI ', '777777', 1),
(22, 22, '', 'Adolphe', ' NKOU BIKOE', '777777', 1),
(23, 23, '', 'Félix', ' NJU ', '777777', 1),
(24, 24, '', 'Gilles André', 'OSSIMA ', '777777', 1),
(25, 25, '', 'Élizabeth', ' FON ', '777777', 1),
(26, 26, '', 'CHARLOTTE', 'TCHIECHE ', '777777', 1),
(27, 27, '', 'Léo', ' NJOCK AYUK', '777777', 1),
(28, 28, '', 'Pascaline', 'LUM ', '777777', 1),
(29, 29, '', 'Thérèse', 'Mme MOUNGOU TSONDO Epse OHANDZA ', '777777', 1),
(30, 30, '', 'MELODY', 'AVOULOU EMANN ', '777777', 0),
(31, 31, '', 'Odette', 'NOUMO épse KEMGANG', '777777', 1),
(32, 32, '', 'Léonie', 'SIMO épse NENWOUO ', '777777', 1),
(33, 34, '', 'Administrator', 'AD', '677 77 77 77', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `anne_academique`
--
ALTER TABLE `anne_academique`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `auditeurs`
--
ALTER TABLE `auditeurs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_specialite` (`id_specialite`),
  ADD KEY `id_promotion` (`id_promotion`),
  ADD KEY `id_annee_aca` (`id_annee_aca`),
  ADD KEY `id_niveau` (`id_niveau`);

--
-- Index pour la table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity` (`last_activity`);

--
-- Index pour la table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_type_evaluation` (`id_type_evaluation`),
  ADD KEY `id_matiere` (`id_matiere`);

--
-- Index pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_module` (`id_module`);

--
-- Index pour la table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_semestre` (`id_semestre`);

--
-- Index pour la table `moyenne`
--
ALTER TABLE `moyenne`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_auditeur` (`id_auditeur`),
  ADD KEY `id_module` (`id_module`);

--
-- Index pour la table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_auditeur` (`id_auditeur`),
  ADD KEY `id_evaluation` (`id_evaluation`);

--
-- Index pour la table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_annee_aca` (`id_annee_aca`);

--
-- Index pour la table `semestre`
--
ALTER TABLE `semestre`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_annee_aca` (`id_annee_aca`),
  ADD KEY `id_niveau` (`id_niveau`),
  ADD KEY `id_specialite` (`id_specialite`);

--
-- Index pour la table `specialite`
--
ALTER TABLE `specialite`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `type_evaluation`
--
ALTER TABLE `type_evaluation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`uacc_id`),
  ADD UNIQUE KEY `uacc_id` (`uacc_id`),
  ADD KEY `uacc_group_fk` (`uacc_group_fk`),
  ADD KEY `uacc_email` (`uacc_email`),
  ADD KEY `uacc_username` (`uacc_username`),
  ADD KEY `uacc_fail_login_ip_address` (`uacc_fail_login_ip_address`);

--
-- Index pour la table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`uadd_id`),
  ADD UNIQUE KEY `uadd_id` (`uadd_id`),
  ADD KEY `uadd_uacc_fk` (`uadd_uacc_fk`);

--
-- Index pour la table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`ugrp_id`),
  ADD UNIQUE KEY `ugrp_id` (`ugrp_id`) USING BTREE;

--
-- Index pour la table `user_login_sessions`
--
ALTER TABLE `user_login_sessions`
  ADD PRIMARY KEY (`usess_token`),
  ADD UNIQUE KEY `usess_token` (`usess_token`);

--
-- Index pour la table `user_privileges`
--
ALTER TABLE `user_privileges`
  ADD PRIMARY KEY (`upriv_id`),
  ADD UNIQUE KEY `upriv_id` (`upriv_id`) USING BTREE;

--
-- Index pour la table `user_privilege_groups`
--
ALTER TABLE `user_privilege_groups`
  ADD PRIMARY KEY (`upriv_groups_id`),
  ADD UNIQUE KEY `upriv_groups_id` (`upriv_groups_id`) USING BTREE,
  ADD KEY `upriv_groups_ugrp_fk` (`upriv_groups_ugrp_fk`),
  ADD KEY `upriv_groups_upriv_fk` (`upriv_groups_upriv_fk`);

--
-- Index pour la table `user_privilege_users`
--
ALTER TABLE `user_privilege_users`
  ADD PRIMARY KEY (`upriv_users_id`),
  ADD UNIQUE KEY `upriv_users_id` (`upriv_users_id`) USING BTREE,
  ADD KEY `upriv_users_uacc_fk` (`upriv_users_uacc_fk`),
  ADD KEY `upriv_users_upriv_fk` (`upriv_users_upriv_fk`);

--
-- Index pour la table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`upro_id`),
  ADD UNIQUE KEY `upro_id` (`upro_id`),
  ADD KEY `upro_uacc_fk` (`upro_uacc_fk`) USING BTREE;

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `anne_academique`
--
ALTER TABLE `anne_academique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `auditeurs`
--
ALTER TABLE `auditeurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=394;
--
-- AUTO_INCREMENT pour la table `niveau`
--
ALTER TABLE `niveau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `semestre`
--
ALTER TABLE `semestre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `specialite`
--
ALTER TABLE `specialite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `uacc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT pour la table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `uadd_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `ugrp_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `user_privileges`
--
ALTER TABLE `user_privileges`
  MODIFY `upriv_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `user_privilege_groups`
--
ALTER TABLE `user_privilege_groups`
  MODIFY `upriv_groups_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `user_privilege_users`
--
ALTER TABLE `user_privilege_users`
  MODIFY `upriv_users_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `upro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `evaluation`
--
ALTER TABLE `evaluation`
  ADD CONSTRAINT `evaluation_ibfk_1` FOREIGN KEY (`id_type_evaluation`) REFERENCES `type_evaluation` (`id`),
  ADD CONSTRAINT `evaluation_ibfk_2` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id`);

--
-- Contraintes pour la table `matiere`
--
ALTER TABLE `matiere`
  ADD CONSTRAINT `matiere_ibfk_1` FOREIGN KEY (`id_module`) REFERENCES `modules` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
