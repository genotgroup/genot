-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 08, 2016 at 03:18 
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `genot`
--

-- --------------------------------------------------------

--
-- Table structure for table `anne_academique`
--

CREATE TABLE `anne_academique` (
  `id` int(11) NOT NULL,
  `annee` varchar(255) DEFAULT NULL,
  `id_precedente` int(11) DEFAULT NULL,
  `date_creation` date NOT NULL,
  `en_cour` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `anne_academique`
--

INSERT INTO `anne_academique` (`id`, `annee`, `id_precedente`, `date_creation`, `en_cour`) VALUES
(1, '2016-2017', NULL, '2016-05-01', 1),
(2, '2015-2016', NULL, '2015-05-03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `auditeurs`
--

CREATE TABLE `auditeurs` (
  `id` int(11) NOT NULL,
  `matricule` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `lieu_naiss` varchar(255) DEFAULT NULL,
  `date_naiss` date DEFAULT NULL,
  `id_promotion` int(11) DEFAULT NULL,
  `id_classe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditeurs`
--

INSERT INTO `auditeurs` (`id`, `matricule`, `nom`, `prenom`, `email`, `lieu_naiss`, `date_naiss`, `id_promotion`, `id_classe`) VALUES
(525, '14EFP006', 'ABANDA MINKADA', 'JEAN JACQUES', '', 'MBALMAYO', '0000-00-00', 1, 1),
(526, '14EFP010', 'ABDOU', 'MAHAMA', '', 'MOZOGO', '0000-00-00', 1, 1),
(527, '14FPL02', 'ABONDO', 'MOISE ADONIS', '', 'SANGMELIMA', '0000-00-00', 1, 1),
(528, '14FPL09', 'AHMADOU ', 'TIDJANI MAYARAH', '', 'BANYO', '0000-00-00', 1, 1),
(529, '14CP006', 'AKEBANA', 'LUCIENNE', '', 'BETARE OYA', '0000-00-00', 1, 1),
(530, '14FPL24', 'AMBANI EBINI', 'ALAIN ROGER', '', 'YAOUNDE', '0000-00-00', 1, 1),
(531, '14EFP021', 'AMENLE EKOMANE', 'GERARD', '', 'YAOUNDE', '0000-00-00', 1, 1),
(532, '14EFP060', 'AMOUGOU BELINGA', 'DOMINIQUE', '', 'MVOUTESSI I', '0000-00-00', 1, 1),
(533, '14FPL28', 'ANGO NLOM', 'STEVE', '', 'EBOLOWA', '0000-00-00', 1, 1),
(534, '14EFP067', 'ANKA', 'CHRISTELLE', '', 'ANGUENGUE', '0000-00-00', 1, 1),
(535, '13GB004', 'ASSENE MAXIME SEVERIN', '-', '', 'BEKOUDOU', '0000-00-00', 1, 1),
(536, '14ECP040', 'ATANGANA BESSALA', 'NORBERT GASTIEN', '', 'YAOUNDE', '0000-00-00', 1, 1),
(537, '14CP023', 'ATANGANA ELOUNA', 'PIERRE  ', '', 'EVODOULA', '0000-00-00', 1, 1),
(538, '14EFP042', 'ATANGANA NGOA', 'THIERRY GABRIEL', '', 'YAOUNDE', '0000-00-00', 1, 1),
(539, '14EFP014', 'AWONO  ', 'FREDERIC', '', 'ELOM', '0000-00-00', 1, 1),
(540, '14FPL25', 'AYISSI AWONO', 'JEAN SIMEON', '', 'YAOUNDE', '0000-00-00', 1, 1),
(541, '14EFP024', 'BANGUE BANGKONBA', 'CHARLES', '', 'DOUALA', '0000-00-00', 1, 1),
(542, '14FPL10', 'BENE', 'HENOCK', '', 'NKONGSAMBA', '0000-00-00', 1, 1),
(543, '14CP001', 'BENGONO ESSINDI', 'RENE GRATIEN', '', 'YAOUNDE', '0000-00-00', 1, 1),
(544, '14CP021', 'BILOO MENGUE', 'GUY PATRICK', '', 'ZINGUI', '0000-00-00', 1, 1),
(545, '14EFP073', 'BLAOURA MAHAMOUDOU', 'BLAISE', '', 'MAGA', '0000-00-00', 1, 1),
(546, '14CP010', 'BODO', 'RAYMOND', '', 'YAOUNDE', '0000-00-00', 1, 1),
(547, '14CP011', 'BOMO', 'THIERRY LANDRY', '', 'NDEN', '0000-00-00', 1, 1),
(548, '14EFP031', 'BOURMEKE ', 'GHISLAIN NOEL', '', 'YAOUNDE', '0000-00-00', 1, 1),
(549, '14EFP043', 'CHECO NOUBISSI', 'YOLANDE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(550, '14CP007', 'CHEGUE ', 'SYLVESTRE', '', 'NKONGSAMBA', '0000-00-00', 1, 1),
(551, '14FPL26', 'DEFO FOUEKA', 'FRANCIS GALLIOS', '', 'BADJOUN', '0000-00-00', 1, 1),
(552, '14EFP025', 'DJOMO ', 'LYNA CHRISTELLE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(553, '14EFP003', 'DJONA ', 'ALPHONSE GUIDANG', '', 'VIRI', '0000-00-00', 1, 1),
(554, '14ECP045', 'DJOUALE TSAFACK', 'ALAIN GERALDIN', '', 'YAOUNDE', '0000-00-00', 1, 1),
(555, '14CP017', 'DOCKO DOCKO', 'EMILE LAMBERT', '', 'NKONGSAMBA', '0000-00-00', 1, 1),
(556, '14CP016', 'DOGUIMI', 'FRANCOIS', '', 'GAROUA', '0000-00-00', 1, 1),
(557, '14EFP044', 'DOKO EDJIANE', 'MATHIEU ALAIN', '', 'NKOLO EBOO', '0000-00-00', 1, 1),
(558, '14EFP040', 'EBOGO', 'LEONCE CLEMENT', '', 'YAOUNDE', '0000-00-00', 1, 1),
(559, '14EFP005', 'EBOLO', 'GEORGE ERIC', '', 'AYOS', '0000-00-00', 1, 1),
(560, '14EFP045', 'EBONGO', 'ARISBER TRESOR', '', 'DOUALA', '0000-00-00', 1, 1),
(561, '14CP019', 'ELIMBI NDOUMBE', 'EMMANUEL', '', 'DOUALA', '0000-00-00', 1, 1),
(562, '14EFP036', 'ESSENGUE ESSENGUE', 'VITUS', '', 'MBAKOMO', '0000-00-00', 1, 1),
(563, '14EFP046', 'ESSOUMA ', 'SALOME DIANE LAURE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(564, '14EFP032', 'EVA MEYO', '-', '', 'KRIBI', '0000-00-00', 1, 1),
(565, '14EFP001', 'EVA MIMBE', 'MARTIN GUY', '', 'YAOUNDE', '0000-00-00', 1, 1),
(566, '14ECP043', 'EYELOKOBA ENDAMEYO', 'EMILE RENE', '', 'BATOURI', '0000-00-00', 1, 1),
(567, '14FPL11', 'EYENGA AZOMBO', 'ESTELLE EPSE ATANGANA', '', 'METET', '0000-00-00', 1, 1),
(568, '14EFP047', 'EYENGA NDJOMO', 'ELISEE AMOUR', '', 'SANGMELIMA', '0000-00-00', 1, 1),
(569, '14FPL23', 'EYENGA ONAMBELE', 'BARTHELEMY', '', 'YAOUNDE', '0000-00-00', 1, 1),
(570, '14CP013', 'EYIKE BAENLA', 'EMILE  ', '', 'EDEA', '0000-00-00', 1, 1),
(571, '14EFP048', 'FOSSO THUANKAM', 'ROSE CLAIRE ARMELLE MARTINE', '', 'MBO', '0000-00-00', 1, 1),
(572, '14CP020', 'FOUDA YANA', 'BASILE PATRICK', '', 'YAOUNDE', '0000-00-00', 1, 1),
(573, '14CP014', 'HYGINUS BINYUYVIDZEM', '-', '', 'KUMBO', '0000-00-00', 1, 1),
(574, '14FPL27', 'JOMBWEDE EMBOLA ', 'VICTOR EMMANUEL 2', '', 'DOUALA', '0000-00-00', 1, 1),
(575, '14EFP063', 'KALDA TCHETCHOULAI', 'DAVID', '', 'MOKOLO', '0000-00-00', 1, 1),
(576, '14EFP034', 'KAMKOUM YOYA', 'BETHEL', '', 'NDOUNGUE', '0000-00-00', 1, 1),
(577, '14FPL12', 'KAMTE POUENE', 'GUY BERTRAND', '', 'NKONGSAMBA', '0000-00-00', 1, 1),
(578, '14EFP065', 'KETCHA NANA', 'ODILE ', '', 'NDOUNGUE', '0000-00-00', 1, 1),
(579, '14CP08', 'KINGUE', 'ERIC MARC VICTOR', '', 'YAOUNDE', '0000-00-00', 1, 1),
(580, '14FPL22', 'KOUAMOU WANDJI', 'CEDRIC', '', 'YAOUNDE', '0000-00-00', 1, 1),
(581, '14FPL01', 'KOUNDI', 'PATRICK SERGE', '', 'MESSAMENA', '0000-00-00', 1, 1),
(582, '14CP022', 'KOUNGANG ', 'FRANCIS CLAUDE', '', 'DOUALA', '0000-00-00', 1, 1),
(583, '14EFP004', 'LOABALBE ', 'FLAVIEN', '', 'EBOLOWA', '0000-00-00', 1, 1),
(584, '13CP018', 'LONO', 'MIASSE', '', 'DOUME', '0000-00-00', 1, 1),
(585, '14FPL21', 'LOTY', 'PIERRE JEAN DANIEL', '', 'YAOUNDE', '0000-00-00', 1, 1),
(586, '14CP018', 'LOUIS ZACHEE', 'MBOCK', '', 'AKOM II', '0000-00-00', 1, 1),
(587, '14CP002', 'MANGA', 'MARC', '', 'AKONOLINGA', '0000-00-00', 1, 1),
(588, '14CP026', 'MANGELE ', 'SEBASTIEN THEOPHILE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(589, '14EFP049', 'MANTSANA MISS', 'HERMINE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(590, '14FPL13', 'MATEO SESE NCHAMA', '-', '', 'BATA', '0000-00-00', 1, 1),
(591, '14EFP016', 'MBALA MBIE', 'MARCELIN', '', 'NKONGNTSAM', '0000-00-00', 1, 1),
(592, '14EFP050', 'MBARGA ', 'IGNACE BLAISE', '', 'AKOM', '0000-00-00', 1, 1),
(593, '14FPL19', 'MBELLE', 'ELISABETH YOLLANDE', '', 'NGOME DZAP', '0000-00-00', 1, 1),
(594, '14EFP002', 'MBOH', 'PATRICE LUMUMBA', '', 'BAMENDA', '0000-00-00', 1, 1),
(595, '14EFP072', 'MBOUMO NJOYA', 'SOULEMANOU', '', 'NJISSE', '0000-00-00', 1, 1),
(596, '14EFP051', 'MEKONGO ONDOUA', 'ADALBERT SERAPHINS', '', 'YAOUNDE', '0000-00-00', 1, 1),
(597, '14EFP033', 'MELINGUI EPSE ESSOMBA', 'GERMAINE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(598, '14ECP042', 'MENDANA BALLA', 'FRANCK ACHILLE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(599, '14FPL04', 'MERSUH', 'LAETITIA', '', 'BAMENDA', '0000-00-00', 1, 1),
(600, '14EFP052', 'MFONO MBA', 'ANNE MARIE', '', 'OLAM ZE', '0000-00-00', 1, 1),
(601, '14ECP044', 'MFOUAPON NJUAMSHETKY', 'HENOCK', '', 'FOUBAN', '0000-00-00', 1, 1),
(602, '14CP029', 'MOUKAM MENDJIADEU', 'M.MAKEBA', '', 'KEMKEM', '0000-00-00', 1, 1),
(603, '14CP003', 'MOUSSA SALI', '-', '', 'DIMEO', '0000-00-00', 1, 1),
(604, '14EFP022', 'MOZOGO', 'ANDRE', '', 'MOKOLO', '0000-00-00', 1, 1),
(605, '14EFP053', 'MPABE FADIMATOU', 'MIREILLE', '', 'MOKOLO', '0000-00-00', 1, 1),
(606, '14CP034', 'MVELE', 'TLESPHORE IRENEE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(607, '14ECP041', 'MVO MVO', 'FRANCOIS', '', 'ZOETELE', '0000-00-00', 1, 1),
(608, '14EFP026', 'MVONDO EYA', 'SIMON PIERRE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(609, '14FPL16', 'MVONDO JULES RODRIGUE', '-', '', 'DJOUM', '0000-00-00', 1, 1),
(610, '14EFP070', 'NANA FADIMATOU', 'NOUHOU ADAMOU', '', 'MAROUA', '0000-00-00', 1, 1),
(611, '14FPL17', 'NDANA MEKOGO', 'RAYMOND', '', 'ABONG MBANG', '0000-00-00', 1, 1),
(612, '14EFP015', 'NDONGO MOLLO', 'MICHEL', '', 'NKOM 1', '0000-00-00', 1, 1),
(613, '14EFP054', 'NGAH ASSENE', 'HONORINE NADINE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(614, '14FPL05', 'NGAH RACHEL', '-', '', 'YAOUNDE', '0000-00-00', 1, 1),
(615, '14EFP055', 'NGAVANGA ', 'NICAISE MAGLOIRE', '', 'YAOUNDE DJOUNGOLO', '0000-00-00', 1, 1),
(616, '14CP004', 'NGO BILLONG', 'CARINE', '', 'SANGMELAMA', '0000-00-00', 1, 1),
(617, '14EFP018', 'NGO MISSAM-HAN', 'MARGUERITE CHANTAL', '', 'YAOUNDE', '0000-00-00', 1, 1),
(618, '14EFP009', 'NGO O DJOB ', 'DESIRE', '', 'EDEA', '0000-00-00', 1, 1),
(619, '14EFP056', 'NJI ', 'PASCAL NDIKUM', '', 'MBATU', '0000-00-00', 1, 1),
(620, '14EFP057', 'NKAZIE MOUCHIPOU', 'MOHAMED DESIRE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(621, '14EFP064', 'NKE', 'CHRISTINE VIVIANE', '', 'MBANKOMO', '0000-00-00', 1, 1),
(622, '14FPL18', 'NKENE ', 'ESTHER NICOLE', '', 'ESEKA', '0000-00-00', 1, 1),
(623, '14EFP028', 'NKENG MULUH', '-', '', 'BANGWA', '0000-00-00', 1, 1),
(624, '14EFP058', 'NKOA ESSOMBA', 'ELISEE', '', 'SANGMELIMA', '0000-00-00', 1, 1),
(625, '14EFP011', 'NKOLO EBENE', 'CREPAIN', '', 'OLEKOTSING', '0000-00-00', 1, 1),
(626, '14EFP059', 'NKOUMBA ESSEBE', 'JEAN DIDIER', '', 'FANG-BILOUN', '0000-00-00', 1, 1),
(627, '14EFP037', 'NNA EDIMENGO', 'JOE MARIUS', '', 'ENONGAL EBOLOWA', '0000-00-00', 1, 1),
(628, '14EFP038', 'NYNYING', 'MOSES NYINCHIA', '', 'OKU', '0000-00-00', 1, 1),
(629, '14CP028', 'NZHIE', 'ARSENE SIDOINE', '', 'KRIBI', '0000-00-00', 1, 1),
(630, '14FPL06', 'OMGBA AMOUGOU', 'JEAN', '', 'MANENGOTENG', '0000-00-00', 1, 1),
(631, '14FPL20', 'ONDOBOE EPSE ESSOMO EDOUA', 'GENEVIEVE LOUISETTE', '', 'NKOMO 1', '0000-00-00', 1, 1),
(632, '14FPL14', 'ONGTOUON', 'MELANIE CELINE', '', 'NDIKINEMEKI', '0000-00-00', 1, 1),
(633, '14FPL15', 'OTIA', 'FERDINAND BOJA', '', 'EBOLOWA', '0000-00-00', 1, 1),
(634, '14EFP019', 'OUSMANOU NASSOUROU', '-', '', 'MAROUA', '0000-00-00', 1, 1),
(635, '14EFP039', 'PETER GIYO', 'YERIMA', '', 'NWA', '0000-00-00', 1, 1),
(636, '14CP024', 'REGINA AGBEKOME', 'MBU', '', 'NGUTI', '0000-00-00', 1, 1),
(637, '14FPL07', 'SOULEYMAN', '-', '', 'MOULVOUDAYE', '0000-00-00', 1, 1),
(638, '14FPL08', 'TADJUIDJE GUIASSU', 'ISAAC', '', 'BAYAM', '0000-00-00', 1, 1),
(639, '14EFP061', 'TAMBE SANGAYA EYONG', '-', '', 'YAOUNDE', '0000-00-00', 1, 1),
(640, '14CP025', 'TANANG TCHOUALA', 'PATRICE', '', 'ENONGAL', '0000-00-00', 1, 1),
(641, '14EFP035', 'TCHIDJO SOBGUE', 'PIERRE ERIC', '', 'YAOUNDE', '0000-00-00', 1, 1),
(642, '14EFP013', 'TCHIENGANG DJUEKOU', 'ADRIEN EMERIC', '', 'GAROUA', '0000-00-00', 1, 1),
(643, '14FPL03', 'TIAMBOU', 'NATHALIE CLAIRE', '', 'BAFOUSSAM', '0000-00-00', 1, 1),
(644, '14EFP041', 'TITA SHIPUH ', 'VICTOR MBANYA', '', 'BABA 1', '0000-00-00', 1, 1),
(645, '14CP008', 'TOH MAH', 'BRIDGET', '', 'BENGWI', '0000-00-00', 1, 1),
(646, '14EFP023', 'TOURE SOUMAILA ', 'EL MADANE', '', 'GOUNDAM-MALI', '0000-00-00', 1, 1),
(647, '14CP015', 'TSOUNGUI', 'JEAN CLAUDE', '', 'YAOUNDE', '0000-00-00', 1, 1),
(648, '14EFP062', 'WAMBA BERTOING', 'JEAN CLAUDE', '', 'KAELE', '0000-00-00', 1, 1),
(649, '14EFP029', 'WANGSO TCHOBWE', 'DIEUDONNE  ', '', 'DOUKOULA', '0000-00-00', 1, 1),
(650, '14EFP020', 'WILLIAM', 'AROUNG', '', 'DIAMARE', '0000-00-00', 1, 1),
(651, '14CP005', 'YAOUBA', 'SAYOU', '', 'GUIDIGUYS', '0000-00-00', 1, 1),
(652, '14EFP017', 'YAYA', 'ADJI', '', 'MAROUA', '0000-00-00', 1, 1),
(653, '14EFP012', 'ZE ', 'BERTIN', '', 'FANG-BIKANG 2', '0000-00-00', 1, 1),
(654, '14EFP071', 'ZEH ZANGA ', 'ALBERT STEVE', '', 'EDOM', '0000-00-00', 0, 0),
(655, '14EFP008', 'ZOUA', 'ALBERT', '', 'MALANEGOME', '0000-00-00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('49c145c475a954ad2bb2cee259d8eead', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:46.0) Gecko/20100101 Firefox/46.0', 1465391758, 'a:9:{s:9:"user_data";s:0:"";s:10:"flexi_auth";a:7:{s:15:"user_identifier";s:15:"admin@admin.com";s:7:"user_id";s:2:"34";s:5:"admin";b:1;s:5:"group";a:1:{i:3;s:12:"Master Admin";}s:10:"privileges";a:10:{i:1;s:10:"View Users";i:3;s:15:"View Privileges";i:4;s:18:"Insert User Groups";i:5;s:17:"Insert Privileges";i:6;s:12:"Update Users";i:7;s:18:"Update User Groups";i:8;s:17:"Update Privileges";i:9;s:12:"Delete Users";i:10;s:18:"Delete User Groups";i:11;s:17:"Delete Privileges";}s:22:"logged_in_via_password";b:1;s:19:"login_session_token";s:40:"0ca3d6a6392b31f11269c73398984f922116e7f7";}s:18:"id_anne_academique";s:1:"1";s:15:"anne_academique";s:9:"2016-2017";s:26:"anne_academique_precedente";N;s:11:"classe_note";O:8:"stdClass":6:{s:2:"id";s:1:"1";s:3:"nom";s:21:"Master 1 Tronc Commun";s:4:"code";s:8:"M1TC2016";s:12:"id_annee_aca";s:1:"1";s:13:"id_specialite";s:1:"4";s:9:"id_niveau";s:1:"1";}s:13:"semestre_note";O:8:"stdClass":4:{s:2:"id";s:1:"5";s:3:"nom";s:13:"TODO Semestre";s:4:"code";s:4:"Sem1";s:9:"id_classe";s:1:"1";}s:8:"semestre";O:8:"stdClass":4:{s:2:"id";s:1:"5";s:3:"nom";s:13:"TODO Semestre";s:4:"code";s:4:"Sem1";s:9:"id_classe";s:1:"1";}s:12:"matiere_note";O:8:"stdClass":4:{s:2:"id";s:1:"3";s:4:"code";s:3:"TFP";s:3:"nom";s:30:"Théorie de Finances Publiques";s:9:"id_module";s:1:"1";}}'),
('e2319c60340223108e212f503b04f945', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:46.0) Gecko/20100101 Firefox/46.0', 1465385385, 'a:8:{s:9:"user_data";s:0:"";s:10:"flexi_auth";a:7:{s:15:"user_identifier";s:15:"admin@admin.com";s:7:"user_id";s:2:"34";s:5:"admin";b:1;s:5:"group";a:1:{i:3;s:12:"Master Admin";}s:10:"privileges";a:10:{i:1;s:10:"View Users";i:3;s:15:"View Privileges";i:4;s:18:"Insert User Groups";i:5;s:17:"Insert Privileges";i:6;s:12:"Update Users";i:7;s:18:"Update User Groups";i:8;s:17:"Update Privileges";i:9;s:12:"Delete Users";i:10;s:18:"Delete User Groups";i:11;s:17:"Delete Privileges";}s:22:"logged_in_via_password";b:1;s:19:"login_session_token";s:40:"529ef3fbd632981422269849ac4dc82660b1e3e3";}s:18:"id_anne_academique";s:1:"1";s:15:"anne_academique";s:9:"2016-2017";s:26:"anne_academique_precedente";N;s:11:"classe_note";O:8:"stdClass":6:{s:2:"id";s:1:"1";s:3:"nom";s:21:"Master 1 Tronc Commun";s:4:"code";s:8:"M1TC2016";s:12:"id_annee_aca";s:1:"1";s:13:"id_specialite";s:1:"4";s:9:"id_niveau";s:1:"1";}s:13:"semestre_note";O:8:"stdClass":4:{s:2:"id";s:1:"5";s:3:"nom";s:13:"TODO Semestre";s:4:"code";s:4:"Sem1";s:9:"id_classe";s:1:"1";}s:12:"matiere_note";O:8:"stdClass":4:{s:2:"id";s:1:"3";s:4:"code";s:3:"TFP";s:3:"nom";s:30:"Théorie de Finances Publiques";s:9:"id_module";s:1:"1";}}');

-- --------------------------------------------------------

--
-- Table structure for table `classe`
--

CREATE TABLE `classe` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `id_annee_aca` int(11) NOT NULL,
  `id_specialite` int(11) NOT NULL,
  `id_niveau` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classe`
--

INSERT INTO `classe` (`id`, `nom`, `code`, `id_annee_aca`, `id_specialite`, `id_niveau`) VALUES
(1, 'Master 1 Tronc Commun', 'M1TC2016', 1, 4, 1),
(2, 'Master 2 CP 2016', 'M2CP2016', 2, 2, 2),
(3, 'Master 2 FPL 2016', 'M2FPL2016', 2, 3, 2),
(4, 'Master 2 GB 2016', 'M2GB2016', 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `evaluation`
--

CREATE TABLE `evaluation` (
  `id` int(11) NOT NULL,
  `pourcentage` float DEFAULT NULL,
  `id_type_evaluation` int(11) DEFAULT NULL,
  `id_matiere` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_Sem1`
--

CREATE TABLE `evaluation_Sem1` (
  `id` int(11) NOT NULL,
  `pourcentage` float DEFAULT NULL,
  `id_type_evaluation` int(11) DEFAULT NULL,
  `id_matiere` int(11) DEFAULT NULL,
  `date_eval` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluation_Sem1`
--

INSERT INTO `evaluation_Sem1` (`id`, `pourcentage`, `id_type_evaluation`, `id_matiere`, `date_eval`) VALUES
(1, 30, 1, 2, '2016-05-26'),
(2, 70, 2, 2, '2016-05-31'),
(3, 30, 1, 3, '2016-06-13'),
(4, 70, 2, 3, '2016-06-30');

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_Sem3GB`
--

CREATE TABLE `evaluation_Sem3GB` (
  `id` int(11) NOT NULL,
  `date_eval` date NOT NULL,
  `pourcentage` float DEFAULT NULL,
  `id_type_evaluation` int(11) DEFAULT NULL,
  `id_matiere` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluation_Sem3GB`
--

INSERT INTO `evaluation_Sem3GB` (`id`, `date_eval`, `pourcentage`, `id_type_evaluation`, `id_matiere`) VALUES
(1, '2016-05-01', 30, 1, 1),
(2, '2016-05-01', 30, 1, 2),
(3, '2016-05-23', 70, 2, 1),
(4, '2016-05-23', 70, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `matiere`
--

CREATE TABLE `matiere` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_module` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `matiere_Sem1`
--

CREATE TABLE `matiere_Sem1` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_module` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `matiere_Sem1`
--

INSERT INTO `matiere_Sem1` (`id`, `code`, `nom`, `id_module`) VALUES
(1, 'TFP', 'Finances publiques locales et fiscalité des CTD', 2),
(2, 'BPU', 'Budget Public', 1),
(3, 'TFP', 'Théorie de Finances Publiques', 1);

-- --------------------------------------------------------

--
-- Table structure for table `matiere_Sem3GB`
--

CREATE TABLE `matiere_Sem3GB` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_module` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `matiere_Sem3GB`
--

INSERT INTO `matiere_Sem3GB` (`id`, `code`, `nom`, `id_module`) VALUES
(1, 'CAC48S2', 'COMPTABILITÉ D’ANALYSE DES COÛTS', 1),
(2, 'EBP42S3', 'ELABORATION DES BUDGETS PUBLICS ET PREVISIONS ECONOMIQUES', 2);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_semestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `modules_Sem1`
--

CREATE TABLE `modules_Sem1` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_semestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_Sem1`
--

INSERT INTO `modules_Sem1` (`id`, `code`, `nom`, `id_semestre`) VALUES
(1, 'TFP', 'Théorie des finances publiques', NULL),
(2, 'TFP', 'Finances publiques locales et fiscalité des CTD', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `modules_Sem3GB`
--

CREATE TABLE `modules_Sem3GB` (
  `id` int(11) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_semestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules_Sem3GB`
--

INSERT INTO `modules_Sem3GB` (`id`, `code`, `nom`, `id_semestre`) VALUES
(1, 'CAC', 'COMPTABILITÉ D’ANALYSE DES COÛTS', NULL),
(2, 'EBP', 'ELABORATION DES BUDGETS PUBLICS ET PREVISIONS ECONOMIQUES', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `moyenne`
--

CREATE TABLE `moyenne` (
  `id` int(11) NOT NULL,
  `moyenne` float DEFAULT NULL,
  `id_auditeur` int(11) DEFAULT NULL,
  `id_module` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `moyenne_matiere_Sem1`
--

CREATE TABLE `moyenne_matiere_Sem1` (
  `id` int(11) NOT NULL,
  `moyenne` float DEFAULT NULL,
  `id_auditeur` int(11) DEFAULT NULL,
  `id_matiere` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `moyenne_matiere_Sem1`
--

INSERT INTO `moyenne_matiere_Sem1` (`id`, `moyenne`, `id_auditeur`, `id_matiere`) VALUES
(1, 13.05, 525, 2),
(2, 12.5, 526, 2),
(3, 11.95, 527, 2),
(4, 12.8, 528, 2),
(5, 13.65, 529, 2),
(6, 12.4, 530, 2),
(7, 12.55, 531, 2),
(8, 11.65, 532, 2),
(9, 12.15, 533, 2),
(10, 11.95, 534, 2),
(11, 4.05, 535, 2),
(12, 4.2, 536, 2),
(13, 11.35, 537, 2),
(14, 4.5, 538, 2),
(15, 4.65, 539, 2),
(16, 4.8, 540, 2),
(17, 4.95, 541, 2),
(18, 2.55, 542, 2),
(19, 2.7, 543, 2),
(20, 2.85, 544, 2),
(21, 3, 545, 2),
(22, 3.15, 546, 2),
(23, 3.3, 547, 2),
(24, 12.55, 548, 2),
(25, 13.4, 549, 2),
(26, 11.45, 550, 2),
(27, 13.7, 551, 2),
(28, 15.25, 552, 2),
(29, 16.1, 553, 2),
(30, 12.75, 554, 2),
(31, 12.2, 555, 2),
(32, 4.65, 556, 2),
(33, 4.8, 557, 2),
(34, 4.95, 558, 2),
(35, 2.55, 559, 2),
(36, 2.7, 560, 2),
(37, 2.85, 561, 2),
(38, 3, 562, 2),
(39, 3.15, 563, 2),
(40, 3.3, 564, 2),
(41, 3.45, 565, 2),
(42, 3.6, 566, 2),
(43, 3.75, 567, 2),
(44, 3.9, 568, 2),
(45, 4.05, 569, 2),
(46, 4.2, 570, 2),
(47, 4.35, 571, 2),
(48, 4.5, 572, 2),
(49, 4.65, 573, 2),
(50, 4.8, 574, 2),
(51, 4.95, 575, 2),
(52, 2.55, 576, 2),
(53, 2.7, 577, 2),
(54, 2.85, 578, 2),
(55, 3, 579, 2),
(56, 3.15, 580, 2),
(57, 3.3, 581, 2),
(58, 3.45, 582, 2),
(59, 3.6, 583, 2),
(60, 3.75, 584, 2),
(61, 3.9, 585, 2),
(62, 4.05, 586, 2),
(63, 4.2, 587, 2),
(64, 4.35, 588, 2),
(65, 4.5, 589, 2),
(66, 4.65, 590, 2),
(67, 4.8, 591, 2),
(68, 4.95, 592, 2),
(69, 2.55, 593, 2),
(70, 2.7, 594, 2),
(71, 2.85, 595, 2),
(72, 3, 596, 2),
(73, 3.15, 597, 2),
(74, 3.3, 598, 2),
(75, 3.45, 599, 2),
(76, 3.6, 600, 2),
(77, 3.75, 601, 2),
(78, 3.9, 602, 2),
(79, 4.05, 603, 2),
(80, 4.2, 604, 2),
(81, 4.35, 605, 2),
(82, 4.5, 606, 2),
(83, 4.65, 607, 2),
(84, 4.8, 608, 2),
(85, 4.95, 609, 2),
(86, 2.55, 610, 2),
(87, 2.7, 611, 2),
(88, 2.85, 612, 2),
(89, 3, 613, 2),
(90, 3.15, 614, 2),
(91, 3.3, 615, 2),
(92, 3.45, 616, 2),
(93, 3.6, 617, 2),
(94, 3.75, 618, 2),
(95, 3.9, 619, 2),
(96, 4.05, 620, 2),
(97, 4.2, 621, 2),
(98, 4.35, 622, 2),
(99, 4.5, 623, 2),
(100, 4.65, 624, 2),
(101, 4.8, 625, 2),
(102, 4.95, 626, 2),
(103, 2.55, 627, 2),
(104, 2.7, 628, 2),
(105, 2.85, 629, 2),
(106, 3, 630, 2),
(107, 3.15, 631, 2),
(108, 3.3, 632, 2),
(109, 3.45, 633, 2),
(110, 3.6, 634, 2),
(111, 3.75, 635, 2),
(112, 3.9, 636, 2),
(113, 4.05, 637, 2),
(114, 4.2, 638, 2),
(115, 4.35, 639, 2),
(116, 4.5, 640, 2),
(117, 4.65, 641, 2),
(118, 4.8, 642, 2),
(119, 4.95, 643, 2),
(120, 2.55, 644, 2),
(121, 2.7, 645, 2),
(122, 2.85, 646, 2),
(123, 3, 647, 2),
(124, 3.15, 648, 2),
(125, 3.3, 649, 2),
(126, 3.45, 650, 2),
(127, 3.6, 651, 2),
(128, 3.75, 652, 2),
(129, 3.9, 653, 2),
(155, 12.8, 525, 3),
(156, 13.925, 526, 3),
(157, 12.4, 527, 3),
(158, 13.6, 528, 3),
(159, 12.05, 529, 3),
(160, 13, 530, 3),
(161, 16.6, 531, 3),
(162, 13.6, 532, 3),
(163, 14.1, 533, 3),
(164, 13, 534, 3),
(165, 13.5, 535, 3),
(166, 13.5, 536, 3),
(167, 12, 537, 3),
(168, 12.4, 538, 3),
(169, 12.2, 539, 3),
(170, 12.575, 540, 3),
(171, 13, 541, 3),
(172, 16.6, 542, 3),
(173, 14.3, 543, 3),
(174, 12.7, 544, 3),
(175, 15.1, 545, 3),
(176, 13.15, 546, 3),
(177, 13.8, 547, 3),
(178, 12.55, 548, 3),
(179, 13.05, 549, 3),
(180, 14.25, 550, 3),
(181, 12.3, 551, 3),
(182, 13.15, 552, 3),
(183, 14, 553, 3),
(184, 14.675, 554, 3),
(185, 13.6, 555, 3),
(186, 15.85, 556, 3),
(187, 14.6, 557, 3),
(188, 12.1, 558, 3),
(189, 14.8, 559, 3),
(190, 12.4, 560, 3),
(191, 12.9, 561, 3),
(192, 11.35, 562, 3),
(193, 13.35, 563, 3),
(194, 15.9, 564, 3),
(195, 12.9, 565, 3),
(196, 12.7, 566, 3),
(197, 13.7, 567, 3),
(198, 14.375, 568, 3),
(199, 13.15, 569, 3),
(200, 15.4, 570, 3),
(201, 14.15, 571, 3),
(202, 13.6, 572, 3),
(203, 15.85, 573, 3),
(204, 13.9, 574, 3),
(205, 15.45, 575, 3),
(206, 12.1, 576, 3),
(207, 13.05, 577, 3),
(208, 13.8, 578, 3),
(209, 10.8, 579, 3),
(210, 11.35, 580, 3),
(211, 13.7, 581, 3),
(212, 15.725, 582, 3),
(213, 13.6, 583, 3),
(214, 14.8, 584, 3),
(215, 13.7, 585, 3),
(216, 13.15, 586, 3),
(217, 15.4, 587, 3),
(218, 13.45, 588, 3),
(219, 15, 589, 3),
(220, 13.75, 590, 3),
(221, 14.25, 591, 3),
(222, 15.45, 592, 3),
(223, 10.95, 593, 3),
(224, 11.8, 594, 3),
(225, 12.65, 595, 3),
(226, 13.325, 596, 3),
(227, 12.25, 597, 3),
(228, 14.5, 598, 3),
(229, 13.25, 599, 3),
(230, 12.7, 600, 3),
(231, 14.95, 601, 3),
(232, 12.1, 602, 3),
(233, 14.1, 603, 3),
(234, 12.4, 604, 3),
(235, 11.85, 605, 3),
(236, 12.75, 606, 3),
(237, 12.3, 607, 3),
(238, 14.5, 608, 3),
(239, 14.3, 609, 3),
(240, 13.925, 610, 3),
(241, 13, 611, 3),
(242, 15.25, 612, 3),
(243, 12.8, 613, 3),
(244, 12.25, 614, 3),
(245, 14.5, 615, 3),
(246, 12.55, 616, 3),
(247, 13.5, 617, 3),
(248, 12.7, 618, 3),
(249, 12.75, 619, 3),
(250, 12.9, 620, 3),
(251, 10.65, 621, 3),
(252, 13, 622, 3),
(253, 15.2, 623, 3),
(254, 14.825, 624, 3),
(255, 12.7, 625, 3),
(256, 15.1, 626, 3),
(257, 13.85, 627, 3),
(258, 11.8, 628, 3),
(259, 14.05, 629, 3),
(260, 12.1, 630, 3),
(261, 13.65, 631, 3),
(262, 12.4, 632, 3),
(263, 12.9, 633, 3),
(264, 14.1, 634, 3),
(265, 12.15, 635, 3),
(266, 13, 636, 3),
(267, 13.5, 637, 3),
(268, 14, 638, 3),
(269, 10, 639, 3),
(270, 12, 640, 3),
(271, 13.1, 641, 3),
(272, 12.725, 642, 3),
(273, 11.35, 643, 3),
(274, 15.1, 644, 3),
(275, 15.2, 645, 3),
(276, 13.6, 646, 3),
(277, 14.8, 647, 3),
(278, 13, 648, 3),
(279, 14.55, 649, 3),
(280, 12.55, 650, 3),
(281, 13.05, 651, 3),
(282, 14.25, 652, 3),
(283, 12.3, 653, 3);

-- --------------------------------------------------------

--
-- Table structure for table `moyenne_Sem1`
--

CREATE TABLE `moyenne_Sem1` (
  `id` int(11) NOT NULL,
  `moyenne` float DEFAULT NULL,
  `id_auditeur` int(11) DEFAULT NULL,
  `id_module` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `moyenne_Sem1`
--

INSERT INTO `moyenne_Sem1` (`id`, `moyenne`, `id_auditeur`, `id_module`) VALUES
(517, 15.6, 525, 1),
(518, 15.2, 526, 1),
(519, 5.7, 527, 1),
(520, 6, 528, 1),
(521, 6.3, 529, 1),
(522, 6.6, 530, 1),
(523, 6.9, 531, 1),
(524, 7.2, 532, 1),
(525, 7.5, 533, 1),
(526, 7.8, 534, 1),
(527, 8.1, 535, 1),
(528, 8.4, 536, 1),
(529, 8.7, 537, 1),
(530, 9, 538, 1),
(531, 9.3, 539, 1),
(532, 9.6, 540, 1),
(533, 9.9, 541, 1),
(534, 5.1, 542, 1),
(535, 5.4, 543, 1),
(536, 5.7, 544, 1),
(537, 6, 545, 1),
(538, 6.3, 546, 1),
(539, 6.6, 547, 1),
(540, 6.9, 548, 1),
(541, 7.2, 549, 1),
(542, 0, 550, 1),
(543, 0, 551, 1),
(544, 0, 552, 1),
(545, 0, 553, 1),
(546, 0, 554, 1),
(547, 0, 555, 1),
(548, 0, 556, 1),
(549, 0, 557, 1),
(550, 0, 558, 1),
(551, 0, 559, 1),
(552, 0, 560, 1),
(553, 0, 561, 1),
(554, 0, 562, 1),
(555, 0, 563, 1),
(556, 0, 564, 1),
(557, 0, 565, 1),
(558, 0, 566, 1),
(559, 0, 567, 1),
(560, 0, 568, 1),
(561, 0, 569, 1),
(562, 0, 570, 1),
(563, 0, 571, 1),
(564, 0, 572, 1),
(565, 0, 573, 1),
(566, 0, 574, 1),
(567, 0, 575, 1),
(568, 0, 576, 1),
(569, 0, 577, 1),
(570, 0, 578, 1),
(571, 0, 579, 1),
(572, 0, 580, 1),
(573, 0, 581, 1),
(574, 0, 582, 1),
(575, 0, 583, 1),
(576, 0, 584, 1),
(577, 0, 585, 1),
(578, 0, 586, 1),
(579, 0, 587, 1),
(580, 0, 588, 1),
(581, 0, 589, 1),
(582, 0, 590, 1),
(583, 0, 591, 1),
(584, 0, 592, 1),
(585, 0, 593, 1),
(586, 0, 594, 1),
(587, 0, 595, 1),
(588, 0, 596, 1),
(589, 0, 597, 1),
(590, 0, 598, 1),
(591, 0, 599, 1),
(592, 0, 600, 1),
(593, 0, 601, 1),
(594, 0, 602, 1),
(595, 0, 603, 1),
(596, 0, 604, 1),
(597, 0, 605, 1),
(598, 0, 606, 1),
(599, 0, 607, 1),
(600, 0, 608, 1),
(601, 0, 609, 1),
(602, 0, 610, 1),
(603, 0, 611, 1),
(604, 0, 612, 1),
(605, 0, 613, 1),
(606, 0, 614, 1),
(607, 0, 615, 1),
(608, 0, 616, 1),
(609, 0, 617, 1),
(610, 0, 618, 1),
(611, 0, 619, 1),
(612, 0, 620, 1),
(613, 0, 621, 1),
(614, 0, 622, 1),
(615, 0, 623, 1),
(616, 0, 624, 1),
(617, 0, 625, 1),
(618, 0, 626, 1),
(619, 0, 627, 1),
(620, 0, 628, 1),
(621, 0, 629, 1),
(622, 0, 630, 1),
(623, 0, 631, 1),
(624, 0, 632, 1),
(625, 0, 633, 1),
(626, 0, 634, 1),
(627, 0, 635, 1),
(628, 0, 636, 1),
(629, 0, 637, 1),
(630, 0, 638, 1),
(631, 0, 639, 1),
(632, 0, 640, 1),
(633, 0, 641, 1),
(634, 0, 642, 1),
(635, 0, 643, 1),
(636, 0, 644, 1),
(637, 0, 645, 1),
(638, 0, 646, 1),
(639, 0, 647, 1),
(640, 0, 648, 1),
(641, 0, 649, 1),
(642, 0, 650, 1),
(643, 0, 651, 1),
(644, 0, 652, 1),
(645, 0, 653, 1);

-- --------------------------------------------------------

--
-- Table structure for table `moyenne_Sem3GB`
--

CREATE TABLE `moyenne_Sem3GB` (
  `id` int(11) NOT NULL,
  `moyenne` float DEFAULT NULL,
  `id_auditeur` int(11) DEFAULT NULL,
  `id_module` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `niveau`
--

CREATE TABLE `niveau` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `niveau`
--

INSERT INTO `niveau` (`id`, `nom`, `code`) VALUES
(1, 'Master 1', 'M1'),
(2, 'Master 2', 'M2');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `note` float DEFAULT NULL,
  `id_auditeur` int(11) DEFAULT NULL,
  `id_evaluation` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notes_Sem1`
--

CREATE TABLE `notes_Sem1` (
  `id` int(11) NOT NULL,
  `note` float DEFAULT NULL,
  `id_auditeur` int(11) DEFAULT NULL,
  `id_evaluation` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notes_Sem1`
--

INSERT INTO `notes_Sem1` (`id`, `note`, `id_auditeur`, `id_evaluation`) VALUES
(157, 8.5, 525, 1),
(158, 9, 526, 1),
(159, 9.5, 527, 1),
(160, 10, 528, 1),
(161, 10.5, 529, 1),
(162, 11, 530, 1),
(163, 11.5, 531, 1),
(164, 12, 532, 1),
(165, 12.5, 533, 1),
(166, 13, 534, 1),
(167, 13.5, 535, 1),
(168, 14, 536, 1),
(169, 14.5, 537, 1),
(170, 15, 538, 1),
(171, 15.5, 539, 1),
(172, 16, 540, 1),
(173, 16.5, 541, 1),
(174, 8.5, 542, 1),
(175, 9, 543, 1),
(176, 9.5, 544, 1),
(177, 10, 545, 1),
(178, 10.5, 546, 1),
(179, 11, 547, 1),
(180, 11.5, 548, 1),
(181, 12, 549, 1),
(182, 12.5, 550, 1),
(183, 13, 551, 1),
(184, 13.5, 552, 1),
(185, 14, 553, 1),
(186, 14.5, 554, 1),
(187, 15, 555, 1),
(188, 15.5, 556, 1),
(189, 16, 557, 1),
(190, 16.5, 558, 1),
(191, 8.5, 559, 1),
(192, 9, 560, 1),
(193, 9.5, 561, 1),
(194, 10, 562, 1),
(195, 10.5, 563, 1),
(196, 11, 564, 1),
(197, 11.5, 565, 1),
(198, 12, 566, 1),
(199, 12.5, 567, 1),
(200, 13, 568, 1),
(201, 13.5, 569, 1),
(202, 14, 570, 1),
(203, 14.5, 571, 1),
(204, 15, 572, 1),
(205, 15.5, 573, 1),
(206, 16, 574, 1),
(207, 16.5, 575, 1),
(208, 8.5, 576, 1),
(209, 9, 577, 1),
(210, 9.5, 578, 1),
(211, 10, 579, 1),
(212, 10.5, 580, 1),
(213, 11, 581, 1),
(214, 11.5, 582, 1),
(215, 12, 583, 1),
(216, 12.5, 584, 1),
(217, 13, 585, 1),
(218, 13.5, 586, 1),
(219, 14, 587, 1),
(220, 14.5, 588, 1),
(221, 15, 589, 1),
(222, 15.5, 590, 1),
(223, 16, 591, 1),
(224, 16.5, 592, 1),
(225, 8.5, 593, 1),
(226, 9, 594, 1),
(227, 9.5, 595, 1),
(228, 10, 596, 1),
(229, 10.5, 597, 1),
(230, 11, 598, 1),
(231, 11.5, 599, 1),
(232, 12, 600, 1),
(233, 12.5, 601, 1),
(234, 13, 602, 1),
(235, 13.5, 603, 1),
(236, 14, 604, 1),
(237, 14.5, 605, 1),
(238, 15, 606, 1),
(239, 15.5, 607, 1),
(240, 16, 608, 1),
(241, 16.5, 609, 1),
(242, 8.5, 610, 1),
(243, 9, 611, 1),
(244, 9.5, 612, 1),
(245, 10, 613, 1),
(246, 10.5, 614, 1),
(247, 11, 615, 1),
(248, 11.5, 616, 1),
(249, 12, 617, 1),
(250, 12.5, 618, 1),
(251, 13, 619, 1),
(252, 13.5, 620, 1),
(253, 14, 621, 1),
(254, 14.5, 622, 1),
(255, 15, 623, 1),
(256, 15.5, 624, 1),
(257, 16, 625, 1),
(258, 16.5, 626, 1),
(259, 8.5, 627, 1),
(260, 9, 628, 1),
(261, 9.5, 629, 1),
(262, 10, 630, 1),
(263, 10.5, 631, 1),
(264, 11, 632, 1),
(265, 11.5, 633, 1),
(266, 12, 634, 1),
(267, 12.5, 635, 1),
(268, 13, 636, 1),
(269, 13.5, 637, 1),
(270, 14, 638, 1),
(271, 14.5, 639, 1),
(272, 15, 640, 1),
(273, 15.5, 641, 1),
(274, 16, 642, 1),
(275, 16.5, 643, 1),
(276, 8.5, 644, 1),
(277, 9, 645, 1),
(278, 9.5, 646, 1),
(279, 10, 647, 1),
(280, 10.5, 648, 1),
(281, 11, 649, 1),
(282, 11.5, 650, 1),
(283, 12, 651, 1),
(284, 12.5, 652, 1),
(285, 13, 653, 1),
(286, 13.5, 654, 1),
(287, 14, 655, 1),
(288, 15, 525, 2),
(289, 14, 526, 2),
(290, 13, 527, 2),
(291, 15, 529, 2),
(292, 12, 533, 2),
(293, 11.5, 532, 2),
(294, 11.5, 534, 2),
(295, 10, 537, 2),
(296, 14, 528, 2),
(297, 13, 530, 2),
(298, 13, 531, 2),
(299, 13, 548, 2),
(300, 14, 549, 2),
(301, 11, 550, 2),
(302, 14, 551, 2),
(303, 16, 552, 2),
(304, 17, 553, 2),
(305, 12, 554, 2),
(306, 11, 555, 2),
(307, 10, 525, 3),
(308, 14, 525, 4),
(309, 12, 526, 3),
(310, 14.75, 526, 4),
(311, 11, 527, 3),
(312, 13, 527, 4),
(313, 8, 528, 3),
(314, 16, 528, 4),
(315, 7.5, 529, 3),
(316, 13, 530, 3),
(317, 18, 531, 3),
(318, 15, 532, 3),
(319, 12, 533, 3),
(320, 13, 534, 3),
(321, 13.5, 535, 3),
(322, 10, 536, 3),
(323, 12, 537, 3),
(324, 11, 538, 3),
(325, 8, 539, 3),
(326, 7.5, 540, 3),
(327, 13, 541, 3),
(328, 18, 542, 3),
(329, 15, 543, 3),
(330, 12, 544, 3),
(331, 13, 545, 3),
(332, 13.5, 546, 3),
(333, 11, 547, 3),
(334, 11.5, 548, 3),
(335, 12, 549, 3),
(336, 12.5, 550, 3),
(337, 13, 551, 3),
(338, 13.5, 552, 3),
(339, 14, 553, 3),
(340, 14.5, 554, 3),
(341, 15, 555, 3),
(342, 15.5, 556, 3),
(343, 16, 557, 3),
(344, 10, 558, 3),
(345, 12, 559, 3),
(346, 11, 560, 3),
(347, 8, 561, 3),
(348, 7.5, 562, 3),
(349, 13, 563, 3),
(350, 18, 564, 3),
(351, 15, 565, 3),
(352, 12, 566, 3),
(353, 13, 567, 3),
(354, 13.5, 568, 3),
(355, 13.5, 569, 3),
(356, 14, 570, 3),
(357, 14.5, 571, 3),
(358, 15, 572, 3),
(359, 15.5, 573, 3),
(360, 16, 574, 3),
(361, 16.5, 575, 3),
(362, 10, 576, 3),
(363, 12, 577, 3),
(364, 11, 578, 3),
(365, 8, 579, 3),
(366, 7.5, 580, 3),
(367, 13, 581, 3),
(368, 18, 582, 3),
(369, 15, 583, 3),
(370, 12, 584, 3),
(371, 13, 585, 3),
(372, 13.5, 586, 3),
(373, 14, 587, 3),
(374, 14.5, 588, 3),
(375, 15, 589, 3),
(376, 15.5, 590, 3),
(377, 16, 591, 3),
(378, 16.5, 592, 3),
(379, 8.5, 593, 3),
(380, 9, 594, 3),
(381, 9.5, 595, 3),
(382, 10, 596, 3),
(383, 10.5, 597, 3),
(384, 11, 598, 3),
(385, 11.5, 599, 3),
(386, 12, 600, 3),
(387, 12.5, 601, 3),
(388, 10, 602, 3),
(389, 12, 603, 3),
(390, 11, 604, 3),
(391, 8, 605, 3),
(392, 7.5, 606, 3),
(393, 13, 607, 3),
(394, 18, 608, 3),
(395, 15, 609, 3),
(396, 12, 610, 3),
(397, 13, 611, 3),
(398, 13.5, 612, 3),
(399, 10, 613, 3),
(400, 10.5, 614, 3),
(401, 11, 615, 3),
(402, 11.5, 616, 3),
(403, 10, 617, 3),
(404, 12, 618, 3),
(405, 11, 619, 3),
(406, 8, 620, 3),
(407, 7.5, 621, 3),
(408, 13, 622, 3),
(409, 18, 623, 3),
(410, 15, 624, 3),
(411, 12, 625, 3),
(412, 13, 626, 3),
(413, 13.5, 627, 3),
(414, 9, 628, 3),
(415, 9.5, 629, 3),
(416, 10, 630, 3),
(417, 10.5, 631, 3),
(418, 11, 632, 3),
(419, 11.5, 633, 3),
(420, 12, 634, 3),
(421, 12.5, 635, 3),
(422, 13, 636, 3),
(423, 13.5, 637, 3),
(424, 14, 638, 3),
(425, 10, 639, 3),
(426, 12, 640, 3),
(427, 11, 641, 3),
(428, 8, 642, 3),
(429, 7.5, 643, 3),
(430, 13, 644, 3),
(431, 18, 645, 3),
(432, 15, 646, 3),
(433, 12, 647, 3),
(434, 13, 648, 3),
(435, 13.5, 649, 3),
(436, 11.5, 650, 3),
(437, 12, 651, 3),
(438, 12.5, 652, 3),
(439, 13, 653, 3),
(440, 13.5, 654, 3),
(441, 14, 655, 3),
(442, 14, 529, 4),
(443, 13, 530, 4),
(444, 16, 531, 4),
(445, 13, 532, 4),
(446, 15, 533, 4),
(447, 13, 534, 4),
(448, 13.5, 535, 4),
(449, 15, 536, 4),
(450, 12, 537, 4),
(451, 13, 538, 4),
(452, 14, 539, 4),
(453, 14.75, 540, 4),
(454, 13, 541, 4),
(455, 16, 542, 4),
(456, 14, 543, 4),
(457, 13, 544, 4),
(458, 16, 545, 4),
(459, 13, 546, 4),
(460, 15, 547, 4),
(461, 13, 548, 4),
(462, 13.5, 549, 4),
(463, 15, 550, 4),
(464, 12, 551, 4),
(465, 13, 552, 4),
(466, 14, 553, 4),
(467, 14.75, 554, 4),
(468, 13, 555, 4),
(469, 16, 556, 4),
(470, 14, 557, 4),
(471, 13, 558, 4),
(472, 16, 559, 4),
(473, 13, 560, 4),
(474, 15, 561, 4),
(475, 13, 562, 4),
(476, 13.5, 563, 4),
(477, 15, 564, 4),
(478, 12, 565, 4),
(479, 13, 566, 4),
(480, 14, 567, 4),
(481, 14.75, 568, 4),
(482, 13, 569, 4),
(483, 16, 570, 4),
(484, 14, 571, 4),
(485, 13, 572, 4),
(486, 16, 573, 4),
(487, 13, 574, 4),
(488, 15, 575, 4),
(489, 13, 576, 4),
(490, 13.5, 577, 4),
(491, 15, 578, 4),
(492, 12, 579, 4),
(493, 13, 580, 4),
(494, 14, 581, 4),
(495, 14.75, 582, 4),
(496, 13, 583, 4),
(497, 16, 584, 4),
(498, 14, 585, 4),
(499, 13, 586, 4),
(500, 16, 587, 4),
(501, 13, 588, 4),
(502, 15, 589, 4),
(503, 13, 590, 4),
(504, 13.5, 591, 4),
(505, 15, 592, 4),
(506, 12, 593, 4),
(507, 13, 594, 4),
(508, 14, 595, 4),
(509, 14.75, 596, 4),
(510, 13, 597, 4),
(511, 16, 598, 4),
(512, 14, 599, 4),
(513, 13, 600, 4),
(514, 16, 601, 4),
(515, 13, 602, 4),
(516, 15, 603, 4),
(517, 13, 604, 4),
(518, 13.5, 605, 4),
(519, 15, 606, 4),
(520, 12, 607, 4),
(521, 13, 608, 4),
(522, 14, 609, 4),
(523, 14.75, 610, 4),
(524, 13, 611, 4),
(525, 16, 612, 4),
(526, 14, 613, 4),
(527, 13, 614, 4),
(528, 16, 615, 4),
(529, 13, 616, 4),
(530, 15, 617, 4),
(531, 13, 618, 4),
(532, 13.5, 619, 4),
(533, 15, 620, 4),
(534, 12, 621, 4),
(535, 13, 622, 4),
(536, 14, 623, 4),
(537, 14.75, 624, 4),
(538, 13, 625, 4),
(539, 16, 626, 4),
(540, 14, 627, 4),
(541, 13, 628, 4),
(542, 16, 629, 4),
(543, 13, 630, 4),
(544, 15, 631, 4),
(545, 13, 632, 4),
(546, 13.5, 633, 4),
(547, 15, 634, 4),
(548, 12, 635, 4),
(549, 13, 636, 4),
(550, 13.5, 637, 4),
(551, 14, 638, 4),
(552, 10, 639, 4),
(553, 12, 640, 4),
(554, 14, 641, 4),
(555, 14.75, 642, 4),
(556, 13, 643, 4),
(557, 16, 644, 4),
(558, 14, 645, 4),
(559, 13, 646, 4),
(560, 16, 647, 4),
(561, 13, 648, 4),
(562, 15, 649, 4),
(563, 13, 650, 4),
(564, 13.5, 651, 4),
(565, 15, 652, 4),
(566, 12, 653, 4),
(567, 13, 654, 4),
(568, 13, 655, 4);

-- --------------------------------------------------------

--
-- Table structure for table `notes_Sem3GB`
--

CREATE TABLE `notes_Sem3GB` (
  `id` int(11) NOT NULL,
  `note` float DEFAULT NULL,
  `id_auditeur` int(11) DEFAULT NULL,
  `id_evaluation` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `id_annee_aca` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `promotion`
--

INSERT INTO `promotion` (`id`, `nom`, `id_annee_aca`) VALUES
(1, 'Elit One', 1);

-- --------------------------------------------------------

--
-- Table structure for table `semestre`
--

CREATE TABLE `semestre` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `code` varchar(255) NOT NULL,
  `id_classe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `semestre`
--

INSERT INTO `semestre` (`id`, `nom`, `code`, `id_classe`) VALUES
(5, 'TODO Semestre', 'Sem1', 1),
(6, 'MAster II Gestion Budgétaire', 'Sem3GB', 4);

-- --------------------------------------------------------

--
-- Table structure for table `specialite`
--

CREATE TABLE `specialite` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specialite`
--

INSERT INTO `specialite` (`id`, `nom`, `code`) VALUES
(1, 'Gestion Budgetaire', 'GB'),
(2, 'Comptabilité Publique', 'CP'),
(3, 'Finances Publiques Locales', 'FPL'),
(4, 'Tronc Commun', 'TC');

-- --------------------------------------------------------

--
-- Table structure for table `type_evaluation`
--

CREATE TABLE `type_evaluation` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `type_evaluation_Sem1`
--

CREATE TABLE `type_evaluation_Sem1` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_evaluation_Sem1`
--

INSERT INTO `type_evaluation_Sem1` (`id`, `nom`, `code`) VALUES
(1, 'Controle Continu', 'CC'),
(2, 'Examen', 'Exam');

-- --------------------------------------------------------

--
-- Table structure for table `type_evaluation_Sem3GB`
--

CREATE TABLE `type_evaluation_Sem3GB` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_evaluation_Sem3GB`
--

INSERT INTO `type_evaluation_Sem3GB` (`id`, `nom`, `code`) VALUES
(1, 'CC', 'Contrôle Continu'),
(2, 'Exam', 'Examen');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE `user_accounts` (
  `uacc_id` int(11) UNSIGNED NOT NULL,
  `uacc_group_fk` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `uacc_email` varchar(100) NOT NULL DEFAULT '',
  `uacc_username` varchar(15) NOT NULL DEFAULT '',
  `uacc_password` varchar(60) NOT NULL DEFAULT '',
  `uacc_ip_address` varchar(40) NOT NULL DEFAULT '',
  `uacc_salt` varchar(40) NOT NULL DEFAULT '',
  `uacc_activation_token` varchar(40) NOT NULL DEFAULT '',
  `uacc_forgotten_password_token` varchar(40) NOT NULL DEFAULT '',
  `uacc_forgotten_password_expire` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uacc_update_email_token` varchar(40) NOT NULL DEFAULT '',
  `uacc_update_email` varchar(100) NOT NULL DEFAULT '',
  `uacc_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `uacc_suspend` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `uacc_fail_login_attempts` smallint(5) NOT NULL DEFAULT '0',
  `uacc_fail_login_ip_address` varchar(40) NOT NULL DEFAULT '',
  `uacc_date_fail_login_ban` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Time user is banned until due to repeated failed logins',
  `uacc_date_last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uacc_date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`uacc_id`, `uacc_group_fk`, `uacc_email`, `uacc_username`, `uacc_password`, `uacc_ip_address`, `uacc_salt`, `uacc_activation_token`, `uacc_forgotten_password_token`, `uacc_forgotten_password_expire`, `uacc_update_email_token`, `uacc_update_email`, `uacc_active`, `uacc_suspend`, `uacc_fail_login_attempts`, `uacc_fail_login_ip_address`, `uacc_date_fail_login_ban`, `uacc_date_last_login`, `uacc_date_added`, `name`) VALUES
(1, 3, 'anatoleabe@gmail.com', 'admin', '$2a$08$lSOQGNqwBFUEDTxm2Y.hb.mfPEAt/iiGY9kJsZsd4ekLJXLD.tCrq', '154.72.161.122', 'XKVT29q2Jr', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 2, '127.0.0.1', '0000-00-00 00:00:00', '2016-05-09 11:10:54', '2011-01-01 00:00:00', 'ABENA Jean Louis'),
(5, 1, 'asanga.achidi@pnlt.cm', 'asanga', '$2a$08$XeM4l5dxmtCeG17V1wofQOaOqxoo0fVBLru7vYu87vdzLeKdvHmke', '::1', 'Sq28yKNmTV', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-19 14:40:32', '2015-05-19 14:40:32', NULL),
(6, 1, 'balla.henri@pnlt.cm', 'balla1', '$2a$08$D5FX800323JKVZ0iJyxMoOPfHPr4IzqrYMnBJM6IGN82bCawNf25a', '::1', 'w899ggMFBD', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-19 15:43:25', '2015-05-19 14:42:28', NULL),
(7, 1, 'efangon.philemon@pnlt.cm', 'efangon', '$2a$08$5iqXOxknl32QbAFh6/h.4e1KpIaUAfIrZcxUZlWXjIpL5HwWODofC', '154.72.134.225', 'm4WwcPScbS', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 15:47:20', '2015-05-19 14:43:11', NULL),
(8, 1, 'efouzou.eveline@pnlt.cm', 'efouzou', '$2a$08$a/O6t1KWjS8cPvSb2KaWXeqIYADMKo29hySC8f.6kx5rJsxs4ETgC', '154.72.132.228', '4cKqdRQCw4', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-07-13 14:47:19', '2015-05-19 14:44:01', NULL),
(9, 1, 'ella.therese.bella@pnlt.cm', 'ella.therese', '$2a$08$0kHNfw5M1OKaT/SPaLS/tetfy5GTsRCONoWdD1gpvV29o25rsCe1y', '41.205.81.234', 'GP84Cw5M6K', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 1, '41.204.86.76', '0000-00-00 00:00:00', '2015-06-01 09:25:00', '2015-05-19 14:44:47', NULL),
(10, 1, 'fouda.olivier@pnlt.cm', 'fouda1', '$2a$08$Wk7DS2GJLkqCtC4LYTCI3e1bKYsgZ.KkpkLH0uOPaONK.VrHL96/m', '154.72.132.86', 'WhV2vrDY5w', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-07-18 17:23:03', '2015-05-19 14:45:32', NULL),
(11, 1, 'mbassa.vincent@pnlt.cm', 'mbassa', '$2a$08$t1HQwxHyAG3qY6jIlORLHO7Z8zrAijCWz4DttyC5EkqSPYo3cfRCW', '154.72.132.126', 'FWM82dSTGG', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-06-05 11:32:26', '2015-05-19 14:46:13', NULL),
(12, 1, 'ndi.ndi.joseph@pnlt.cm', 'ndi.ndi', '$2a$08$I9dW8WB5JeYDWJXLiVylBOLG31H28g6ii4M0w.b7fvlskUv2JGm/6', '::1', 'Wh6skpGGSp', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2016-05-16 20:36:11', '2015-05-19 14:47:09', NULL),
(13, 1, 'nguafack.dubliss@pnlt.cm', 'nguafack', '$2a$08$MQ5PDVFysjWIXnrLTCadLeEwM79SN3Kuw5nx5e0KY6z7Pa6lBpNoG', '41.204.86.31', 'kZvQYpWR38', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 5, '154.72.132.95', '2016-04-01 17:05:59', '2015-08-05 11:00:36', '2015-05-19 14:47:46', NULL),
(14, 1, 'tagnenousse.jean@pnlt.cm', 'tagnenousse', '$2a$08$aY/wNB.MEx10XyJ9dumdn.P9rrVTfWfcqinlj2w1el4pXiT2b8PTq', '::1', 'ZtkPmj362m', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 12, '154.72.133.90', '2015-09-16 12:46:50', '2015-05-19 14:57:10', '2015-05-19 14:48:32', NULL),
(15, 1, 'etoundi.antoine@pnlt.cm', 'etoundi', '$2a$08$HOFmo/gGUxoyLE2L1GTJ3.9HHu1oPaaAg842XCc5EgBVfXDDFh1yq', '154.72.135.173', 'D79zKQKHcy', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-09-28 16:28:18', '2015-05-19 14:49:18', NULL),
(16, 1, 'nantchouang.alain@pnlt.cm', 'nantchouang', '$2a$08$4OD6v.3BRXwt95ovVFtjQOJX1pnpwk99LrdMJg5Qy4Ajq6iJZ7nTm', '154.72.132.135', 'RbxFbZcpWK', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-06-02 13:23:25', '2015-05-19 14:49:53', NULL),
(17, 1, 'abena.jeanlouis@pnlt.cm', 'jlabena', '$2a$08$9xotc3sNuBg/8JSFOvS7pedjuAEqfNq5nRbEscNg72LRzEkRrB24.', '154.72.161.122', 'FDMq9MYbQZ', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2016-05-09 10:30:39', '2015-05-19 14:53:05', NULL),
(18, 1, 'belinga.mvondo.edwige@pnlt.cm', 'belinga', '$2a$08$KP5hpG/OGX7g5Dqju/Lv9OyokbQP3hbEaQeUjOCggeuBsUNseLF3G', '154.72.134.225', 'WnWvhNQ8c7', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:50:20', '2015-05-22 10:50:20', NULL),
(19, 1, 'kemenang.edie@pnlt.cm', 'kemenang', '$2a$08$RAe49VJBpKRaJW4XJZUtg.nhDcqE7zGRm8M5Ctof0GJvVtGgUkHva', '154.72.134.225', 'fr5v7cf6zT', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:52:21', '2015-05-22 10:52:21', NULL),
(20, 1, 'tsawo.paulin@pnlt.cm', 'tsawop', '$2a$08$VovNohwGkemVlzP1J48As.vtdx9WkseAWmycZtgBrfu2PpbHqzBTq', '154.72.134.225', '7rsVB7R3gr', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:54:53', '2015-05-22 10:54:53', NULL),
(21, 1, 'kaoussiri.brekmo@pnlt.cm', 'kaoussiri', '$2a$08$mV.92qVoBS0aS2O4VP7JteplwQ5Lq6Zhbh51Aoau5Mlu4r2FaztSK', '154.72.134.225', 'R75Yy2C7BB', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:55:44', '2015-05-22 10:55:44', NULL),
(22, 1, 'nkou.bikoe@pnlt.cm', 'nkou.bikoe', '$2a$08$cYgqGLcK6E.iXbuuwmguxO1jA3QxJNrhPUIpUPbuqgust0MGtGUoq', '154.72.134.225', 'rJXwW49TJD', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 10:57:39', '2015-05-22 10:57:39', NULL),
(23, 1, 'nju.felix@pnlt.cm', 'nju.felix', '$2a$08$up5Ge9Erej8kcVlAS.4VRuo7Yo/LRNckMigjuv1EwFbUatGrTPQmO', '154.72.134.225', 'Y3s284fHD4', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:00:46', '2015-05-22 11:00:46', NULL),
(24, 1, 'ossima.gilles.andre@pnlt.cm', 'ossima', '$2a$08$2dcU8zBDRHkRTRHyn1nCM.ElCLvkLYGqaSgA1Gjk568rN2.K0yrAK', '154.72.134.225', '8zTz7nvMJp', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:01:47', '2015-05-22 11:01:47', NULL),
(25, 1, 'fon.elisabeth@pnlt.cm', 'elisabeth', '$2a$08$QRYnc5GGCjzOif7f5ut/duk/by0zAaQbWcjKz0UBKj2wQBmrI3q5a', '154.72.134.225', 'cTphFZpy9b', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:02:49', '2015-05-22 11:02:49', NULL),
(26, 1, 'tchieche.charlotte@pnlt.cm', 'tchieche', '$2a$08$XIFbCPgg1M18yuoLZV9iS.9r7a5PIjIriHOtuxsKvOYCBi7t4AdwS', '154.72.134.225', 'CbnpvHr9Wf', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:27:39', '2015-05-22 11:27:39', NULL),
(27, 1, 'njock.ayuk@pnlt.cm', 'njock.ayuk', '$2a$08$tIuo.PtQLllmVYmmf.RsQeCv1QLDk7RC0YIr.lEPBWDxqL0YSqGvO', '154.72.134.225', 'k29BSYVxbV', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:30:17', '2015-05-22 11:30:17', NULL),
(28, 1, 'lum.pascaline@pnlt.cm', 'pascaline', '$2a$08$KrcBLCtxv/c6sSV1vnqjR.4wIuw3pW1cNYs/eAnYHKjUv5a6XyskG', '154.72.134.225', '9fctYVtGJS', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:31:01', '2015-05-22 11:31:01', NULL),
(29, 1, 'moungou.tsondo@pnlt.cm', 'moungou', '$2a$08$9e2Rv8VkBeTsSD2J.EXr7./R6Xdhkcq109Muc6RG0jBmQw/93bmg.', '154.72.134.225', 'hPq2xpfgk8', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:32:36', '2015-05-22 11:32:36', NULL),
(30, 1, 'melody.emann@pnlt.cm', 'melody', '$2a$08$KxuivaRSzOMa8BdxZMDAAuWw84EACHhIT2IP/xNW8bK0YJfJNPZF2', '41.204.86.76', 'jSzrsPzgqf', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-07-22 13:31:21', '2015-05-22 11:34:43', NULL),
(31, 1, 'noumo@pnlt.cm', 'onoumo', '$2a$08$sLK7lO0vGbUcnRr1ezonKuGp/i3u2g6rG.euxTC0l.7WNnjTn45yG', '154.72.134.225', 'pbYGPwXcRR', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:37:04', '2015-05-22 11:37:04', NULL),
(32, 1, 'simo.nenwouo@pnlt.cm', 'nenwouo', '$2a$08$VL38AEeP5PzWtJ3EBi2sLOC1QG9O7XnQh974hLVY8we20QSWrFoJ6', '154.72.134.225', '3XGCNQ3QPv', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2015-05-22 11:38:14', '2015-05-22 11:38:14', NULL),
(34, 3, 'admin@admin.com', 'adminflex', '$2a$08$lSOQGNqwBFUEDTxm2Y.hb.mfPEAt/iiGY9kJsZsd4ekLJXLD.tCrq', '127.0.0.1', 'XKVT29q2Jr', '', '', '0000-00-00 00:00:00', '', '', 1, 0, 0, '', '0000-00-00 00:00:00', '2016-06-08 13:48:02', '2011-01-01 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_address`
--

CREATE TABLE `user_address` (
  `uadd_id` int(11) NOT NULL,
  `uadd_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `uadd_alias` varchar(50) NOT NULL DEFAULT '',
  `uadd_recipient` varchar(100) NOT NULL DEFAULT '',
  `uadd_phone` varchar(25) NOT NULL DEFAULT '',
  `uadd_company` varchar(75) NOT NULL DEFAULT '',
  `uadd_address_01` varchar(100) NOT NULL DEFAULT '',
  `uadd_address_02` varchar(100) NOT NULL DEFAULT '',
  `uadd_city` varchar(50) NOT NULL DEFAULT '',
  `uadd_county` varchar(50) NOT NULL DEFAULT '',
  `uadd_post_code` varchar(25) NOT NULL DEFAULT '',
  `uadd_country` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `ugrp_id` smallint(5) NOT NULL,
  `ugrp_name` varchar(20) NOT NULL DEFAULT '',
  `ugrp_desc` varchar(100) NOT NULL DEFAULT '',
  `ugrp_admin` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`ugrp_id`, `ugrp_name`, `ugrp_desc`, `ugrp_admin`) VALUES
(1, 'Public', 'Public User : has no admin access rights.', 0),
(2, 'Moderator', 'Admin Moderator : has partial admin access rights.', 1),
(3, 'Master Admin', 'Master Admin : has full admin access rights.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_login_sessions`
--

CREATE TABLE `user_login_sessions` (
  `usess_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `usess_series` varchar(40) NOT NULL DEFAULT '',
  `usess_token` varchar(40) NOT NULL DEFAULT '',
  `usess_login_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_login_sessions`
--

INSERT INTO `user_login_sessions` (`usess_uacc_fk`, `usess_series`, `usess_token`, `usess_login_date`) VALUES
(12, '', '024f31989cf327048c9360fdd7d28714b1fe2e78', '2016-05-16 20:36:12'),
(34, '', '02c93214872594a3383952b1abf56fac99182812', '2016-05-31 15:13:03'),
(34, '', '0ca3d6a6392b31f11269c73398984f922116e7f7', '2016-06-08 15:17:23'),
(34, '', '1be3c7c36d37c3727c8cffa180c31176d049588b', '2016-05-26 18:05:01'),
(34, '', '1e6434e18bd3cfe5f77eb91d4c0385f5ab79d282', '2016-06-06 14:58:06'),
(34, '', '2d2b6e54897402257ee16303f284c4f0255c333b', '2016-05-30 20:47:12'),
(34, '', '3445a0ab6830b929d0fc357e9679bcda700d2bf3', '2016-05-28 18:54:51'),
(34, '', '529ef3fbd632981422269849ac4dc82660b1e3e3', '2016-06-08 13:32:27'),
(34, '', '5f9e9c9461b941f88721edb16edd8e0856e3e3d1', '2016-05-29 02:03:54'),
(34, '', '656c6f5c7e28b6355727a73146e216995ac93969', '2016-05-27 13:16:35'),
(34, '', '67306a3ac0679104c90fccfdca2bac9dfb38ea7e', '2016-06-07 18:42:16'),
(34, '', '6a91f554c0b12b3d1767446bf8ec4d255d8eca55', '2016-05-30 12:14:15'),
(34, '', '76497c56d0df2353d87f5f24de59109e5eea4182', '2016-06-06 02:15:26'),
(12, '', '7b09b73fd01e1ed9f36a8067ab0f7d001051c0e7', '2016-05-16 16:49:37'),
(34, '', '81f76c009f3f537bfa8f152f8fec37f498f5210d', '2016-05-28 13:36:16'),
(34, '', '8f8bd970e1442a8995522b17ebc7fc04b6094512', '2016-06-07 02:15:58'),
(34, '', '92c9da4738db904387896f3a31ab3a55c73872f7', '2016-06-01 13:49:48'),
(12, '', '93e1c58d87a52f50059870e54c700ad40fcd0f66', '2016-05-15 23:47:31'),
(34, '', '9ec943b37ccd7041dba10bd27edb2266d6919768', '2016-06-02 17:05:22'),
(34, '', 'ba02e829797e23123d01140179e1886cb0bd5a65', '2016-06-07 15:07:15'),
(34, '', 'c187cb32cf4e7d426997dd54a534461ea55d8e30', '2016-05-31 20:17:56'),
(34, '', 'db83ed21bd374c799c9b262ed97e8cc4a4200b60', '2016-06-02 17:04:34'),
(34, '', 'e0da51c86e365db9551e759636d18a0f2d594c13', '2016-06-04 13:15:42'),
(34, '', 'e9dea11634def30c8f3c1f259a782eba0eaf883e', '2016-05-29 10:01:23'),
(34, '', 'f6ea3d6332480e031abc0151b1af5736427ee0c0', '2016-05-26 14:49:01'),
(34, '', 'f8e474bd3399add519d80ed084419ef6774c68a0', '2016-05-29 14:41:07'),
(34, '', 'fee3b44fcb36a647a32da586de9bb84a341fd777', '2016-05-27 18:16:41');

-- --------------------------------------------------------

--
-- Table structure for table `user_privileges`
--

CREATE TABLE `user_privileges` (
  `upriv_id` smallint(5) NOT NULL,
  `upriv_name` varchar(20) NOT NULL DEFAULT '',
  `upriv_desc` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_privileges`
--

INSERT INTO `user_privileges` (`upriv_id`, `upriv_name`, `upriv_desc`) VALUES
(1, 'View Users', 'User can view user account details.'),
(2, 'View User Groups', 'User can view user groups.'),
(3, 'View Privileges', 'User can view privileges.'),
(4, 'Insert User Groups', 'User can insert new user groups.'),
(5, 'Insert Privileges', 'User can insert privileges.'),
(6, 'Update Users', 'User can update user account details.'),
(7, 'Update User Groups', 'User can update user groups.'),
(8, 'Update Privileges', 'User can update user privileges.'),
(9, 'Delete Users', 'User can delete user accounts.'),
(10, 'Delete User Groups', 'User can delete user groups.'),
(11, 'Delete Privileges', 'User can delete user privileges.'),
(12, 'Print Global', 'Print Global');

-- --------------------------------------------------------

--
-- Table structure for table `user_privilege_groups`
--

CREATE TABLE `user_privilege_groups` (
  `upriv_groups_id` smallint(5) UNSIGNED NOT NULL,
  `upriv_groups_ugrp_fk` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `upriv_groups_upriv_fk` smallint(5) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_privilege_groups`
--

INSERT INTO `user_privilege_groups` (`upriv_groups_id`, `upriv_groups_ugrp_fk`, `upriv_groups_upriv_fk`) VALUES
(1, 3, 1),
(3, 3, 3),
(4, 3, 4),
(5, 3, 5),
(6, 3, 6),
(7, 3, 7),
(8, 3, 8),
(9, 3, 9),
(10, 3, 10),
(11, 3, 11),
(12, 2, 2),
(13, 2, 4),
(14, 2, 5);

-- --------------------------------------------------------

--
-- Table structure for table `user_privilege_users`
--

CREATE TABLE `user_privilege_users` (
  `upriv_users_id` smallint(5) NOT NULL,
  `upriv_users_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `upriv_users_upriv_fk` smallint(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_privilege_users`
--

INSERT INTO `user_privilege_users` (`upriv_users_id`, `upriv_users_uacc_fk`, `upriv_users_upriv_fk`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(16, 8, 12),
(17, 1, 12),
(18, 17, 12);

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `upro_id` int(11) NOT NULL,
  `upro_uacc_fk` int(11) NOT NULL DEFAULT '0',
  `upro_company` varchar(50) NOT NULL DEFAULT '',
  `upro_first_name` varchar(50) NOT NULL DEFAULT '',
  `upro_last_name` varchar(50) NOT NULL DEFAULT '',
  `upro_phone` varchar(25) NOT NULL DEFAULT '',
  `upro_newsletter` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`upro_id`, `upro_uacc_fk`, `upro_company`, `upro_first_name`, `upro_last_name`, `upro_phone`, `upro_newsletter`) VALUES
(1, 1, '', 'Anatole ', 'ABE', '237679694420', 0),
(5, 5, '', 'ZACCHEOUS', 'ASANGA ACHIDI ', '689456123', 1),
(6, 6, '', 'Henri', 'BALLA MBARGA', '689456123', 1),
(7, 7, '', 'Philémon', 'EFANGON AWOMO', '689456123', 1),
(8, 8, '', 'Eveline Sylvie', 'MBESSA épse EFOUZOU ', '689456123', 1),
(9, 9, '', 'Thérèse ', 'ELLA BELLA', '689456123', 1),
(10, 10, '', 'Olivier Désiré', 'FOUDA ', '689456123', 1),
(11, 11, '', 'Vincent', 'MBASSA ', '689456123', 1),
(12, 12, '', 'Joseph', 'NDI NDI', '689456123', 1),
(13, 13, '', 'Dubliss ', 'NGUAFACK NJIMOH', '689456123', 1),
(14, 14, '', 'JEAN JULES', 'TAGNE NOUSSE ', '689456123', 1),
(15, 15, '', 'Antoine de Padoue', ' ETOUNDI EVOUNA ', '689456123', 1),
(16, 16, '', 'Alain', 'NANTCHOUANG  PIEDJOU', '689456123', 1),
(17, 17, '', 'Jean Louis', 'ABENA FOE ', '689456123', 1),
(18, 18, '', 'EDWIGE', 'BELINGA Née MVONDO ABENG ', '689456123', 1),
(19, 19, '', 'Alain', ' Kemenang Edie ', '777777', 1),
(20, 20, '', 'Paulin Castro', ' TSAWO ', '777777', 1),
(21, 21, '', 'BREKMO ', ' KAOUSSIRI ', '777777', 1),
(22, 22, '', 'Adolphe', ' NKOU BIKOE', '777777', 1),
(23, 23, '', 'Félix', ' NJU ', '777777', 1),
(24, 24, '', 'Gilles André', 'OSSIMA ', '777777', 1),
(25, 25, '', 'Élizabeth', ' FON ', '777777', 1),
(26, 26, '', 'CHARLOTTE', 'TCHIECHE ', '777777', 1),
(27, 27, '', 'Léo', ' NJOCK AYUK', '777777', 1),
(28, 28, '', 'Pascaline', 'LUM ', '777777', 1),
(29, 29, '', 'Thérèse', 'Mme MOUNGOU TSONDO Epse OHANDZA ', '777777', 1),
(30, 30, '', 'MELODY', 'AVOULOU EMANN ', '777777', 0),
(31, 31, '', 'Odette', 'NOUMO épse KEMGANG', '777777', 1),
(32, 32, '', 'Léonie', 'SIMO épse NENWOUO ', '777777', 1),
(33, 34, '', 'Administrator', 'AD', '677 77 77 77', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anne_academique`
--
ALTER TABLE `anne_academique`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auditeurs`
--
ALTER TABLE `auditeurs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_promotion` (`id_promotion`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `last_activity` (`last_activity`);

--
-- Indexes for table `classe`
--
ALTER TABLE `classe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_type_evaluation` (`id_type_evaluation`),
  ADD KEY `id_matiere` (`id_matiere`);

--
-- Indexes for table `evaluation_Sem1`
--
ALTER TABLE `evaluation_Sem1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluation_Sem3GB`
--
ALTER TABLE `evaluation_Sem3GB`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matiere`
--
ALTER TABLE `matiere`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_module` (`id_module`);

--
-- Indexes for table `matiere_Sem1`
--
ALTER TABLE `matiere_Sem1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matiere_Sem3GB`
--
ALTER TABLE `matiere_Sem3GB`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_semestre` (`id_semestre`);

--
-- Indexes for table `modules_Sem1`
--
ALTER TABLE `modules_Sem1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules_Sem3GB`
--
ALTER TABLE `modules_Sem3GB`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moyenne`
--
ALTER TABLE `moyenne`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_auditeur` (`id_auditeur`),
  ADD KEY `id_module` (`id_module`);

--
-- Indexes for table `moyenne_matiere_Sem1`
--
ALTER TABLE `moyenne_matiere_Sem1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moyenne_Sem1`
--
ALTER TABLE `moyenne_Sem1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moyenne_Sem3GB`
--
ALTER TABLE `moyenne_Sem3GB`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `niveau`
--
ALTER TABLE `niveau`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_auditeur` (`id_auditeur`),
  ADD KEY `id_evaluation` (`id_evaluation`);

--
-- Indexes for table `notes_Sem1`
--
ALTER TABLE `notes_Sem1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes_Sem3GB`
--
ALTER TABLE `notes_Sem3GB`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_annee_aca` (`id_annee_aca`);

--
-- Indexes for table `semestre`
--
ALTER TABLE `semestre`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specialite`
--
ALTER TABLE `specialite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_evaluation`
--
ALTER TABLE `type_evaluation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_evaluation_Sem1`
--
ALTER TABLE `type_evaluation_Sem1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_evaluation_Sem3GB`
--
ALTER TABLE `type_evaluation_Sem3GB`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_accounts`
--
ALTER TABLE `user_accounts`
  ADD PRIMARY KEY (`uacc_id`),
  ADD UNIQUE KEY `uacc_id` (`uacc_id`),
  ADD KEY `uacc_group_fk` (`uacc_group_fk`),
  ADD KEY `uacc_email` (`uacc_email`),
  ADD KEY `uacc_username` (`uacc_username`),
  ADD KEY `uacc_fail_login_ip_address` (`uacc_fail_login_ip_address`);

--
-- Indexes for table `user_address`
--
ALTER TABLE `user_address`
  ADD PRIMARY KEY (`uadd_id`),
  ADD UNIQUE KEY `uadd_id` (`uadd_id`),
  ADD KEY `uadd_uacc_fk` (`uadd_uacc_fk`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`ugrp_id`),
  ADD UNIQUE KEY `ugrp_id` (`ugrp_id`) USING BTREE;

--
-- Indexes for table `user_login_sessions`
--
ALTER TABLE `user_login_sessions`
  ADD PRIMARY KEY (`usess_token`),
  ADD UNIQUE KEY `usess_token` (`usess_token`);

--
-- Indexes for table `user_privileges`
--
ALTER TABLE `user_privileges`
  ADD PRIMARY KEY (`upriv_id`),
  ADD UNIQUE KEY `upriv_id` (`upriv_id`) USING BTREE;

--
-- Indexes for table `user_privilege_groups`
--
ALTER TABLE `user_privilege_groups`
  ADD PRIMARY KEY (`upriv_groups_id`),
  ADD UNIQUE KEY `upriv_groups_id` (`upriv_groups_id`) USING BTREE,
  ADD KEY `upriv_groups_ugrp_fk` (`upriv_groups_ugrp_fk`),
  ADD KEY `upriv_groups_upriv_fk` (`upriv_groups_upriv_fk`);

--
-- Indexes for table `user_privilege_users`
--
ALTER TABLE `user_privilege_users`
  ADD PRIMARY KEY (`upriv_users_id`),
  ADD UNIQUE KEY `upriv_users_id` (`upriv_users_id`) USING BTREE,
  ADD KEY `upriv_users_uacc_fk` (`upriv_users_uacc_fk`),
  ADD KEY `upriv_users_upriv_fk` (`upriv_users_upriv_fk`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`upro_id`),
  ADD UNIQUE KEY `upro_id` (`upro_id`),
  ADD KEY `upro_uacc_fk` (`upro_uacc_fk`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anne_academique`
--
ALTER TABLE `anne_academique`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `auditeurs`
--
ALTER TABLE `auditeurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=656;
--
-- AUTO_INCREMENT for table `classe`
--
ALTER TABLE `classe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `evaluation_Sem1`
--
ALTER TABLE `evaluation_Sem1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `evaluation_Sem3GB`
--
ALTER TABLE `evaluation_Sem3GB`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `matiere_Sem1`
--
ALTER TABLE `matiere_Sem1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `matiere_Sem3GB`
--
ALTER TABLE `matiere_Sem3GB`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `modules_Sem1`
--
ALTER TABLE `modules_Sem1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `modules_Sem3GB`
--
ALTER TABLE `modules_Sem3GB`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `moyenne_matiere_Sem1`
--
ALTER TABLE `moyenne_matiere_Sem1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=284;
--
-- AUTO_INCREMENT for table `moyenne_Sem1`
--
ALTER TABLE `moyenne_Sem1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=646;
--
-- AUTO_INCREMENT for table `moyenne_Sem3GB`
--
ALTER TABLE `moyenne_Sem3GB`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `niveau`
--
ALTER TABLE `niveau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notes_Sem1`
--
ALTER TABLE `notes_Sem1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=569;
--
-- AUTO_INCREMENT for table `notes_Sem3GB`
--
ALTER TABLE `notes_Sem3GB`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `semestre`
--
ALTER TABLE `semestre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `specialite`
--
ALTER TABLE `specialite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `type_evaluation_Sem1`
--
ALTER TABLE `type_evaluation_Sem1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `type_evaluation_Sem3GB`
--
ALTER TABLE `type_evaluation_Sem3GB`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_accounts`
--
ALTER TABLE `user_accounts`
  MODIFY `uacc_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `user_address`
--
ALTER TABLE `user_address`
  MODIFY `uadd_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `ugrp_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_privileges`
--
ALTER TABLE `user_privileges`
  MODIFY `upriv_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `user_privilege_groups`
--
ALTER TABLE `user_privilege_groups`
  MODIFY `upriv_groups_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `user_privilege_users`
--
ALTER TABLE `user_privilege_users`
  MODIFY `upriv_users_id` smallint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `upro_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `evaluation`
--
ALTER TABLE `evaluation`
  ADD CONSTRAINT `evaluation_ibfk_1` FOREIGN KEY (`id_type_evaluation`) REFERENCES `type_evaluation` (`id`),
  ADD CONSTRAINT `evaluation_ibfk_2` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id`);

--
-- Constraints for table `matiere`
--
ALTER TABLE `matiere`
  ADD CONSTRAINT `matiere_ibfk_1` FOREIGN KEY (`id_module`) REFERENCES `modules` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
